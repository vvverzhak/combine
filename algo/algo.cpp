
#include "algo/algo.hpp"

unsigned CAlgo::height;
unsigned CAlgo::width;

vector<shared_ptr<CAlgo> > algs;

void CAlgo::init(const unsigned __height, const unsigned __width)
{
#ifdef ALGO_ENABLE

	height = __height;
	width = __width;

	algs.push_back(shared_ptr<CAlgo>(new CDeblurring));

#endif
}

void CAlgo::process()
{
#ifdef ALGO_ENABLE

	Mat frame = disp_to_Mat(height, width);

	for(auto & algo : algs)
		frame = (* algo)(frame);

	Mat_to_disp(frame);

#endif
}

void CAlgo::destroy()
{
#ifdef ALGO_ENABLE

	;

#endif
}

// ############################################################################ 

void wait_function(const unsigned param_size, atomic_bool & is_run, const uint16_t port, const unsigned delay_ms, function<void(void *, const bool)> set, mutex & is_init, mutex & mtx)
{
#define BUF_SIZE 4096

	int sock;
	uint8_t buf[BUF_SIZE];
	sockaddr_in addr;
	pollfd pfd;

	throw_if(param_size > BUF_SIZE);
	set(NULL, true);

	is_init.unlock();

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;

	throw_if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0);
	throw_if(bind(sock, (sockaddr *) & addr, sizeof(addr)));

	while(is_run)
	{
		pfd.fd = sock;
		pfd.events = POLLRDNORM;
		pfd.revents = 0;

		if
		(
			poll(& pfd, 1, delay_ms) >= 0
			&&
			pfd.revents & POLLRDNORM
		)
		{
			mtx.lock();

			if(read(sock, buf, param_size) != param_size)
				set(NULL, true);
			else
				set(buf, false);

			mtx.unlock();
		}
	}

	close(sock);
}

// ############################################################################ 

CAlgo::CAlgo(const unsigned param_size, const uint16_t port, const unsigned delay_ms, function<void(void *, const bool)> set)
{
	is_run = true;
	is_init.lock();

	thr = thread(wait_function, param_size, ref(is_run), port, delay_ms, set, ref(is_init), ref(mtx));
}

CAlgo::~CAlgo()
{
	if(is_run)
	{
		is_run = false;
		thr.join();
	}
}

