
#ifndef ALGO_HPP
#define ALGO_HPP

#ifndef EMBEDDED
#ifndef __MINGW32__

// #define ALGO_ENABLE TODO Перенести в CMakeLists.txt

#endif
#endif

#include "base/base.hpp"

class CAlgo
{
	static unsigned height, width;

	mutex mtx, is_init;
	atomic_bool is_run;
	thread thr;

	public:

		static void init(const unsigned __height, const unsigned __width);
		static void process();
		static void destroy();

		CAlgo(const unsigned param_size, const uint16_t port, const unsigned delay_ms, function<void(void *, const bool)> set);
		~CAlgo();

#ifdef ALGO_ENABLE

		virtual Mat operator()(const Mat & frame) = 0;

};

#include "algo/deblurring.hpp"

#else

};

#endif

#endif

