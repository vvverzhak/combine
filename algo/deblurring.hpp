
#ifndef DEBLURRING_HPP
#define DEBLURRING_HPP

#include "base/base.hpp"
#include "base/config.hpp"

class CDeblurring : public CAlgo
{
	struct __attribute__((packed)) s_param
	{
		uint32_t is_apply;
		double sigma;
		double koef;
		uint32_t filterM;
		uint32_t filterN;
		uint32_t medianSize;
	} param;

	public:

		CDeblurring();

		Mat operator()(const Mat & frame);
};

#endif

