
#include "algo/algo.hpp"

#ifdef ALGO_ENABLE

CDeblurring::CDeblurring() :
	CAlgo(sizeof(s_param), deblurring_port(), 100,
	[this] (void * ptr, const bool is_default)
	{
		if(is_default)
		{
			param.is_apply = false;
			param.sigma = 0.5;
			param.koef = 0.1;
			param.filterM = 3;
			param.filterN = 3;
			param.medianSize = 11;
		}
		else
		{
			s_param * __param = (s_param *) ptr;

			param.is_apply = __param->is_apply;
			param.sigma = __param->sigma;
			param.koef = __param->koef;
			param.filterM = __param->filterM;
			param.filterN = __param->filterN;
			param.medianSize = __param->medianSize;
		}
	})
{
	;
}

Mat CDeblurring::operator()(const Mat & src)
{
	if(! param.is_apply)
		return src;

    // Параметры для обработки
	int height=src.rows;
	int width=src.cols;

    // Размываем
    Mat frameBlur;
    medianBlur(src, frameBlur, param.medianSize);

    // Получаем ядро Лапласа + dft
    Mat lFilter;
    lFilter = Mat::zeros(height,width,CV_64F);
    lFilter.at<double>(0,1) = -1;
    lFilter.at<double>(1,0) = -1;
    lFilter.at<double>(1,2) = -1;
    lFilter.at<double>(2,2) = -1;
    lFilter.at<double>(1,1) = 4;

    Mat lFilterFourier;
    Mat lFilterResult;
    vector<Mat> lFilterChannel2 = { lFilter,
                                Mat::zeros(lFilter.size(), CV_64F) };

    merge(lFilterChannel2, lFilterResult);
    cv::dft(lFilterResult, lFilterFourier, DFT_COMPLEX_OUTPUT);

    // Получаем ядро Гаусса + dft
    Mat gFilter;
    gFilter = Mat::zeros(height,width,CV_64F);
    double sigma2=param.sigma*param.sigma;
    for (int i=0; i<param.filterM; i++)
    {
        int x=(i - param.filterM/2);
        int x2=x*x;
        for (int j=0; j<param.filterN; j++ )
        {
            int y=(j - param.filterN/2);
            int y2=y*y;
            gFilter.at<double>(i,j)=exp(-(x2+ y2)/(2*sigma2))/(2*M_PI*sigma2);
        }
    }

    Mat gaussFilterFourier;
    Mat gaussFilterResult;
    vector<Mat> gFilterChannel2 = { gFilter,
                                Mat::zeros(gFilter.size(), CV_64F) };

    merge(gFilterChannel2, gaussFilterResult);
    cv::dft(gaussFilterResult, gaussFilterFourier, DFT_COMPLEX_OUTPUT);

    // Получаем фильтр Тихонова
    Mat kernel;
    kernel.create(height, width, CV_64FC2);

           for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                Vec2d laplasElement=lFilterFourier.at<Vec2d>(i,j);
                complex<double> laplas(laplasElement[0],laplasElement[1]);
                complex<double> laplasFourieAbs = laplas
                        * conj(laplas);

                Vec2d gaussElement=gaussFilterFourier.at<Vec2d>(i,j);
                complex<double> gauss(gaussElement[0],gaussElement[1]);
                complex<double> gaussFilterFourieAbs = gauss
                        * conj(gauss);

                complex<double> tichonov=conj(gauss)/(gaussFilterFourieAbs + param.koef*laplasFourieAbs);

                Vec2d tichonovElement(real(tichonov),imag(tichonov));
                kernel.at<Vec2d>(i,j)=tichonovElement;
            }

           vector<Mat> channels;
           vector<Mat> result;
           split(frameBlur,channels);

    for (auto & partColor:channels)
    {

        Mat partColorReal;
        Mat partColorFourie;
        //Добавили нули, сделали 2х канальным
        vector<Mat> partColorChannel2 = { Mat_<double>(partColor),
                                 Mat::zeros(height, width, CV_64F) };

        merge(partColorChannel2, partColorReal);

        //Преобразование Фурье к изображению в 1м цвете
        dft(partColorReal, partColorFourie, DFT_COMPLEX_OUTPUT);

        //перемножениe
        Mat partColorFourieResult;
        Mat partColorResultComplexChannel;
        mulSpectrums(partColorFourie,kernel, partColorFourieResult, 0);

        //Обратное преобразование Фурье
        idft(partColorFourieResult, partColorResultComplexChannel, DFT_SCALE);

        // Бьем на каналы для выделения действительной и мнимой части
        vector<Mat> partColorResultChannels;
        split(partColorResultComplexChannel, partColorResultChannels);

        Mat partColorResult=Mat_<uint8_t>(partColorResultChannels[0]);
        // Добавляем обработанный канал в вектор матриц
        result.push_back(partColorResult);
    }
    Mat dst;
    merge(result,dst);
    //Mat dst = src.clone();

    return dst;
}

#endif

