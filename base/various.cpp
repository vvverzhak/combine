
#include "base/base.hpp"

#ifndef EMBEDDED

Mat disp_to_Mat(const unsigned height, const unsigned width)
{
	shared_ptr<uint8_t> buf;
	uint8_t * p_buf, * p_pixel;
	const unsigned buf_size = height * width * 4;
	unsigned v, u;
	Mat img;

	buf.reset(new uint8_t[buf_size]);
	throw_null(p_buf = buf.get());
	img = Mat::zeros(height, width, CV_8UC3);

	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, p_buf);

	for(v = 0; v < height; v++)
		for(u = 0; u < width; u++)
		{
			p_pixel = p_buf + v * width * 4 + u * 4;
			// img.at<Vec3b>(v, u) = Vec3b(* (p_pixel + 2), * (p_pixel + 1), * p_pixel);
			img.at<Vec3b>(v, u) = Vec3b(* p_pixel, * (p_pixel + 1), * (p_pixel + 2));
		}

	return img;
}

void Mat_to_disp(const Mat & img)
{
	const unsigned height = img.rows, width = img.cols;
	Mat __img;
	vector<Mat> ch;

//	cvtColor(img, __img, CV_BGR2RGB);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDrawPixels(width, height, GL_RGB, GL_UNSIGNED_BYTE, img.data);
//	glDrawPixels(width, height, GL_RGB, GL_UNSIGNED_BYTE, __img.data);
}

void save_texture(const string fname, const unsigned height, const unsigned width)
{
	imwrite(fname, disp_to_Mat(height, width));
}

void save_buf_to_image(const uint8_t * buf, const string fname, const unsigned height, const unsigned width)
{
	unsigned v, u;
	Mat img(height, width, CV_8UC4);

	for(v = 0; v < height; v++)
		for(u = 0; u < width; u++)
		{
			const uint8_t * ptr = buf + (v * width + u) * 4;
			img.at<Vec4b>(v, u) = Vec4b(* ptr, * (ptr + 1), * (ptr + 2), * (ptr + 3));
		}

	imwrite(fname, img);
}

#endif

