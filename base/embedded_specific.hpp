
#ifndef EMBEDDED_SPECIFIC_HPP
#define EMBEDDED_SPECIFIC_HPP

#ifdef EMBEDDED

	#include <GLES/gl.h>
	#include <GLES/glext.h>
	#include "glu.h"
	
	#define GL_FLOAT_TYPE		GLfloat
	#define GL_FLOAT_TYPE_ID	GL_FLOAT

	void DEPRECATED gl_ortho(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat near, const GLfloat far);
	void DEPRECATED glu_perspective(const double aspect_y, const double aspect_xy, const double near, const double far);

	#define glBindFramebuffer			glBindFramebufferOES
	#define glFramebufferTexture2D		glFramebufferTexture2DOES
	#define glCheckFramebufferStatus	glCheckFramebufferStatusOES
	#define glMapBuffer					glMapBufferOES
	#define glUnmapBuffer				glUnmapBufferOES
	#define glGenFramebuffers			glGenFramebuffersOES
	#define glGenerateMipmap			glGenerateMipmapOES
	#define GL_FRAMEBUFFER				GL_FRAMEBUFFER_OES
	#define GL_COLOR_ATTACHMENT0		GL_COLOR_ATTACHMENT0_OES
	#define GL_FRAMEBUFFER_COMPLETE		GL_FRAMEBUFFER_COMPLETE_OES
	#define GL_WRITE_ONLY				GL_WRITE_ONLY_OES

#else

	#include <GL/glew.h>
	#include <opencv2/opencv.hpp>

	using namespace cv;

	#define GL_FLOAT_TYPE		GLdouble
	#define GL_FLOAT_TYPE_ID	GL_DOUBLE

	#define gl_ortho			glOrtho
	#define glu_perspective		gluPerspective

#endif

#endif

