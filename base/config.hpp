
// Конфигурация

#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "base/base.hpp"

bool is_print_coord();
bool is_polygon_as_a_line();
uint16_t deblurring_port();
string map_fname();
string height_map_fname();
string classifier_fname();
string local_param_fname();
string font_fname();

#endif

