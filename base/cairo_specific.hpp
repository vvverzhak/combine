
#ifndef CAIRO_SPECIFIC_HPP
#define CAIRO_SPECIFIC_HPP

#ifdef USING_CAIRO

	#include <cairomm/cairomm.h>

#else

	#include <ft2build.h>
	#include FT_FREETYPE_H

#endif

#endif

