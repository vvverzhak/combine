
#ifndef BASE_HPP
#define BASE_HPP

#define GL_GLEXT_PROTOTYPES

#include <cstdio>
#include <cstdint>
#include <cstring>
#include <cstdarg>
#include <vector>
#include <map>
#include <string>
#include <functional>
#include <algorithm>
#include <memory>
#include <stack>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <malloc.h>
#include <utility>
#include <functional>
#include <iconv.h>
#include <thread>
#include <mutex>
#include <atomic>

using namespace std;

#define DEPRECATED __attribute__ ((deprecated))

#include "base/mingw_specific.hpp"
#include "base/cairo_specific.hpp"
#include "base/embedded_specific.hpp"
#include "base/various.hpp"

#define WITH_OPENGL
#include <amv/amv.hpp>

using namespace amv;

#endif

