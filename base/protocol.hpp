
#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

#define COMMAND_EXIT						0
#define COMMAND_PING						1
#define COMMAND_REDRAW						2

#define COMMAND_GET_X						3
#define COMMAND_GET_Y						4
#define COMMAND_GET_H						9
#define COMMAND_GET_COURSE					10
#define COMMAND_GET_ROLL					11
#define COMMAND_GET_PITCH					12
#define COMMAND_GET_TIME					13
// #define COMMAND_GET_SPEED_NORTH			14
// #define COMMAND_GET_SPEED_EAST			15
// #define COMMAND_GET_SPEED_VERT			16
#define COMMAND_GET_ASPECT_X				32
#define COMMAND_GET_ASPECT_Y				33

#define COMMAND_SET_X						17
#define COMMAND_SET_Y						18
#define COMMAND_SET_H						23
#define COMMAND_SET_COURSE					24
#define COMMAND_SET_ROLL					25
#define COMMAND_SET_PITCH					26
// #define COMMAND_SET_SPEED_NORTH			27
// #define COMMAND_SET_SPEED_EAST			28
// #define COMMAND_SET_SPEED_VERT			29
#define COMMAND_SET_ASPECT_X				30
#define COMMAND_SET_ASPECT_Y				31
#define COMMAND_SET_ALL_GL					42
#define COMMAND_SET_ALL_GEO					43
#define COMMAND_SET_ALL_GAUSS_KRUEGER		44

// ############################################################################ 

#define PONG	0xDEADDEAD
#define MAX_H	74688

struct __attribute__((packed)) SPacketHeader
{
	uint32_t command;
	uint32_t size;
	uint32_t port;
};

struct __attribute__((packed)) SPacketPong
{
	uint32_t pong;
};

struct __attribute__((packed)) SPacketGetSet
{
	double value;
};

struct __attribute__((packed)) SPacketSetAll
{
	double x;
	double y;
	double h;
	double course;
	double roll;
	double pitch;
};

struct __attribute__((packed)) SPacketTime
{
	uint16_t hour;
	uint16_t min;
	uint16_t sec;
};

struct __attribute__((packed)) SPacketRaster
{
	uint32_t height;
	uint32_t width;
	uint8_t channel_num;
};

// ############################################################################ 

struct __attribute__((packed)) SGaginPacket
{
	int32_t Head;
	int32_t FrameNumber;
	double  Latitude;
	double	Longitude;
	double	Height;
	double	HeightBarometric;
	double	Course;
	double	CourseComplex;
	double	CourseMagnetic;
	double	Roll;
	double	Tangage;

	float   V_TV;
	float	H_TV;
	float	V_CTV;
	float	H_CTV;
	float	V_EFA;
	float	H_EFA;
	float	V_FLIR;
	float	H_FLIR;

	double	Snos;
	double	VComp;
	double	VN;
	double	VE;
	double	VH;

	int32_t	WorkCycles;

	double  AngVelX;
	double	AngVelY;
	double	AngVelZ;
	double	ApparentAccelerationX;
	double	ApparentAccelerationY;
	double	ApparentAccelerationZ;

	int16_t CheckSum;
};

#endif

