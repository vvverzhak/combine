
#ifndef MINGW_SPECIFIC_HPP
#define MINGW_SPECIFIC_HPP

#ifdef __MINGW32__

#else

	#include <sys/mman.h>
	#include <signal.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <sys/stat.h>
	#include <arpa/inet.h>
	#include <sys/wait.h>
	#include <poll.h>
	#include <netinet/ip.h>

#endif

#endif

