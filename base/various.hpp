
#ifndef VARIOUS_HPP
#define VARIOUS_HPP

#include "base/base.hpp"
#include "base/config.hpp"

#ifndef EMBEDDED

	Mat disp_to_Mat(const unsigned height, const unsigned width);
	void Mat_to_disp(const Mat & Mat);
	void save_texture(const string fname, const unsigned height, const unsigned width);
	void save_buf_to_image(const uint8_t * buf, const string fname, const unsigned height, const unsigned width);

#endif

#endif

