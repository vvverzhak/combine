
#include "map/point.hpp"

CVec3d::CVec3d() :
	x(0), y(0), z(0)
{
	;
}

CVec3d::CVec3d(const CVec3d & v) :
	x(v.x), y(v.y), z(v.z)
{
	;
}

CVec3d::CVec3d(const double t_x, const double t_y, const double t_z)
{
	x = t_x;
	y = t_y;
	z = t_z;
}

const CVec3d & CVec3d::operator=(const CVec3d & v)
{
	x = v.x;
	y = v.y;
	z = v.z;

	return * this;
}

double CVec3d::operator[](const unsigned ind) const
{
	switch(ind)
	{
		case 0: return x;
		case 1: return y;
		case 2: return z;
	}

	throw_;
}

double & CVec3d::operator[](const unsigned ind)
{
	switch(ind)
	{
		case 0: return x;
		case 1: return y;
		case 2: return z;
	}

	throw_;
}

const CVec3d CVec3d::operator+(const CVec3d & v)
{
	CVec3d ret;

	ret.x = x + v.x;
	ret.y = y + v.y;
	ret.z = z + v.z;

	return ret;
}

const CVec3d & CVec3d::operator+=(const CVec3d & v)
{
	x += v.x;
	y += v.y;
	z += v.z;

	return * this;
}

const CVec3d CVec3d::operator-(const CVec3d & v)
{
	return CVec3d(x - v.x, y - v.y, z - v.z);
}

double CVec3d::distance(const CVec3d & op_1, const CVec3d & op_2)
{
	return sqrt(pow(op_1.x - op_2.x, 2) + pow(op_1.y - op_2.y, 2) + pow(op_1.z - op_2.z, 2));
}

// ############################################################################ 

CGeoPoint::CGeoPoint()
{
	;
}

CGeoPoint::CGeoPoint(const double t_lat, const double t_lon, const double t_h) :
	CPoint(t_lat, t_lon, t_h)
{
	;
}

CGeoPoint::CGeoPoint(const CGeoPoint & gp) :
	CPoint(gp)
{
	;
}

const CGeoPoint & CGeoPoint::operator=(const CGeoPoint & gpnt)
{
	lat = gpnt.lat;
	lon = gpnt.lon;
	h = gpnt.h;

	return * this;
}

const CGeoPoint & CGeoPoint::operator=(const CPoint & pnt)
{
	lat = pnt.x;
	lon = pnt.y;
	h = pnt.z;

	return * this;
}

// ############################################################################ 

CEulerAngle::CEulerAngle()
{
	;
}

CEulerAngle::CEulerAngle(const double t_course, const double t_roll, const double t_pitch) :
	CVec3d(t_course, t_roll, t_pitch)
{
	;
}

CEulerAngle::CEulerAngle(const CEulerAngle & ea) :
	CVec3d(ea)
{
	;
}

const CEulerAngle & CEulerAngle::operator=(const CEulerAngle & ea)
{
	course = ea.course;
	roll = ea.roll;
	pitch = ea.pitch;

	return * this;
}

