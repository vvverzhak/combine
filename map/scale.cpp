
#include "map/scale.hpp"

CScale::CScale(const CPoint & t_gk_from, const CPoint & t_gk_to, const CPoint & t_map_from, const CPoint & t_map_to, const CPoint & t_scene_from, const CPoint & t_scene_to, const bool is_xy_proportional) :
	gk_from(t_gk_from), gk_to(t_gk_to), map_from(t_map_from), map_to(t_map_to), scene_from(t_scene_from), scene_to(t_scene_to)
{
	unsigned v;
	CPoint real_gk_from = gk_from, real_gk_to = gk_to;
	CPoint real_map_from = map_from, real_map_to = map_to;
	
	if(is_xy_proportional)
	{
		if(real_gk_to.x - real_gk_from.x > real_gk_to.y - real_gk_from.y)
		{
			real_gk_to.y = real_gk_to.x;
			real_gk_from.y = real_gk_from.x;
		}
		else
		{
			real_gk_to.x = real_gk_to.y;
			real_gk_from.x = real_gk_from.y;
		}

		if(real_map_to.x - real_map_from.x > real_map_to.y - real_map_from.y)
		{
			real_map_to.y = real_map_to.x;
			real_map_from.y = real_map_from.x;
		}
		else
		{
			real_map_to.x = real_map_to.y;
			real_map_from.x = real_map_from.y;
		}
	}

	// ############################################################################ 
	// Гаусс, Крюгер

	for(v = 0; v < 3; v++)
	{
		const double gk_d = real_gk_to[v] - real_gk_from[v];
		const double scene_d = scene_to[v] - scene_from[v];

		throw_if(! (fabs(gk_d) > 0. && fabs(scene_d) > 0.));
		
		gk_a[v] = scene_d / gk_d;
		gk_b[v] = scene_to[v] - gk_a[v] * gk_to[v];
	}

	// ############################################################################ 
	// Карта

	for(v = 0; v < 3; v++)
	{
		const double map_d = real_map_to[v] - real_map_from[v];
		const double scene_d = scene_to[v] - scene_from[v];

		throw_if(! (fabs(map_d) > 0. && fabs(scene_d) > 0.));
		
		map_a[v] = scene_d / map_d;
		map_b[v] = scene_to[v] - map_a[v] * map_to[v];
	}
}

CPoint CScale::scene_to_gk(const CPoint & pnt) const
{
	unsigned v;
	CPoint ret;

	for(v = 0; v < 3; v++)
		ret[v] = (pnt[v] - gk_b[v]) / gk_a[v];

	return ret;
}

CPoint CScale::gk_to_scene(const CPoint & pnt) const
{
	unsigned v;
	CPoint ret;

	for(v = 0; v < 3; v++)
		ret[v] = gk_a[v] * pnt[v] + gk_b[v];

	return ret;
}

CPoint CScale::scene_to_map(const CPoint & pnt) const
{
	unsigned v;
	CPoint ret;

	for(v = 0; v < 3; v++)
		ret[v] = (pnt[v] - map_b[v]) / map_a[v];

	return ret;
}

CPoint CScale::map_to_scene(const CPoint & pnt) const
{
	unsigned v;
	CPoint ret;

	for(v = 0; v < 3; v++)
		ret[v] = map_a[v] * pnt[v] + map_b[v];

	return ret;
}

