
#ifndef MAP_HPP
#define MAP_HPP

#include "base/base.hpp"
#include "map/point.hpp"
#include "map/scale.hpp"

enum EObjectType
{
	EOT_NOT_DRAW, EOT_POINT, EOT_LINE, EOT_POLYGON, EOT_TEXT
};

enum ESemanticsType
{
	EST_ASCIIZ		= 0,
	EST_ANSI		= 126,
	EST_UNICODE		= 127,
	EST_INT8		= 1,
	EST_INT16		= 2,
	EST_INT32		= 4,
	EST_DOUBLE		= 8
};

template<typename sem_type>
class CSemantics
{
	public:

		ESemanticsType type;
		sem_type value;

		CSemantics()
		{
			;
		};

		CSemantics(const sem_type __value, const ESemanticsType __type) :
			type(__type), value(__value)
		{
			;
		};

		CSemantics & operator=(const CSemantics & sem)
		{
			type = sem.type;
			value = sem.value;

			return * this;
		};

		int cmp(const CSemantics & sem) const
		{
			const double sub = value - sem.value;
			const double threshold = 0.001;

			if(sub < threshold)
				return -1;
			else if(sub > threshold)
				return 1;

			return 0;
		};
};

class CObject
{
	public:

		unsigned id, code, localization;
		vector<CPoint> pnt, pnt_scene;
		vector<CObject> sub_obj;
		EObjectType type;
		string label;
		map<unsigned, CSemantics<string> > string_semantics;
		map<unsigned, CSemantics<double> > double_semantics;

		void init(const EObjectType __type, const unsigned __id, const unsigned __code, const unsigned __localization);
		void init(const vector<CPoint> __pnt, const EObjectType __type, const unsigned __id, const unsigned __code, const unsigned __localization, const string __label);
		CObject & operator=(const CObject & obj);
		void map_to_scene(const CScale & scale);
		void add_semantics(const unsigned code, const CSemantics<string> & sem);
		void add_semantics(const unsigned code, const CSemantics<double> & sem);
		string semantics_value(const unsigned code, const string default_value = "") const;
		double semantics_value(const unsigned code, const double default_value = 0) const;
		int cmp_double_semantics(const unsigned code, const uint64_t value, const uint64_t default_value) const;
};

class CBaseMap
{
	public:

		CBaseMap();

		virtual void map_to_scene(const CScale & scale) = 0;
		virtual void print() = 0;

		virtual CPoint min_map() const = 0;
		virtual CPoint max_map() const = 0;
		virtual CPoint min_gk() const = 0;
		virtual CPoint max_gk() const = 0;
};

class CMap : public CBaseMap
{
	public:

		vector<CObject> obj;

		CMap();

		void map_to_scene(const CScale & scale);
		void create_texture(const CScale & scale, const unsigned texture_size);
};

class CHeightMap : public CBaseMap
{
	public:

		vector< vector<CPoint> > pnt;
		vector< vector<CPoint> > pnt_scene;

		CHeightMap();

		void map_to_scene(const CScale & scale);
};

#endif

