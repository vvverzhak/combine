
#include "map/map.hpp"

void CObject::init(const EObjectType __type, const unsigned __id, const unsigned __code, const unsigned __localization)
{
	id = __id;
	code = __code;
	localization = __localization;
	type = __type;
}

void CObject::init(const vector<CPoint> __pnt, const EObjectType __type, const unsigned __id, const unsigned __code, const unsigned __localization, const string __label)
{
	id = __id;
	code = __code;
	localization = __localization;
	type = __type;
	label = __label;
	pnt = __pnt;
}

CObject & CObject::operator=(const CObject & obj)
{
	pnt = obj.pnt;
	sub_obj = obj.sub_obj;
	type = obj.type;
	code = obj.code;
	localization = obj.localization;
	label = obj.label;

	return * this;
}

void CObject::map_to_scene(const CScale & scale)
{
	for(auto & it : pnt)
		pnt_scene.push_back(scale.map_to_scene(it));

	for(auto & it : sub_obj)
		it.map_to_scene(scale);
}

void CObject::add_semantics(const unsigned code, const CSemantics<string> & sem)
{
	string_semantics[code] = sem;
};

void CObject::add_semantics(const unsigned code, const CSemantics<double> & sem)
{
	double_semantics[code] = sem;
};

string CObject::semantics_value(const unsigned code, const string default_value) const
{
	if(string_semantics.count(code))
		return string_semantics.at(code).value;

	return default_value;
}

double CObject::semantics_value(const unsigned code, const double default_value) const
{
	if(double_semantics.count(code))
		return double_semantics.at(code).value;

	return default_value;
}

int CObject::cmp_double_semantics(const unsigned code, const uint64_t value, const uint64_t default_value) const
{
	if(double_semantics.count(code))
	{
		const CSemantics<double> & sem = double_semantics.at(code);
		const ESemanticsType type = sem.type;
		uint64_t __value = value;
		double value_d = * ((double *) & __value);

		return sem.cmp(CSemantics<double>(value_d, type));
	}
	else if(string_semantics.count(code))
	{
		throw_;
	}
	else
	{
		// TODO default_value
	}

	return 0;
}

// ############################################################################ 

CBaseMap::CBaseMap()
{
	;
}

// ############################################################################ 

CMap::CMap() :
	CBaseMap()
{
	;
}

void CMap::map_to_scene(const CScale & scale)
{
	for(auto & it : obj)
		it.map_to_scene(scale);
}

// ############################################################################ 

CHeightMap::CHeightMap() :
	CBaseMap()
{
	;
}

void CHeightMap::map_to_scene(const CScale & scale)
{
	const unsigned height = pnt.size(), width = pnt[0].size();
	unsigned v, u;

	pnt_scene.resize(height);

	for(v = 0; v < height; v++)
		for(u = 0; u < width; u++)
			pnt_scene[v].push_back(scale.map_to_scene(pnt[v][u]));
}

