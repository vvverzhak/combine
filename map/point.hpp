
#ifndef POINT_HPP
#define POINT_HPP

#include "base/base.hpp"

class CVec3d
{
	public:

		double x, y, z;

		CVec3d();
		CVec3d(const CVec3d & v);
		CVec3d(const double t_x, const double t_y, const double t_z);

		const CVec3d & operator=(const CVec3d & v);
		double operator[](const unsigned ind) const;
		double & operator[](const unsigned ind);
		const CVec3d operator+(const CVec3d & v);
		const CVec3d & operator+=(const CVec3d & v);
		const CVec3d operator-(const CVec3d & v);

		static double distance(const CVec3d & op_1, const CVec3d & op_2);
};

typedef CVec3d CPoint;

class CGeoPoint : public CPoint
{
	public:

		double & lat = x, & lon = y, & h = z;

		CGeoPoint();
		CGeoPoint(const double t_lat, const double t_lon, const double t_h);
		CGeoPoint(const CGeoPoint & gp);

		const CGeoPoint & operator=(const CGeoPoint & gpnt);
		const CGeoPoint & operator=(const CPoint & pnt);
};

class CEulerAngle : public CVec3d
{
	public:

		double & course = x, & roll = y, & pitch = z;

		CEulerAngle();
		CEulerAngle(const double t_course, const double t_roll, const double t_pitch);
		CEulerAngle(const CEulerAngle & ea);

		const CEulerAngle & operator=(const CEulerAngle & v);
};

#endif

