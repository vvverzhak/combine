
#ifndef SCALE_HPP
#define SCALE_HPP

#include "base/base.hpp"
#include "map/point.hpp"

class CScale
{
	CPoint map_a, map_b;
	CPoint gk_a, gk_b;

	public:

		const CPoint gk_from, gk_to, map_from, map_to, scene_from, scene_to;

		CScale(const CPoint & t_gk_from, const CPoint & t_gk_to, const CPoint & t_map_from, const CPoint & t_map_to, const CPoint & t_scene_from, const CPoint & t_scene_to, const bool is_xy_proportional);

		CPoint scene_to_gk(const CPoint & pnt) const;
		CPoint gk_to_scene(const CPoint & pnt) const;
		CPoint scene_to_map(const CPoint & pnt) const;
		CPoint map_to_scene(const CPoint & pnt) const;
};

#endif

