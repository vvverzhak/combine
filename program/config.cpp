
#include "program/config.hpp"

#define CHECK_BOOL(fun, var)\
bool fun()\
{\
	return getenv(var) != NULL;\
}

#define CHECK_INT(fun, var, dflt, from, to, ret_type)\
ret_type fun()\
{\
	const char * value = getenv(var);\
\
	if(value)\
	{\
		int val = atoi(value);\
\
		if(val >= from && val <= to)\
			return val;\
	}\
\
	return dflt;\
}

#define GET_STRING(fun, var, dflt)\
string fun()\
{\
	const char * value = getenv(var);\
\
	if(value)\
		return value;\
\
	return dflt;\
}

CHECK_BOOL(is_fps, "COMBINE_FPS");
CHECK_BOOL(is_print_coord, "COMBINE_PRINT_COORD");
CHECK_BOOL(is_polygon_as_a_line, "COMBINE_IS_POLYGON_AS_A_LINE");
CHECK_BOOL(is_send_frame, "COMBINE_SEND_FRAME");

EIOType io_type()
{
	const char * var = getenv("COMBINE_IO");

	if(var)
	{
		if(! strcmp(var, "screen"))
			return EIOT_SCREEN;

		if(! strcmp(var, "udp"))
			return EIOT_UDP;

		if(! strcmp(var, "lab7_udp"))
			return EIOT_LAB7_UDP;

		if(! strcmp(var, "animate"))
			return EIOT_ANIMATE;
	}
	
	return EIOT_SCREEN; // По-умолчанию, все выводится на экран хостовой системы
}

CHECK_INT(deblurring_port, "COMBINE_DEBLURRING_PORT", 1988, 1, 65535, uint16_t);

GET_STRING(map_fname, "COMBINE_MAP_FNAME", "/home/amv/data/Полет/maps/N3716/N3716.sxf");
GET_STRING(height_map_fname, "COMBINE_HEIGHT_MAP_FNAME", "/home/amv/data/Полет/maps/N3716/N3716.mtr");
GET_STRING(classifier_fname, "COMBINE_CLASSIFIER_FNAME", "/home/amv/task/combine_sh_linux/data/flight/data/data.rsc");
GET_STRING(send_frame_destination_ip, "COMBINE_SEND_FRAME_DESTINATION_IP", "192.168.1.255");
GET_STRING(local_param_fname, "COMBINE_LOCAL_PARAM_FNAME", "/tmp/param");
GET_STRING(font_fname, "COMBINE_FONT_FNAME", "/usr/share/fonts/TTF/times.ttf");
GET_STRING(save_frame_fname, "COMBINE_FRAME_FNAME", "");

int port()
{
	const char * var = getenv("COMBINE_PORT");

	if(var)
		return atoi(var);

	return 7777;
}

int send_frame_destination_port()
{
	const char * var = getenv("COMBINE_SEND_FRAME_DESTINATION_PORT");

	if(var)
		return atoi(var);

	return 50027;
}

