
#ifndef MAIN_LOOP_HPP
#define MAIN_LOOP_HPP

#include "base/base.hpp"
#include "base/protocol.hpp"
#include "display/display.hpp"
#include "driver/sxf.hpp"
#include "driver/mtr.hpp"
#include "camera/camera.hpp"
#include "bad_code/bad_code.hpp"
#include "program/config.hpp"

class CMainLoop
{
	void send_frame();
	void save_frame();

	protected:

		CDisplay * disp;
		const CMap & map;
		const CHeightMap & height_map;
		CCamera & camera;

		void draw();
		void redraw();

	public:

		CMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera);
		virtual ~CMainLoop();

		virtual void run() = 0;
};

class CScreenMainLoop : public CMainLoop
{
	public:

		CScreenMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera);
		~CScreenMainLoop();

		void run();
};

class CUDPMainLoop : public CMainLoop
{
	int sock;
	sockaddr_in saddr, daddr;

	public:

		CUDPMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera);
		~CUDPMainLoop();

		void run();
};

class CLab7UDPMainLoop : public CMainLoop
{
	int sock;
	sockaddr_in saddr, daddr;

	public:

		CLab7UDPMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera);
		~CLab7UDPMainLoop();

		void run();
};

class CAnimateMainLoop : public CMainLoop
{
	public:

		CAnimateMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera);
		~CAnimateMainLoop();

		void run();
};

shared_ptr<CMainLoop> get_main_loop(CDisplay * disp, const CMap & map, const CHeightMap & height_map, CCamera & camera);

#endif

