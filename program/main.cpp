
/*
 * x86, x86_64 - little endian
 * mips - big endian
 */

// TODO Индексированные текстуры
// TODO Оценить время инициализации
// #define USING_CAIRO

#include "base/base.hpp"
#include "base/protocol.hpp"
#include "display/display.hpp"
#include "renderer/gpu/renderer.hpp"
#include "renderer/classifier/renderer.hpp"
#include "renderer/draw_engine/draw_engine.hpp"
#include "renderer/draw_engine/draw_engine_opengl.hpp"
#include "driver/sxf.hpp"
#include "driver/mtr.hpp"
#include "palette/my_palette.hpp"
#include "palette/thin_line_palette.hpp"
#include "palette/black_white_palette.hpp"
#include "camera/camera.hpp"
#include "program/main_loop.hpp"
#include "algo/algo.hpp"

#ifdef USING_CAIRO

	#include "renderer/draw_engine/draw_engine_cairo.hpp"

#endif

int main()
{
	CPoint gk_min, gk_max, map_min, map_max, height_map_min, height_map_max;
	shared_ptr<CDisplay> disp;
	shared_ptr<CMainLoop> main_loop;
	CSXFMap map(map_fname());
	const vector<unsigned> texture_step = { 11, 13, 15 };
//	const vector<unsigned> texture_step = { 15 };

	map.print();

	map_min = map.min_map();
	map_max = map.max_map();
	map_min.z = 0;
	map_max.z = MAX_H;

	gk_min = map.min_gk();
	gk_max = map.max_gk();
	gk_min.z = 0;
	gk_max.z = MAX_H;

	CMTRMap height_map(height_map_fname(), map.min_gk(), map.max_gk(), 1500);

	height_map_min = height_map.min_map();
	height_map_max = height_map.max_map();
	height_map_min.z = 0;
	height_map_max.z = MAX_H;

	CScale scale_map(gk_min, gk_max, map_min, map_max, CPoint(-1, -1, -1), CPoint(1, 1, 1), true);
	CScale scale_height_map(height_map_min, height_map_max, height_map_min, height_map_max, CPoint(-1, -1, -1), CPoint(1, 1, 1), true);
	// CCamera camera(map, scale_map, CPoint(0, 0, -0.9), CEulerAngle(0, 0, 0), 20, 20, ECT_GL);
	// CCamera camera(map, scale_map, CPoint(0.118282, -0.120045, 0.3), CEulerAngle(0, 0, -90), 43, 27, ECT_GL);
	// CCamera camera(map, scale_map, CPoint(0.118282, -0.120045, 0.3), CEulerAngle(0, 0, -90), 44.9, 26.6, ECT_GL);
	// CCamera camera(map, scale_map, CPoint(0.118282, -0.120045, 0.3), CEulerAngle(0, 0, -90), 44.9, 26.6, ECT_GL);
	// CCamera camera(map, scale_map, CPoint(7242.392306, 5096.350272, 1017.868530), CEulerAngle(56, 0, 0), 30, 25, ECT_MAP);
	// CCamera camera(map, scale_map, CPoint(7207.267272, 5126.726744, 942.443481), CEulerAngle(119.886576, -0.900108, -11.167869), 20, 25, ECT_MAP);
	// CCamera camera(map, scale_map, CPoint(0.118282, -0.120045, 0.3), CEulerAngle(0, 0, -90), 44.9, 26.6, ECT_GL);
	// CCamera camera(map, scale_map, CPoint(6059415.391560, 7550393.798301, 1218.721998), CEulerAngle(120.043537, 1.395106, -9.781263), 44.9, 26.6, ECT_GAUSS_KRUEGER);
	CCamera camera(map, scale_map, CPoint(6051948.581560, 7554127.203301, 7940.641998), CEulerAngle(260.043537, 1.395106, -59.781263), 44.9, 26.6, ECT_GAUSS_KRUEGER);
	// CCamera camera(map, scale_map, CPoint(6055681.986560, 7550393.798301, 18396.961998), CEulerAngle(260.043537, 0, -90), 44.9, 26.6, ECT_GAUSS_KRUEGER);
	// CCamera camera(map, scale_map, CPoint(6044481.771560, 7550393.798301, 2339.041998), CEulerAngle(260.043537, 1.395106, -29.781263), 44.9, 26.6, ECT_GAUSS_KRUEGER);

	map.map_to_scene(scale_map);
	height_map.map_to_scene(scale_height_map);

	// disp = create_display(1200, 1600);
	disp = create_display(1024, 1024); // Для send_frame()
	// disp = create_display(1050, 1400);
	// disp = create_display(576, 704);
	// disp = create_display(1400, 1400);
	// disp = create_display(576, 720);

#ifndef EMBEDDED

	throw_if(glewInit() != GLEW_OK);

#endif

#if 1

	CMyPalette palette;

#elif 0

	CThinLinePalette palette;

#else

	CBlackWhitePalette palette;

#endif

#ifdef USING_CAIRO

	CCairoDrawEngine draw_engine(palette);

#else

	COpenGLDrawEngine draw_engine(palette);

#endif

#if 0

	CGPURenderer renderer(
		disp->height,
		disp->width,
		// 4096,	// texture_size
		// 1024,	// texture_size
		2048,		// texture_size
		6,			// texture_per_row
		texture_step,
		map, height_map, camera, scale_map, palette);

#else

	CClassifierRenderer renderer(
		classifier_fname(),
		disp->height,
		disp->width,
		2048,		// texture_size
		5,			// texture_per_row
		// 2048,	// texture_size
		// 5,		// texture_per_row
		texture_step,
		map, height_map, camera, scale_map, palette);

#endif

	CAlgo::init(disp->height, disp->width);

	disp->set_renderer(& renderer);
	
	main_loop = get_main_loop(disp.get(), map, height_map, camera);
	main_loop->run();

	CAlgo::destroy();

	return 0;
}

