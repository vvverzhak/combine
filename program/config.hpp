
#ifndef PROGRAM_CONFIG_HPP
#define PROGRAM_CONFIG_HPP

#include "base/base.hpp"
#include "base/config.hpp"

enum EIOType
{
	EIOT_SCREEN, EIOT_UDP, EIOT_LAB7_UDP, EIOT_ANIMATE
};

EIOType io_type();
bool is_fps();
bool is_send_frame();
int port();
string send_frame_destination_ip();
int send_frame_destination_port();
string save_frame_fname();

#endif

