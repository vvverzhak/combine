
#include "program/main_loop.hpp"

CMainLoop::CMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera) :
	map(t_map), height_map(t_height_map), camera(t_camera)
{
	disp = t_disp;
}

CMainLoop::~CMainLoop()
{
	;
}

void CMainLoop::redraw()
{
	const bool t_is_fps = is_fps();
	double fps;
	timespec begin_time, end_time;

	if(t_is_fps)
		clock_gettime(CLOCK_MONOTONIC, & begin_time);

	disp->redraw();

	if(t_is_fps)
	{
		clock_gettime(CLOCK_MONOTONIC, & end_time);
				
		if(fabs(fps = end_time.tv_sec - begin_time.tv_sec + (end_time.tv_nsec - begin_time.tv_nsec) / 1000000000.) > 0.)
			printf("%.3lf FPS\n", 1 / fps);
	}

	send_frame();
	save_frame();
	DEBUG;
};

void CMainLoop::send_frame()
{
	if(is_send_frame())
	{
		int sock = -1;

		try
		{
			timespec begin_time, end_time;

			clock_gettime(CLOCK_MONOTONIC, & begin_time);

			const int broadcast_enable = 1;
			const unsigned height = disp->height, width = disp->width;
			const unsigned width_2 = width * 2;
			const unsigned max_part_size = 8192, size = height * width;
			const unsigned size_2 = size * 2;
			const unsigned last_part_size = size_2 % max_part_size;
			int v, u;
			shared_ptr<uint8_t> buf(new uint8_t [max_part_size + 3], std::default_delete<uint8_t[]>());
			shared_ptr<uint8_t> frame(new uint8_t[size_2 + 3], std::default_delete<uint8_t[]>());
			shared_ptr<uint8_t> frame_rev(new uint8_t[size_2 + 3], std::default_delete<uint8_t[]>());
			uint8_t * p_buf = buf.get(), * p_frame = frame.get(), * p_frame_rev = frame_rev.get();
			sockaddr_in addr;

			throw_null(p_frame);
			throw_null(p_frame_rev);
			disp->read_frame(p_frame_rev + 3, GL_RGB, GL_UNSIGNED_SHORT_5_6_5);

			for(v = 0; v < size_2; v += 2)
			{
				uint8_t temp = p_frame_rev[v];

				p_frame_rev[v] = p_frame_rev[v + 1];
				p_frame_rev[v + 1] = temp;
			}

			for(v = height - 1, u = 0; v >= 0; v--, u++)
				memcpy(p_frame + u * width_2, p_frame_rev + v * width_2, width_2);

			CFile::pack<uint16_t>((uint16_t *) p_frame, size);

			addr.sin_family = AF_INET;
			addr.sin_port = htons(send_frame_destination_port());
			addr.sin_addr.s_addr = inet_addr(send_frame_destination_ip().c_str());

			throw_if((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1);
			throw_if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, & broadcast_enable, sizeof(broadcast_enable)) == -1);

			auto send_frame = [ sock, addr, & p_buf ](const uint8_t prefix, const unsigned frame_size)
			{
				p_buf[0] = prefix;
				throw_if(sendto(sock, p_buf, frame_size, 0, (sockaddr *) & addr, sizeof(addr)) != frame_size);
			};

			// ############################################################################ 
			// Первый фрейм

			const unsigned first_frame_size = 64;

			memset(p_buf, 0, first_frame_size); // Не нужно, но сделаем valgrind'у приятно
			// CFile::pack<uint8_t>(p_buf, first_frame_size); TODO Не нужно, убрать

			send_frame(0xFF, first_frame_size);

			// ############################################################################ 
			// Остальные фреймы

			auto send_part = [ & p_buf, & p_frame, sock, addr, send_frame ](const unsigned from, const unsigned part_size)
			{
				const unsigned frame_size = part_size + 3;

				p_buf = p_frame + from;
				p_buf[1] = part_size & 0xFF;
				p_buf[2] = (part_size >> 8) & 0xFF;

				send_frame(0xFE, frame_size);
			};

			for(v = 0; v < size_2; v += max_part_size)
				send_part(v, max_part_size);

			if(last_part_size)
				send_part(size_2 - last_part_size, last_part_size);

			clock_gettime(CLOCK_MONOTONIC, & end_time);
			printf_TODO("Передача = %lf с.\n", end_time.tv_sec - begin_time.tv_sec + (end_time.tv_nsec - begin_time.tv_nsec) / 1000000000.);
		}
		catch(...)
		{
			printf_TODO("Не удалось отправить кадр\n");
		}

		if(sock != -1)
			close(sock);
	}
}

void CMainLoop::save_frame()
{
	const string fname = save_frame_fname();

	if(! fname.empty())
	{
		const unsigned height = disp->height, width = disp->width;
		const unsigned line_size = width * 3;
		// const unsigned line_size_2 = width * 2;
		const unsigned size = height * line_size;
		// const unsigned size_2 = height * line_size_2;
		int v, u, t, t_2;
		shared_ptr<uint8_t> img(new uint8_t[size], std::default_delete<uint8_t[]>());
		shared_ptr<uint8_t> frame(new uint8_t[size], std::default_delete<uint8_t[]>());
		shared_ptr<uint8_t> frame_rev(new uint8_t[size], std::default_delete<uint8_t[]>());
		// shared_ptr<uint8_t> frame(new uint8_t[size_2], std::default_delete<uint8_t[]>());
		// shared_ptr<uint8_t> frame_rev(new uint8_t[size_2], std::default_delete<uint8_t[]>());
		uint8_t * p_img = img.get(), * p_frame = frame.get(), * p_frame_rev = frame_rev.get();

		throw_null(p_frame);
		throw_null(p_frame_rev);

		disp->read_frame(p_frame_rev + 3, GL_RGB, GL_UNSIGNED_BYTE);
		// disp->read_frame(p_frame_rev + 3, GL_RGB, GL_UNSIGNED_SHORT_5_6_5);

		/*
		for(v = height - 1, u = 0; v >= 0; v--, u++)
			memcpy(p_frame + u * line_size_2, p_frame_rev + v * line_size_2, line_size_2);

		for(v = 0, t = 0, t_2 = 0; v < height; v++)
			for(u = 0; u < width; u++, t += 3, t_2 += 2)
			{
				p_img[t + 2] = p_frame[t_2] & 0xF8;
				p_img[t + 1] = (((p_frame[t_2] & 0x7) << 3) + ((p_frame[t_2 + 1] & 0xE) >> 5)) << 2;
				p_img[t] = (p_frame[t_2 + 1] & 0x1F) << 3;
			}
		*/

		for(v = 0, t = 0; v < height; v++)
			for(u = 0; u < width; u++, t += 3)
				swap(p_frame_rev[t], p_frame_rev[t + 2]);

		for(v = height - 1, u = 0; v >= 0; v--, u++)
			memcpy(p_img + u * line_size, p_frame_rev + v * line_size, line_size);

		// TODO
		// save_bmp(fname, img, height, width, 3);
	}
}

// ############################################################################ 

CScreenMainLoop::CScreenMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera) :
	CMainLoop(t_disp, t_map, t_height_map, t_camera)
{
	;
}

CScreenMainLoop::~CScreenMainLoop()
{
	;
}

void CScreenMainLoop::run()
{
	char ch;

	redraw();

	while(1)
		switch(disp->wait_for_event(ch))
		{
			case EET_EXIT:
				return;
			case EET_REDRAW:
			{
				redraw();
				
				break;
			}
			case EET_KEYBOARD:
			{

#define PAIR(top, bottom, param, step)\
	case top:\
	{\
		param += step;\
		camera.eval(ECT_GL);\
		redraw();\
		\
		break;\
	}\
	case bottom:\
	{\
		param -= step;\
		camera.eval(ECT_GL);\
		redraw();\
		\
		break;\
	}

				switch(ch)
				{
					PAIR('q', 'a', camera.pos.x, 0.1)
					PAIR('w', 's', camera.pos.y, 0.1)
					PAIR('e', 'd', camera.pos.z, 0.01)
					PAIR('r', 'f', camera.euler_angle.course, 10)
					PAIR('t', 'g', camera.euler_angle.roll, 10)
					PAIR('y', 'h', camera.euler_angle.pitch, 10)
					PAIR('u', 'j', camera.aspect_x, 10)
					PAIR('i', 'k', camera.aspect_y, 10)

					case 'z':
					{
						redraw();

						break;
					}
				}
			
				break;
			}
			case EET_NOTHING:
			{
				break;
			}
		}
}

// ############################################################################ 

CUDPMainLoop::CUDPMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera) :
	CMainLoop(t_disp, t_map, t_height_map, t_camera)
{
	const int t_port = port();

	sock = -1;

	throw_if((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1);
	throw_if(t_port <= 0 || t_port >= 65536);

	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(t_port);
	saddr.sin_addr.s_addr = INADDR_ANY;

	throw_if(bind(sock, (sockaddr *) & saddr, sizeof(saddr)));
}

CUDPMainLoop::~CUDPMainLoop()
{
	if(sock != -1)
		close(sock);
}

void CUDPMainLoop::run()
{
	SPacketHeader header;
	void * ptr;
	shared_ptr<uint8_t> buf;
	socklen_t slen;

	redraw();

	while(true)
	{
		try
		{
			slen = sizeof(daddr);
			throw_if(recvfrom(sock, & header, sizeof(header), 0, (sockaddr *) & daddr, & slen) != sizeof(header));
			CFile::unpack<uint32_t>((uint32_t *) & header, 3);

			daddr.sin_port = htons(header.port);

			if(header.size > 0)
			{
				buf.reset(new uint8_t[header.size], std::default_delete<uint8_t[]>());
				ptr = (void *) buf.get();

				throw_if(recv(sock, ptr, header.size, 0) != header.size);
			}

			switch(header.command)
			{
				case COMMAND_EXIT:
					return;
				case COMMAND_PING:
				{
					SPacketPong pong = { .pong = PONG };

					CFile::pack<uint32_t>(& pong.pong);
					throw_if(sendto(sock, & pong, sizeof(pong), 0, (sockaddr *) & daddr, sizeof(daddr)) != sizeof(pong));
					
					break;
				}
				case COMMAND_REDRAW:
				{
					redraw();

					break;
				}

				// ############################################################################ 

#define GET_SET(get_command, set_command, gs_value, ect)\
				case get_command:\
				{\
					SPacketGetSet packet = { .value = gs_value };\
\
					CFile::pack<double>((double *) & packet);\
					throw_if(sendto(sock, & packet, sizeof(packet), 0, (sockaddr *) & daddr, sizeof(daddr)) != sizeof(packet));\
\
					break;\
				}\
				case set_command:\
				{\
					gs_value = ((SPacketGetSet *) ptr)->value;\
					CFile::unpack<double>((double *) & gs_value);\
					camera.eval(ect);\
\
					redraw();\
\
					break;\
				}

				GET_SET(COMMAND_GET_X, COMMAND_SET_X, camera.pos.x, ECT_GL);
				GET_SET(COMMAND_GET_Y, COMMAND_SET_Y, camera.pos.y, ECT_GL);
				GET_SET(COMMAND_GET_H, COMMAND_SET_H, camera.pos.z, ECT_GL);
				GET_SET(COMMAND_GET_COURSE, COMMAND_SET_COURSE, camera.euler_angle.course, ECT_GL);
				GET_SET(COMMAND_GET_ROLL, COMMAND_SET_ROLL, camera.euler_angle.roll, ECT_GL);
				GET_SET(COMMAND_GET_PITCH, COMMAND_SET_PITCH, camera.euler_angle.pitch, ECT_GL);
				GET_SET(COMMAND_GET_ASPECT_X, COMMAND_SET_ASPECT_X, camera.aspect_x, ECT_GL);
				GET_SET(COMMAND_GET_ASPECT_Y, COMMAND_SET_ASPECT_Y, camera.aspect_y, ECT_GL);

				case COMMAND_GET_TIME:
				{
					const time_t t_time = time(NULL);
					const tm * cur_time = localtime(& t_time);
					SPacketTime packet;
					
					packet.hour = cur_time->tm_hour;
					packet.min = cur_time->tm_min;
					packet.sec = cur_time->tm_sec;

					CFile::pack<uint16_t>((uint16_t *) & packet, 3);

					throw_if(sendto(sock, & packet, sizeof(packet), 0, (sockaddr *) & daddr, sizeof(daddr)) != sizeof(packet));

					break;
				}
				case COMMAND_SET_ALL_GL:
				{
					SPacketSetAll * set_all_header = (SPacketSetAll *) ptr;

					CFile::unpack<double>((double *) ptr, 6);

					camera.pos.x = set_all_header->x;
					camera.pos.y = set_all_header->y;
					camera.pos.z = set_all_header->h;
					camera.euler_angle.course = set_all_header->course;
					camera.euler_angle.roll = set_all_header->roll;
					camera.euler_angle.pitch = set_all_header->pitch;

					camera.eval(ECT_GL);
					
					redraw();

					break;
				}
				case COMMAND_SET_ALL_GEO:
				{
					SPacketSetAll * set_all_header = (SPacketSetAll *) ptr;

					CFile::unpack<double>((double *) ptr, 6);

					camera.geo_pos.x = set_all_header->x;
					camera.geo_pos.y = set_all_header->y;
					camera.geo_pos.z = set_all_header->h;
					camera.euler_angle.course = set_all_header->course;
					camera.euler_angle.roll = set_all_header->roll;
					camera.euler_angle.pitch = set_all_header->pitch;

					camera.eval(ECT_GEO);
					
					redraw();

					break;
				}
				case COMMAND_SET_ALL_GAUSS_KRUEGER:
				{
					SPacketSetAll * set_all_header = (SPacketSetAll *) ptr;

					CFile::unpack<double>((double *) ptr, 6);

					camera.gauss_krueger_pos.x = set_all_header->x;
					camera.gauss_krueger_pos.y = set_all_header->y;
					camera.gauss_krueger_pos.z = set_all_header->h;
					camera.euler_angle.course = set_all_header->course;
					camera.euler_angle.roll = set_all_header->roll;
					camera.euler_angle.pitch = set_all_header->pitch;

					camera.eval(ECT_GAUSS_KRUEGER);
					
					redraw();

					break;
				}

				// ############################################################################ 

				default:
					throw_;
			}
		}
		catch(...)
		{
			;
		}
	}
}

// ############################################################################ 

CLab7UDPMainLoop::CLab7UDPMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera) :
	CMainLoop(t_disp, t_map, t_height_map, t_camera)
{
	const int t_port = port();

	sock = -1;

	throw_if((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1);
	throw_if(t_port <= 0 || t_port >= 65536);

	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(t_port);
	saddr.sin_addr.s_addr = INADDR_ANY;

	throw_if(bind(sock, (sockaddr *) & saddr, sizeof(saddr)));
}

CLab7UDPMainLoop::~CLab7UDPMainLoop()
{
	if(sock != -1)
		close(sock);
}

void CLab7UDPMainLoop::run()
{
	SGaginPacket packet;
	socklen_t slen;

	redraw();

	while(true)
	{
		try
		{
			slen = sizeof(daddr);
			throw_if(recvfrom(sock, & packet, sizeof(packet), 0, (sockaddr *) & daddr, & slen) != sizeof(packet));

			CFile::unpack<uint32_t>((uint32_t *) & packet.Head);
			CFile::unpack<uint32_t>((uint32_t *) & packet.FrameNumber);
			CFile::unpack<double>(& packet.Latitude);
			CFile::unpack<double>(& packet.Longitude);
			CFile::unpack<double>(& packet.Height);
			CFile::unpack<double>(& packet.HeightBarometric);
			CFile::unpack<double>(& packet.Course);
			CFile::unpack<double>(& packet.CourseComplex);
			CFile::unpack<double>(& packet.CourseMagnetic);
			CFile::unpack<double>(& packet.Roll);
			CFile::unpack<double>(& packet.Tangage);
			CFile::unpack<float>(& packet.V_TV);
			CFile::unpack<float>(& packet.H_TV);
			CFile::unpack<float>(& packet.V_CTV);
			CFile::unpack<float>(& packet.H_CTV);
			CFile::unpack<float>(& packet.V_EFA);
			CFile::unpack<float>(& packet.H_EFA);
			CFile::unpack<float>(& packet.V_FLIR);
			CFile::unpack<float>(& packet.H_FLIR);
			CFile::unpack<double>(& packet.Snos);
			CFile::unpack<double>(& packet.VComp);
			CFile::unpack<double>(& packet.VN);
			CFile::unpack<double>(& packet.VE);
			CFile::unpack<double>(& packet.VH);
			CFile::unpack<uint32_t>((uint32_t *) & packet.WorkCycles);
			CFile::unpack<double>(& packet.AngVelX);
			CFile::unpack<double>(& packet.AngVelY);
			CFile::unpack<double>(& packet.AngVelZ);
			CFile::unpack<double>(& packet.ApparentAccelerationX);
			CFile::unpack<double>(& packet.ApparentAccelerationY);
			CFile::unpack<double>(& packet.ApparentAccelerationZ);
			CFile::unpack<uint16_t>((uint16_t *) & packet.CheckSum);

			camera.geo_pos.lat = packet.Latitude;
			camera.geo_pos.lon = packet.Longitude;
			camera.geo_pos.h = packet.Height;
			camera.euler_angle.course = packet.Course;
			camera.euler_angle.roll = packet.Roll;
			camera.euler_angle.pitch = packet.Tangage;

//			camera.aspect_x = packet.H_CTV;
//			camera.aspect_y = packet.V_CTV;

			camera.eval(ECT_GEO);
					
			redraw();
		}
		catch(...)
		{
			;
		}
	}
}

// ############################################################################ 

CAnimateMainLoop::CAnimateMainLoop(CDisplay * t_disp, const CMap & t_map, const CHeightMap & t_height_map, CCamera & t_camera) :
	CMainLoop(t_disp, t_map, t_height_map, t_camera)
{
	;
}

CAnimateMainLoop::~CAnimateMainLoop()
{
	;
}

void CAnimateMainLoop::run()
{
	useconds_t delay = 1000;
	double step = 0.01;

	while(true)
	{
		redraw();

		switch(rand() % 3)
		{
			case 0:
			{
				camera.pos.x += (rand() % 2 ? 1 : -1) * step;

				break;
			}
			case 1:
			{
				camera.pos.y += (rand() % 2 ? 1 : -1) * step;

				break;
			}
			case 2:
			{
				camera.pos.z += (rand() % 2 ? 1 : -1) * step;

				break;
			}
		};

		if(fabs(camera.pos.x) > 1)
			camera.pos.x = camera.pos.x < 0 ? -1 : 1;

		if(fabs(camera.pos.y) > 1)
			camera.pos.y = camera.pos.y < 0 ? -1 : 1;

		if(fabs(camera.pos.z) > 1)
			camera.pos.z = camera.pos.z < 0 ? -1 : 1;

		usleep(delay);
	}
}

// ############################################################################ 

shared_ptr<CMainLoop> get_main_loop(CDisplay * disp, const CMap & map, const CHeightMap & height_map, CCamera & camera)
{
	shared_ptr<CMainLoop> main_loop;

	switch(io_type())
	{
		case EIOT_SCREEN:
		{
			main_loop.reset(new CScreenMainLoop(disp, map, height_map, camera));

			break;
		}
		case EIOT_UDP:
		{
			main_loop.reset(new CUDPMainLoop(disp, map, height_map, camera));

			break;
		}
		case EIOT_LAB7_UDP:
		{
			main_loop.reset(new CLab7UDPMainLoop(disp, map, height_map, camera));

			break;
		}
		case EIOT_ANIMATE:
		{
			main_loop.reset(new CAnimateMainLoop(disp, map, height_map, camera));

			break;
		}
		default:
		{
			throw_;
		}
	}

	throw_null(main_loop.get());

	return main_loop;
}

