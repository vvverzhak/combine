
#include "bad_code/bad_code.hpp"

#ifdef EMBEDDED

void gl_ortho(const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat near, const GLfloat far)
{
	GLfloat matrix[16];

#define SET_ELEM(matrix, v, u, value) \
	matrix[u * 4 + v] = value;

	SET_ELEM(matrix, 0, 0, 2. / (right - left));
	SET_ELEM(matrix, 0, 1, 0);
	SET_ELEM(matrix, 0, 2, 0);
	SET_ELEM(matrix, 0, 3, - (right + left) / (right - left));

	SET_ELEM(matrix, 1, 0, 0);
	SET_ELEM(matrix, 1, 1, 2. / (top - bottom));
	SET_ELEM(matrix, 1, 2, 0);
	SET_ELEM(matrix, 1, 3, - (top + bottom) / (top - bottom));

	SET_ELEM(matrix, 2, 0, 0);
	SET_ELEM(matrix, 2, 1, 0);
	SET_ELEM(matrix, 2, 2, - 2. / (far - near));
	SET_ELEM(matrix, 2, 3, - (far + near) / (far - near));

	SET_ELEM(matrix, 3, 0, 0);
	SET_ELEM(matrix, 3, 1, 0);
	SET_ELEM(matrix, 3, 2, 0);
	SET_ELEM(matrix, 3, 3, 1);

	glMultMatrixf(matrix);
}

void glu_perspective(const double aspect_y, const double aspect_xy, const double near, const double far)
{
	unsigned v, u;
    const double rad = aspect_y * M_PI / 360., dh = far - near;
	const double sn = sin(rad);
    GLfloat m[4][4];

    if(dh == 0 || sn == 0 || aspect_xy == 0)
        return;

    const double ctn = cos(rad) / sn;

	for(v = 0; v < 4; v++)
		for(u = 0; u < 4; u++)
			m[v][u] = (v == u) ? 1 : 0;

    m[0][0] = ctn / aspect_xy;
    m[1][1] = ctn;
    m[2][2] = - (far + near) / dh;
    m[2][3] = -1;
    m[3][2] = -2 * near * far / dh;
    m[3][3] = 0;

    glMultMatrixf(& m[0][0]);
}

#endif

// ############################################################################ 
// Географические координаты <-> координаты Гаусса - Крюгера
// (from Анатолий, сентябрь - октябрь 2013 г.)

void GetGausXY(long double B,long double L,long double& X,long double& Y)
{
	long double Br=(B)*M_PI/180.0;
   long double Lr=(L)*M_PI/180.0;
	long double sBr=sinl(Br);
	long double s2Br=sBr*sBr;
	long double s4Br=s2Br*s2Br;
	long double s6Br=s2Br*s4Br;
   long double cBr=cosl(Br);
   long double c2Br=cBr*cBr;
   long double an0,an2,an4,an6;
   long double l4;
   long double l,l2;

	int n=int((6+L)/6);
	l=(L-(3+6*(n-1)))*M_PI/180.0;
	l2=l*l;
   l4=l2*l2;

	X=6367558.4968*Br-sin(2*Br)*(16002.89+66.9607*s2Br+0.3515*s4Br-
	l2*(1594561.25+5336.535*s2Br+26.79*s4Br+0.149*s6Br+l2*(672483.4-
	811219.9*s2Br+5420*s4Br-10.6*s6Br+l2*(278194-830174*s2Br+
	572434*s4Br-16010*s6Br+l2*(109500-574700*s2Br+863700*s4Br-
	398600*s6Br)))));

	Y=(5+10*n)*100000+l*cosl(Br)*(6378245+21346.1415*s2Br+107.159*s4Br+
	0.5977*s6Br+l2*(1070204.16-2136826.66*s2Br+17.98*s4Br-11.99*s6Br+
	l2*(270806-1523417*s2Br+1327645*s4Br-21701*s6Br+l2*(79690-866190*s2Br+
	1730360*s4Br-945460*s6Br))));

}

void GetTrasformCoordsdBdL(long double &B,long double& L,long double& H,long double aa,long double ab,
long double alpa,long double alpb,long double dx,long double dy,long double dz,long double m,long double wx,long double wy,long double wz)
{
   long double dB,dL;
   long double M,N,e2,da,r,de2,a,eb2,ea2,a2;
   long double Br,Lr,sB,cB,sL,cL,s2B,dH;
   long double La,Ba;
   La=L;
   Ba=B;
   a=(aa+ab)/2;
   a2=a*a;
   da=ab-aa;
   eb2=2*alpb-alpb*alpb;
   ea2=2*alpa-alpa*alpa;
   de2=eb2-ea2;
   e2=(eb2+ea2)/2;
   r=(206264.8062*M_PI/180.0)/(3600.0);


	Br=B*M_PI/180.0;
	Lr=L*M_PI/180.0;
   sB=sinl(Br);
   cB=cosl(Br);
   sL=sinl(Lr);
   cL=cosl(Lr);
   s2B=sB*sB;

   M=a*(1-e2)/sqrt((1-e2*s2B)*(1-e2*s2B)*(1-e2*s2B));
   N=a/sqrt((1-e2*s2B));

   dB=((r/(M+N))*((N/a)*e2 *sB*cB*da+((N*N/a2)+1)*N*sB*cB*de2/2-(dx*cL+dy*sL)*sB+dz*cB)-
   wx*sL*(1+e2*cosl(2*Br))+wy*cL*(1+e2*cosl(2*Br))-r*m*e2*sB*cB);
   dB=dB*180/M_PI;

   dL=((r/((M+N)*cB))*(-1*dx*sL+dy*cL)+tanl(Br)*(1-e2)*(wx*cL+wy*sL)-wz) *180/M_PI;
   dH=-1*a*da/N+N*s2B*de2/2+(dx*cL+dy*sL)*cB+dz*sB-N*e2*sB*cB*(wx*sL/r-wy*cL/r)+(a2/N+H)*m;

   L=(L+(L+dL))/2;
   B=(B+(B+dB))/2;
   H=(H+(H+dH))/2;
	Br=B*M_PI/180.0;
	Lr=L*M_PI/180.0;
   sB=sin(Br);
   cB=cos(Br);
   sL=sin(Lr);
   cL=cos(Lr);
   s2B=sB*sB;

   M=a*(1-e2)/sqrt((1-e2*s2B)*(1-e2*s2B)*(1-e2*s2B));
   N=a/sqrt((1-e2*s2B));

   dB=((r/(M+N))*((N/a2) *sB*cB*da+((N*N/a2)+1)*N*sB*cB*de2/2-(dx*cL+dy*sL)*sB+dz*cB)-wx*sL*(1+e2*cos(2*Br))+wy*cL*(1+e2*cos(2*Br))-r*m*e2*sB*cB) *180/M_PI;
   dL=((r/((M+N)*cB))*(-1*dx*sL+dy*cL)+tan(Br)*(1-e2)*(wx*cL+wy*sL)-wz) *180/M_PI;
   dH=-1*a*da/N+N*s2B*de2/2+(dx*cL+dy*sL)*cB+dz*sB-N*e2*sB*cB*(wx*sL/r-wy*cL/r)+(a2/N+H)*m;

   L=L+dL;
   B=B+dB;
   H=H+dH;
}

void LatLon2XY2(long double B,long double L,long double H,long double &X,long double &Y,long double &H2)
{
   long double dB,dL;
   int n;
   long double alpa,alpb;
   long double M,N,e2,da,r,de2,dx,dy,dz,aa,ab,a,eb2,ea2,wx,wy,wz,m,a2,Br,Lr,sB,cB,sL,cL,s2B;


   Br=B*M_PI/180.0;Lr=L*M_PI/180.0;
   sB=sin(Br);cB=cos(Br);
   sL=sin(Lr);cL=cos(Lr);
   s2B=sB*sB;
  aa=6378137;alpa=1/298.257223563;
  ab=6378136;alpb=1/298.25784;

  dx=1.08; dy=0.27; dz=0.9;
  m=0.00000012;
  wx=0;wy=0;wz=0;//0.16*M_PI/(3600.0*180.0);

  GetTrasformCoordsdBdL(B,L,H,aa,ab,alpa,alpb,dx,dy,dz,m,wx,wy,wz);

  aa=6378136;alpa=1/298.25784;
  ab=6378245;alpb=1/298.3;

  dx=-25; dy=141;dz=80;
  m=0.00000025;
  wx=0; wy=0;wz=0.0;
  wy=0.0000007;  wz=-0.0000035;
  GetTrasformCoordsdBdL(B,L,H,aa,ab,alpa,alpb,dx,dy,dz,m,wx,wy,wz);

  GetGausXY(B,L,X,Y);
  H2=H;
}

