
#include "camera/camera.hpp"

CCamera::CCamera(const CMap & t_map, const CScale & t_scale, const CPoint & t_pos, const CEulerAngle & t_euler_angle, const double t_aspect_x, const double t_aspect_y, const ECoordType ect) :
	map(t_map), scale(t_scale)
{
	euler_angle = t_euler_angle;
	aspect_x = t_aspect_x;
	aspect_y = t_aspect_y;

	eval(ect, true, t_pos);
}

void CCamera::print() const
{
	if(is_print_coord())
		printf("\
Camera coord:\n\
\tscene_coord = [ %lf %lf %lf ]\n\
\tgauss_krueger_coord = [ %lf %lf %lf ]\n\
\tgeo_coord = [ %lf %lf %lf ]\n\
\teuler_angle =\n\
\t[\n\
\t\tcourse = %lf\n\
\t\troll = %lf\n\
\t\tpitch = %lf\n\
\t]\n\
\taspect =\n\
\t[\n\
\t\tx = %lf\n\
\t\ty = %lf\n\
\t]\n",
		pos.x, pos.y, pos.z,
		gauss_krueger_pos.x, gauss_krueger_pos.y, gauss_krueger_pos.z,
		geo_pos.lat, geo_pos.lon, geo_pos.h,
		euler_angle.course,
		euler_angle.roll,
		euler_angle.pitch,
		aspect_x,
		aspect_y);
}

void CCamera::set() const
{
	double _aspect_x = aspect_x, _aspect_y = aspect_y;
	FILE * _fl;
	CPoint _pos(pos), d_pos;
	CEulerAngle _euler_angle(euler_angle), d_euler_angle;

	if(
		(_fl = fopen(local_param_fname().c_str(), "r")) != NULL
	  )
	{
		fscanf(_fl, "_pos.x += %lf;\n_pos.y += %lf;\n_pos.z += %lf;\n_euler_angle.course += %lf;\n_euler_angle.roll += %lf;\n_euler_angle.pitch += %lf;\n_aspect_x = %lf;\n_aspect_y = %lf;\n",
				& d_pos.x, & d_pos.y, & d_pos.z, & d_euler_angle.course, & d_euler_angle.roll, & d_euler_angle.pitch, & _aspect_x, & _aspect_y);

		fclose(_fl);

		_pos += d_pos;
		_euler_angle += d_euler_angle;
	}

	// ############################################################################ 

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gl_ortho(scale.scene_from.x, scale.scene_to.x, scale.scene_from.z, scale.scene_to.z, scale.scene_from.y, scale.scene_to.y);

	glu_perspective(_aspect_y, _aspect_x / _aspect_y, 0, 2500000);

	// ############################################################################ 

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glRotatef(- _euler_angle.pitch, 1, 0, 0);
	glRotatef(_euler_angle.roll, 0, 0, 1);
	glRotatef(_euler_angle.course + 90, 0, 1, 0); // Докрутка на север

	glTranslatef(- _pos.x, - _pos.z, - _pos.y);
}

void CCamera::eval(const ECoordType ect, const bool is_init, const CPoint & t_pos)
{
	switch(ect)
	{
		case ECT_GL:
		{
			if(is_init)
				pos = t_pos;

			gauss_krueger_pos = scale.scene_to_gk(pos);
			geo_pos = gauss_krueger_to_geo(gauss_krueger_pos);

			break;
		}
		case ECT_GEO:
		{
			if(is_init)
				geo_pos = t_pos;

			gauss_krueger_pos = geo_to_gauss_krueger(geo_pos);
			pos = scale.gk_to_scene(gauss_krueger_pos);

			break;
		}
		case ECT_GAUSS_KRUEGER:
		{
			if(is_init)
				gauss_krueger_pos = t_pos;

			pos = scale.gk_to_scene(gauss_krueger_pos);
			geo_pos = gauss_krueger_to_geo(gauss_krueger_pos);

			break;
		}
	};
}

