
#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "base/base.hpp"
#include "map/point.hpp"
#include "map/map.hpp"
#include "map/scale.hpp"
#include "coord/coord.hpp"
#include "library/combine.h"

class CCamera
{
	const CMap & map;
	const CScale & scale;

	public:

		CPoint pos, gauss_krueger_pos;
		CGeoPoint geo_pos;
		CEulerAngle euler_angle;
		double aspect_x, aspect_y;

		CCamera(const CMap & t_map, const CScale & t_scale, const CPoint & t_pos, const CEulerAngle & t_euler_angle, const double t_aspect_x, const double t_aspect_y, const ECoordType ect);

		void print() const;
		void set() const;
		void eval(const ECoordType ect, const bool is_init = false, const CPoint & t_pos = CPoint(0, 0, 0));
};

#endif

