
#include "palette/palette.hpp"

CPalette::CPalette(const CColor t_substrate, const CColor t_background, const CColor t_horizont) :
	substrate(t_substrate), background(t_background), horizont(t_horizont)
{
	;
}

CColor CPalette::operator[](uint32_t key) const
{
	for(auto & it : palette)
		if(key == it.first)
			return it.second;

	return CColor(0, 0, 0, 0);
}

