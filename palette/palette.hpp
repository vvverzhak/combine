
#ifndef PALETTE_HPP
#define PALETTE_HPP

#include "base/base.hpp"
#include "palette/color.hpp"

typedef pair<uint32_t, CColor> TPaletteColor;

class CPalette
{
	protected:

		vector<TPaletteColor> palette;

	public:

		const CColor substrate, background, horizont;

		CPalette(const CColor t_substrate, const CColor t_background, const CColor t_horizont);
		CColor operator[](uint32_t key) const;
};

#endif

