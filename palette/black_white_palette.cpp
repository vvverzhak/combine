
#include "palette/black_white_palette.hpp"

CBlackWhitePalette::CBlackWhitePalette() :
	CPalette(CColor::from_rgb(0, 0, 0, 255), CColor::from_rgb(0, 0, 0, 255), CColor::from_rgb(0, 0, 0, 255))
{
	CColor forest			= CColor::from_rgb	(10,	100,	10,		0);
	CColor water			= CColor::from_rgb	(255,	255,	255,	255);
	CColor road				= CColor::from_rgb	(255,	255,	255,	255);
	CColor asphalt_road		= CColor::from_rgb	(255,	255,	255,	255);
	CColor hline			= CColor::from_rgb	(0,		0,		0,		0);
	CColor city				= CColor::from_rgb	(70,	20,		20,		0);

	palette.push_back(TPaletteColor(0x1DADA80, water)); // ОЗЕРА (ПОСТОЯННЫЕ)  = НАБОР
	palette.push_back(TPaletteColor(0x1DB0578, water)); // ВОДОХРАНИЛИЩА ДЕЙСТВУЮЩИЕ = НАБОР
	palette.push_back(TPaletteColor(0x1DF4750, water)); // РЕКИ ПОСТОЯНЫЕ (от 20-120 м) = ЛИНИЯ
	palette.push_back(TPaletteColor(0x1DF9958, water)); // КАНАЛЫ = НАБОР (ЛИНИЯ)
	palette.push_back(TPaletteColor(0x1E0CDF0, water)); // УРЕЗЫ ВОДЫ = ТОЧКА
//	palette.push_back(TPaletteColor(0x44F5CF0, water)); // БОЛОТА НЕПРОХОДИМЫЕ = ШТРИХОВАННАЯ ПЛОЩАДЬ

	palette.push_back(TPaletteColor(0x43D11C6, forest)); // ЛЕС ГУСТОЙ ВЫСОКИЙ = ПЛОЩАДЬ
	palette.push_back(TPaletteColor(0x43D5F78, forest)); // ДРЕВЕСНАЯ РАСТИТ.ВДОЛЬ ДОРОГ = ПУНКТИРНАЯ ЛИНИЯ
	palette.push_back(TPaletteColor(0x43D6360, forest)); // ДЕРЕВЬЯ НЕ ИМЕЮЩИЕ ЗНАЧ.ОРИЕН. = ТОЧКА

	palette.push_back(TPaletteColor(0x4402A50, CColor::from_rgb(10, 200, 10, 0))); // ЛУГОВАЯ РАСТИТЕЛЬНОСТЬ  = ПЛОЩАДЬ_СО_ЗНАКАМИ
	palette.push_back(TPaletteColor(0x43D4038, CColor::from_rgb(10, 150, 10, 0))); // САДЫ ФРУКТОВЫЕ И ЦИТРУСОВЫЕ  = НАБОР
	palette.push_back(TPaletteColor(0x43E98C0, CColor::from_rgb(10, 150, 10, 0))); // КУСТАРНИКИ ОБЫЧНЫЕ = НАБОР (точка, пунктирная линия)

	palette.push_back(TPaletteColor(0x3A47AD8, CColor::from_rgb(0, 0, 30, 0))); // ЖЕЛ.ДОРОГИ ШИРОКОЛ.ДЕЙСТВУЮЩИЕ = ЛИНИЯ

	palette.push_back(TPaletteColor(0x3A7AB40, road)); // ДОРОГИ ГРУНТОВЫЕ ПРОСЕЛОЧНЫЕ = ЛИНИЯ
	palette.push_back(TPaletteColor(0x3A7D250, road)); // ДОРОГИ ПОЛЕВЫЕ,ЛЕСНЫЕ = ПУНКТИРНАЯ ЛИНИЯ
	palette.push_back(TPaletteColor(0x3A78430, road)); // УЛУЧШЕННЫЕ ГРУНТ.ДОРОГИ ДЕЙСТВ = НАБОР
	palette.push_back(TPaletteColor(0x3B12120, road)); // ДОРОГИ ТРУДНОПРОЕЗЖИЕ(улуч.гр) = НАБОР
	palette.push_back(TPaletteColor(0x3B6D9F8, road)); // МОСТЫ ПРОЧИЕ = 149 (TODO)

	palette.push_back(TPaletteColor(0x3A624A0, asphalt_road)); // ШОССЕ УСОВЕРШЕНСТВ.(ДЕЙСТВ.) = НАБОР
	palette.push_back(TPaletteColor(0x3A64BB0, asphalt_road)); // ШОССЕ ДЕЙСТВУЮЩИЕ  = НАБОР
	palette.push_back(TPaletteColor(0x3B6CE40, asphalt_road)); // МОСТЫ НА РАЗОБЩЕНН.ОСНОВАНИЯХ = 149 (TODO)
	
	palette.push_back(TPaletteColor(0x30C2E91, CColor::from_rgb(240, 210, 80, 0))); // ТЕРРИТ.ПРОМ.,С/Х,С/К ОБЪЕКТОВ = ЛИНИЯ

	palette.push_back(TPaletteColor(0x2826520, city)); // ПОСЕЛКИ СЕЛЬСКОГО ТИПА = ЛИНИЯ
	palette.push_back(TPaletteColor(0x274A980, city)); // ПОСЕЛКИ ГОРОДСКОГО ТИПА = ЛИНИЯ
	palette.push_back(TPaletteColor(0x27322E0, city)); // ГОРОДА  = ЛИНИЯ
	palette.push_back(TPaletteColor(0x291A760, city)); // ПОСЕЛКИ ДАЧНОГО ТИПА = ЛИНИЯ
	palette.push_back(TPaletteColor(0x2B4BFC0, city)); // ЧАСТИ НАСЕЛЕННОГО ПУНКТА = ЛИНИЯ
	palette.push_back(TPaletteColor(0x2B02BE0, city)); // КВАРТАЛЫ = ЛИНИЯ
	palette.push_back(TPaletteColor(0x2932E00, city)); // ПОСЕЛКИ, НЕ ОТНЕСЕННЫЕ К ПГТ = ТОЧКА
	palette.push_back(TPaletteColor(0x2A0E9A0, city)); // СТРОЕНИЯ ОТДЕЛЬНЫЕ ВЫДАЮЩИЕСЯ = ПЛОЩАДЬ
	/*
	palette.push_back(TPaletteColor(0x2A27040, city)); // СТРОЕНИЯ ОТДЕЛЬН.НЕВЫДАЮЩИЕСЯ = ПЛОЩАДЬ
	palette.push_back(TPaletteColor(0x283EBC0, city)); // ОТДЕЛЬНЫЕ ДВОРЫ (ХУТОРА) = НАБОР
	palette.push_back(TPaletteColor(0x33365A0, city)); // СПОРТИВНЫЕ СООРУЖЕНИЯ = ЛИНИЯ
	palette.push_back(TPaletteColor(0x30C2E90, city)); // ПРОМЫШЛЕННЫЕ ПРЕДПРИЯТИЯ = НАБОР
	*/
	/*
	palette.push_back(TPaletteColor(0x2A27040, CColor::from_rgb(192, 13, 55, 255))); // СТРОЕНИЯ ОТДЕЛЬН.НЕВЫДАЮЩИЕСЯ = ПЛОЩАДЬ
	palette.push_back(TPaletteColor(0x283EBC0, CColor::from_rgb(112, 206, 57, 255))); // ОТДЕЛЬНЫЕ ДВОРЫ (ХУТОРА) = НАБОР
	palette.push_back(TPaletteColor(0x33365A0, CColor::from_rgb(64, 19, 77, 255))); // СПОРТИВНЫЕ СООРУЖЕНИЯ = ЛИНИЯ
	palette.push_back(TPaletteColor(0x30C2E90, CColor::from_rgb(160, 148, 82, 255))); // ПРОМЫШЛЕННЫЕ ПРЕДПРИЯТИЯ = НАБОР
	*/

	palette.push_back(TPaletteColor(0x141F5E0, hline)); // ГОРИЗОНТАЛИ ОСНОВНЫЕ(УТОЛЩЕН.) = ЛИНИЯ
	palette.push_back(TPaletteColor(0x1437C80, hline)); // ГОРИЗОНТАЛИ ОСНОВНЫЕ = ЛИНИЯ
	palette.push_back(TPaletteColor(0x1450320, hline)); // ГОРИЗОНТАЛИ ДОПОЛНИТЕЛЬНЫЕ = ПУНКТИРНАЯ ЛИНИЯ

	// palette.push_back(TPaletteColor(0x53EC600, CColor::from_rgb(TODO, TODO, TODO, 255))); // РАЗНОЕ = ТЕКСТ
	// palette.push_back(TPaletteColor(0x5404CA0, CColor::from_rgb(TODO, TODO, TODO, 255))); // РАЗНОЕ ПОЯСНИТЕЛЬНОЕ = ТЕКСТ

	// ############################################################################ 
	
	palette.push_back(TPaletteColor(0x2826520, CColor::from_rgb(176, 192, 2, 0)));
	palette.push_back(TPaletteColor(0x274A980, CColor::from_rgb(96, 129, 5, 0)));
	palette.push_back(TPaletteColor(0x27322E0, CColor::from_rgb(16, 66, 8, 0)));
	palette.push_back(TPaletteColor(0x291A760, CColor::from_rgb(192, 2, 11, 0)));
	palette.push_back(TPaletteColor(0x2B4BFC0, CColor::from_rgb(112, 195, 13, 0)));
	palette.push_back(TPaletteColor(0x2932E00, CColor::from_rgb(32, 132, 16, 0)));
	palette.push_back(TPaletteColor(0x43D4038, CColor::from_rgb(208, 68, 19, 0)));
	palette.push_back(TPaletteColor(0x43D11C6, CColor::from_rgb(128, 5, 22, 0)));
	palette.push_back(TPaletteColor(0x44F5CF0, CColor::from_rgb(48, 198, 24, 0)));
	palette.push_back(TPaletteColor(0x1538210, CColor::from_rgb(224, 134, 27, 0)));
	palette.push_back(TPaletteColor(0x157A0C0, CColor::from_rgb(144, 71, 30, 0)));
	palette.push_back(TPaletteColor(0x152E9B8, CColor::from_rgb(64, 8, 33, 0)));
	palette.push_back(TPaletteColor(0x4402A50, CColor::from_rgb(240, 200, 35, 0)));
	palette.push_back(TPaletteColor(0x3B3E040, CColor::from_rgb(96, 140, 49, 0)));
	palette.push_back(TPaletteColor(0x2B02BE0, CColor::from_rgb(16, 77, 52, 0)));
	palette.push_back(TPaletteColor(0x32A64F0, CColor::from_rgb(32, 143, 60, 0)));
	palette.push_back(TPaletteColor(0x311D3E0, CColor::from_rgb(208, 79, 63, 0)));
	palette.push_back(TPaletteColor(0x30BE458, CColor::from_rgb(128, 16, 66, 0)));
	palette.push_back(TPaletteColor(0x3308358, CColor::from_rgb(48, 209, 68, 0)));
	palette.push_back(TPaletteColor(0x3307F70, CColor::from_rgb(224, 145, 71, 0)));
	palette.push_back(TPaletteColor(0x30BEC28, CColor::from_rgb(144, 82, 74, 0)));
	palette.push_back(TPaletteColor(0x141F5E0, CColor::from_rgb(80, 85, 85, 0)));
	palette.push_back(TPaletteColor(0x1437C80, CColor::from_rgb(0, 22, 88, 0)));
	palette.push_back(TPaletteColor(0x1450320, CColor::from_rgb(176, 214, 90, 0)));
	palette.push_back(TPaletteColor(0x1594E70, CColor::from_rgb(96, 151, 93, 0)));
	palette.push_back(TPaletteColor(0x152EDA0, CColor::from_rgb(16, 88, 96, 0)));
	palette.push_back(TPaletteColor(0x444AE90, CColor::from_rgb(192, 24, 99, 0)));
	palette.push_back(TPaletteColor(0x43D5F78, CColor::from_rgb(112, 217, 101, 0)));
	palette.push_back(TPaletteColor(0x1DCB710, CColor::from_rgb(32, 154, 104, 0)));
	palette.push_back(TPaletteColor(0x1E9F5B0, CColor::from_rgb(208, 90, 107, 0)));
	palette.push_back(TPaletteColor(0x1ED02F0, CColor::from_rgb(128, 27, 110, 0)));
	palette.push_back(TPaletteColor(0x1EA91F0, CColor::from_rgb(48, 220, 112, 0)));
	palette.push_back(TPaletteColor(0x1EA43D0, CColor::from_rgb(224, 156, 115, 0)));
	palette.push_back(TPaletteColor(0x1EBF180, CColor::from_rgb(144, 93, 118, 0)));
	palette.push_back(TPaletteColor(0x1F95F00, CColor::from_rgb(64, 30, 121, 0)));
	palette.push_back(TPaletteColor(0x1F943A8, CColor::from_rgb(240, 222, 123, 0)));
	palette.push_back(TPaletteColor(0x1EA1CC0, CColor::from_rgb(160, 159, 126, 0)));
	palette.push_back(TPaletteColor(0x3B762B0, CColor::from_rgb(80, 96, 129, 0)));
	palette.push_back(TPaletteColor(0x3B789C0, CColor::from_rgb(0, 33, 132, 0)));
	palette.push_back(TPaletteColor(0x3A7D250, CColor::from_rgb(176, 225, 134, 0)));
	palette.push_back(TPaletteColor(0x3A78430, CColor::from_rgb(96, 162, 137, 0)));
	palette.push_back(TPaletteColor(0x3A47AD8, CColor::from_rgb(16, 99, 140, 0)));
	palette.push_back(TPaletteColor(0x3A624A0, CColor::from_rgb(192, 35, 143, 0)));
	palette.push_back(TPaletteColor(0x3A64BB0, CColor::from_rgb(112, 228, 145, 0)));
	palette.push_back(TPaletteColor(0x3A5FD90, CColor::from_rgb(32, 165, 148, 0)));
	palette.push_back(TPaletteColor(0x3A4A5D0, CColor::from_rgb(208, 101, 151, 0)));
	palette.push_back(TPaletteColor(0x3A7AB40, CColor::from_rgb(128, 38, 154, 0)));
	palette.push_back(TPaletteColor(0x3B12120, CColor::from_rgb(48, 231, 156, 0)));
	palette.push_back(TPaletteColor(0x3B6D9F8, CColor::from_rgb(224, 167, 159, 0)));
	palette.push_back(TPaletteColor(0x3B6ED80, CColor::from_rgb(144, 104, 162, 0)));
	palette.push_back(TPaletteColor(0x3B6CE40, CColor::from_rgb(64, 41, 165, 0)));
	palette.push_back(TPaletteColor(0x30EEDB0, CColor::from_rgb(240, 233, 167, 0)));
	palette.push_back(TPaletteColor(0x30F14C0, CColor::from_rgb(160, 170, 170, 0)));
	palette.push_back(TPaletteColor(0x4E645C0, CColor::from_rgb(80, 107, 173, 0)));
	palette.push_back(TPaletteColor(0x43D6360, CColor::from_rgb(0, 44, 176, 0)));
	palette.push_back(TPaletteColor(0x43E98C0, CColor::from_rgb(176, 236, 178, 0)));
	palette.push_back(TPaletteColor(0x1E0CDF0, CColor::from_rgb(96, 173, 181, 0)));
	palette.push_back(TPaletteColor(0x1EC1890, CColor::from_rgb(16, 110, 184, 0)));
	palette.push_back(TPaletteColor(0x3B19650, CColor::from_rgb(192, 46, 187, 0)));
	palette.push_back(TPaletteColor(0x3B40F20, CColor::from_rgb(112, 239, 189, 0)));
	palette.push_back(TPaletteColor(0x3B40B38, CColor::from_rgb(32, 176, 192, 0)));
	palette.push_back(TPaletteColor(0x3B41308, CColor::from_rgb(208, 112, 195, 0)));
	palette.push_back(TPaletteColor(0x3B416F0, CColor::from_rgb(128, 49, 198, 0)));
	palette.push_back(TPaletteColor(0x2A0E9A0, CColor::from_rgb(48, 242, 200, 0)));
	palette.push_back(TPaletteColor(0x330CD90, CColor::from_rgb(224, 178, 203, 0)));
	palette.push_back(TPaletteColor(0x331DF00, CColor::from_rgb(144, 115, 206, 0)));
	palette.push_back(TPaletteColor(0x3107450, CColor::from_rgb(64, 52, 209, 0)));
	palette.push_back(TPaletteColor(0x3109B60, CColor::from_rgb(240, 244, 211, 0)));
	palette.push_back(TPaletteColor(0x32F1FE0, CColor::from_rgb(160, 181, 214, 0)));
	palette.push_back(TPaletteColor(0x30C3278, CColor::from_rgb(80, 118, 217, 0)));
	palette.push_back(TPaletteColor(0x30DB530, CColor::from_rgb(0, 55, 220, 0)));
	palette.push_back(TPaletteColor(0x32DE760, CColor::from_rgb(176, 247, 222, 0)));
	palette.push_back(TPaletteColor(0x30D8E20, CColor::from_rgb(96, 184, 225, 0)));
	palette.push_back(TPaletteColor(0x32DC050, CColor::from_rgb(16, 121, 228, 0)));
	palette.push_back(TPaletteColor(0x32D7230, CColor::from_rgb(192, 57, 231, 0)));
	palette.push_back(TPaletteColor(0x31137A0, CColor::from_rgb(112, 250, 233, 0)));
	palette.push_back(TPaletteColor(0x30C3660, CColor::from_rgb(32, 187, 236, 0)));
	palette.push_back(TPaletteColor(0x31AFBA0, CColor::from_rgb(208, 123, 239, 0)));
	palette.push_back(TPaletteColor(0x56C8CC0, CColor::from_rgb(255, 255, 255, 0)));
}

