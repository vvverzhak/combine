
#include "palette/color.hpp"

CColor::CColor()
{
	;
}

CColor::CColor(const double t_r, const double t_g, const double t_b, const double t_a)
{
	r = t_r;
	g = t_g;
	b = t_b;
	a = t_a;
}

CColor::CColor(const CColor & c)
{
	r = c.r;
	g = c.g;
	b = c.b;
	a = c.a;
}

const CColor & CColor::operator=(const CColor & c)
{
	r = c.r;
	g = c.g;
	b = c.b;
	a = c.a;

	return * this;
}

CColor CColor::from_rgb(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a)
{
	return CColor(r / 255., g / 255., b / 255., a / 255.);
}

CColor CColor::to_rgb(const CColor & c)
{
	return CColor(c.r * 255, c.g * 255, c.b * 255, c.a * 255);
}

