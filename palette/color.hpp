
#ifndef COLOR_HPP
#define COLOR_HPP

#include "base/base.hpp"
#include "map/point.hpp"

class CColor
{
	public:

		double r, g, b, a;

		CColor();
		CColor(const double t_r, const double t_g, const double t_b, const double t_a);
		CColor(const CColor & c);

		const CColor & operator=(const CColor & c);
		
		static CColor from_rgb(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a);
		static CColor to_rgb(const CColor & c);
};

#endif

