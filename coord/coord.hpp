
#ifndef COORD_HPP
#define COORD_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "bad_code/bad_code.hpp"

CPoint geo_to_gauss_krueger(const CGeoPoint & gpnt);
CGeoPoint gauss_krueger_to_geo(const CPoint & pnt);

#endif

