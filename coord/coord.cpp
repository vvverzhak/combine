
#include "coord/coord.hpp"

CPoint geo_to_gauss_krueger(const CGeoPoint & gpnt)
{
	const long double lat = gpnt.lat, lon = gpnt.lon, geo_h = gpnt.h;
	long double x, y, h;
	CPoint pnt;
	
	LatLon2XY2(lat, lon, geo_h, x, y, h);

	pnt.x = x;
	pnt.y = y;
	pnt.z = h;

	return pnt;
}

CGeoPoint gauss_krueger_to_geo(const CPoint & pnt)
{
	CGeoPoint gpnt;

	printf_TODO("gauss_krueger_to_geo\n");

	return gpnt;
}

