
#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include "base/base.hpp"
#include "algo/algo.hpp"
#include "renderer/renderer.hpp"

enum EEventType
{
	EET_EXIT, EET_REDRAW, EET_KEYBOARD, EET_NOTHING
};

class CDisplay
{
	CRenderer * renderer;

	protected:

		virtual void swap() = 0;

	public:

		const unsigned height, width;

		CDisplay(const unsigned __height, const unsigned __width);

		void set_renderer(CRenderer * __renderer);
		void read_frame(void * p_frame, const GLenum type, const GLenum format);
		void redraw();
		void refresh();

		virtual EEventType wait_for_event(char & ch) = 0;
};

shared_ptr<CDisplay> create_display(const unsigned height, const unsigned width);

#endif

