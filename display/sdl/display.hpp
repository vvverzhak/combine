
#ifndef SDL_DISPLAY_HPP
#define SDL_DISPLAY_HPP

#include "base/base.hpp"
#include "display/display.hpp"
#include <SDL/SDL.h>

class CSDLDisplay : public CDisplay
{
	SDL_Surface * surf;

	public:

		CSDLDisplay(const unsigned __height, const unsigned __width);
		~CSDLDisplay();

		void swap();
		EEventType wait_for_event(char & ch);
};

#endif

