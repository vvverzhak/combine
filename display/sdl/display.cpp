
#include "display/sdl/display.hpp"

CSDLDisplay::CSDLDisplay(const unsigned __height, const unsigned __width) :
	CDisplay(__height, __width)
{
	throw_if(SDL_Init(SDL_INIT_VIDEO) < 0);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	throw_null(surf = SDL_SetVideoMode(width, height, 32, SDL_OPENGL));
}

CSDLDisplay::~CSDLDisplay()
{
	SDL_Quit();
}

void CSDLDisplay::swap()
{
	glFlush();
	SDL_GL_SwapBuffers();
}

EEventType CSDLDisplay::wait_for_event(char & ch)
{
	SDL_Event event;

	while(true)
	{
		SDL_WaitEvent(& event);

		if(event.type == SDL_KEYDOWN)
		{
			if(event.key.keysym.sym == SDLK_ESCAPE)
				return EET_EXIT;

			ch = event.key.keysym.sym;

			return EET_KEYBOARD;
		}

		return EET_NOTHING;
	}
}

shared_ptr<CDisplay> create_display(const unsigned height, const unsigned width)
{
	CDisplay * disp;

	throw_null(disp = new CSDLDisplay(height, width));

	return shared_ptr<CDisplay>(disp);
}

