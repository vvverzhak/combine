
#include "display/display.hpp"

CDisplay::CDisplay(const unsigned __height, const unsigned __width) :
	height(__height), width(__width)
{
	renderer = NULL;
}

void CDisplay::set_renderer(CRenderer * __renderer)
{
	throw_null(renderer = __renderer);
}

void CDisplay::read_frame(void * buf, const GLenum type, const GLenum format)
{
	throw_null(buf);
	
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadPixels(0, 0, width, height, type, format, buf);

//	print_count_values_in_buf((uint16_t *) buf, height, width);
}

void CDisplay::redraw()
{
	throw_null(renderer);

	renderer->redraw();
	CAlgo::process();
	swap();
}

