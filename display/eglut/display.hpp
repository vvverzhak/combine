
#ifndef EGLUT_DISPLAY_HPP
#define EGLUT_DISPLAY_HPP

#include "base/base.hpp"
#include "display/display.hpp"

class CEGLUTDisplay : public CDisplay
{
	int win;

	public:

		CEGLUTDisplay(const unsigned __height, const unsigned __width);
		~CEGLUTDisplay();

		void swap();
		EEventType wait_for_event(char & ch);
};

#endif

