
extern "C"
{
	#include "eglut.h"
	#include "eglutint.h"
}

#include "display/eglut/display.hpp"

eglut_state * state;

CEGLUTDisplay::CEGLUTDisplay(const unsigned __height, const unsigned __width) :
	CDisplay(__height, __width)
{
	int argc = 0;
	char ** argv = NULL;

	eglutInitWindowSize(width, height);
	eglutInit(argc, argv);
	eglutInitAPIMask(EGLUT_OPENGL_ES1_BIT | EGLUT_OPENGL_BIT);

	win = eglutCreateWindow("combine");
	state = (eglut_state *) get_eglut_state();
}

CEGLUTDisplay::~CEGLUTDisplay()
{
	;
}

void CEGLUTDisplay::swap()
{
	glFlush();
	eglSwapBuffers(state->dpy, state->current->surface);
}

EEventType CEGLUTDisplay::wait_for_event(char & ch)
{
	return EET_NOTHING;
}

shared_ptr<CDisplay> create_display(const unsigned height, const unsigned width)
{
	CDisplay * disp;

	throw_null(disp = new CEGLUTDisplay(height, width));

	return shared_ptr<CDisplay>(disp);
}

