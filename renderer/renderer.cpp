
#include "renderer/renderer.hpp"

CRenderer::CRenderer(
		const unsigned __height, const unsigned __width,
		const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
		const CMap & __map, const CHeightMap & __height_map,
		const CCamera & __camera,
		const CScale & __scale,
		const CColor & __bg_color,
		const CColor & __horizont_color) :

	texture_size(__texture_size), texture_per_row(__texture_per_row), texture_step(__texture_step), map(__map), height_map(__height_map), camera(__camera), scale(__scale), bg_color(__bg_color), horizont_color(__horizont_color),
	height(__height), width(__width)
{
	is_first = true;
}

// ############################################################################ 

unsigned CRenderer::find_square(const CPoint pnt)
{
	const double max_r_2 = (scale.scene_to.x - scale.scene_from.x) / texture_per_row; // TODO Не квадратные сцены
	const unsigned tv = (pnt.y - scale.scene_from.y) / max_r_2;
	const unsigned tu = (pnt.x - scale.scene_from.x) / max_r_2;

	return tv * texture_per_row + tu;
};

vector<STriangleRow> CRenderer::get_triangle(const unsigned step)
{
	const double seam_2 = seam / 2;
	const unsigned height = height_map.pnt_scene.size(), width = height_map.pnt_scene[0].size();
	const unsigned border_u = (width * seam_2) / (scale.scene_to.x - scale.scene_from.x);
	const unsigned border_v = (height * seam_2) / (scale.scene_to.y - scale.scene_from.y);
	const unsigned pnt_per_row = width / texture_per_row, pnt_per_column = height / texture_per_row;
	unsigned v, u, tv, ind;
	int from_v, to_v, from_u, to_u;
	vector< STriangleRow > triangle;

	auto add_point = [ this ] (STriangleRow & row, const unsigned tv_1, const unsigned tv_2, const unsigned tu, const unsigned ind)
	{
		const CPoint & pnt_1 = height_map.pnt_scene[tv_1][tu];
		row.pnt.push_back(pair<CPoint, CPoint>(pnt_1, texture[ind]->scene_to_texture(pnt_1)));

		const CPoint & pnt_2 = height_map.pnt_scene[tv_2][tu];
		row.pnt.push_back(pair<CPoint, CPoint>(pnt_2, texture[ind]->scene_to_texture(pnt_2)));
	};

	auto add_row = [ this, & triangle, & step, & width, & add_point ] (const unsigned tv_1, const unsigned tv_2, const unsigned from_u, const unsigned to_u, const unsigned ind)
	{
		STriangleRow row;
		unsigned tu;

		row.texture = ind;

		for(tu = from_u; tu < to_u; tu += step)
			add_point(row, tv_1, tv_2, tu, ind);

		if(tu < width)
			add_point(row, tv_1, tv_2, tu, ind);
		else if(tu > width)
			add_point(row, tv_1, tv_2, width - 1, ind);

		triangle.push_back(row);
	};

	for(v = 0, ind = 0; v < texture_per_row; v++)
		for(u = 0; u < texture_per_row; u++, ind++)
		{
			from_u = u * pnt_per_row;
			from_u = from_u < border_u ? 0 : from_u - border_u;

			to_u = (u + 1) * pnt_per_row + border_u;

			if(to_u > width)
				to_u = width;

			from_v = v * pnt_per_column - border_v;
			from_v = from_v < border_v ? 0 : from_v - border_v;

			to_v = (v + 1) * pnt_per_column + border_v;

			if(to_v > height - step)
				to_v = height - step;

			// ############################################################################ 

			for(tv = from_v; tv < to_v; tv += step)
				add_row(tv, tv + step, from_u, to_u, ind);

			if(v == texture_per_row - 1 && tv < height - 1)
				add_row(tv, height - 1, from_u, to_u, ind);
		}

	return triangle;
}

void CRenderer::add_buffer(const vector< STriangleRow > & triangle)
{
	unsigned v, u;
	vector< vector<GLuint> > t_vertex_buffer_ind, t_texture_buffer_ind;
	vector< vector<unsigned> > t_pnt_num_array;

	t_vertex_buffer_ind.resize(texture_per_row * texture_per_row);
	t_texture_buffer_ind.resize(texture_per_row * texture_per_row);
	t_pnt_num_array.resize(texture_per_row * texture_per_row);

	for(auto & row : triangle)
	{
		const unsigned pnt_num = row.pnt.size();
		GLuint vbi, tbi;
		GL_FLOAT_TYPE * ptr;

		glGenBuffers(1, & vbi);
		glGenBuffers(1, & tbi);

		// ############################################################################ 

		glBindBuffer(GL_ARRAY_BUFFER, vbi);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GL_FLOAT_TYPE) * pnt_num * 3, NULL, GL_STATIC_DRAW);

		throw_null(ptr = (GL_FLOAT_TYPE *) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
		
		for(v = 0, u = 0; v < pnt_num; v++, u += 3)
		{
			CPoint pnt = row.pnt[v].first;

			ptr[u] = pnt.x;
			ptr[u + 1] = pnt.z;
			ptr[u + 2] = pnt.y;
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);

		// ############################################################################ 

		glBindBuffer(GL_ARRAY_BUFFER, tbi);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GL_FLOAT_TYPE) * pnt_num * 2, NULL, GL_STATIC_DRAW);

		throw_null(ptr = (GL_FLOAT_TYPE *) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
		
		for(v = 0, u = 0; v < pnt_num; v++, u += 2)
		{
			CPoint pnt = row.pnt[v].second;

			ptr[u] = pnt.x;
			ptr[u + 1] = pnt.y;
		}

		glUnmapBuffer(GL_ARRAY_BUFFER);

		// ############################################################################ 

		t_vertex_buffer_ind[row.texture].push_back(vbi);
		t_texture_buffer_ind[row.texture].push_back(tbi);
		t_pnt_num_array[row.texture].push_back(pnt_num);
	}

	vertex_buffer_ind.push_back(t_vertex_buffer_ind);
	texture_buffer_ind.push_back(t_texture_buffer_ind);
	pnt_num_array.push_back(t_pnt_num_array);
}

void CRenderer::create_model()
{
	vertex_buffer_ind.clear();
	texture_buffer_ind.clear();
	pnt_num_array.clear();

	for(auto & cur_step : texture_step)
	{
		vector< STriangleRow > triangle = get_triangle(cur_step);
		add_buffer(triangle);
	}
}

// ############################################################################ 

void CRenderer::redraw()
{
	// DEBUG;
	const unsigned texture_num = texture_per_row * texture_per_row;
	const unsigned texture_level_1 = texture_step.size() - 1;
	const double frame_xy = 3, frame_z = scale.scene_from.z;
	const int pos = find_square(camera.pos);
	int i_0, j_0, i, j, d;
	unsigned v, u, t, ind, ind_num, size, ind_array[2];
	// DEBUG;

	if(is_first)
	{
	// DEBUG;
		is_first = false;
	// DEBUG;

		create_textures();
	// DEBUG;
		create_model();
	// DEBUG;

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
	// DEBUG;

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// DEBUG;
		glShadeModel(GL_SMOOTH);
	// DEBUG;
	}
	// DEBUG;

	glViewport(0, 0, width, height);
	// DEBUG;

	camera.print();
	camera.set();
	// DEBUG;

	glClearColor(bg_color.r, bg_color.g, bg_color.b, bg_color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// DEBUG;

	glEnableClientState(GL_VERTEX_ARRAY);
	// DEBUG;

	// ############################################################################ 

	glEnableClientState(GL_COLOR_ARRAY);
	// DEBUG;

	static GL_FLOAT_TYPE substrate_vertex[5][3] =
	{
		{ - frame_xy, frame_z,   frame_xy },
		{ - frame_xy, frame_z, - frame_xy },
		{   frame_xy, frame_z, - frame_xy },
		{   frame_xy, frame_z,	 frame_xy },
		{ - frame_xy, frame_z,   frame_xy }
	};
	
	static GL_FLOAT_TYPE __horizont_color[5][4] =
	{
		{ horizont_color.r, horizont_color.g, horizont_color.b, horizont_color.a },
		{ horizont_color.r, horizont_color.g, horizont_color.b, horizont_color.a },
		{ horizont_color.r, horizont_color.g, horizont_color.b, horizont_color.a },
		{ horizont_color.r, horizont_color.g, horizont_color.b, horizont_color.a },
		{ horizont_color.r, horizont_color.g, horizont_color.b, horizont_color.a }
	};

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// DEBUG;
	glVertexPointer(3, GL_FLOAT_TYPE_ID, 0, substrate_vertex);
	// DEBUG;
	glColorPointer(4, GL_FLOAT_TYPE_ID, 0, __horizont_color);
	// DEBUG;
	glDrawArrays(GL_LINE_STRIP, 0, 5);
	// DEBUG;
	
	glDisableClientState(GL_COLOR_ARRAY);
	// DEBUG;

	// ############################################################################ 

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	// DEBUG;
	glEnable(GL_TEXTURE_2D);
	// DEBUG;

	auto get_texture_coord = [ = ](const int ind, int & i, int & j)
	{
		i = ind / texture_per_row;
		j = ind - i * texture_per_row;
	};

	get_texture_coord(pos, i_0, j_0);
	// DEBUG;

	for(v = 0; v < texture_num; v++)
	{
	// DEBUG;
		texture[v]->bind();
	// DEBUG;

		get_texture_coord(v, i, j);
		d = sqrt(pow(i - i_0, 2) + pow(j - j_0, 2));
	// DEBUG;

		if(d >= texture_level_1)
		{
			ind_array[0] = texture_level_1;
			ind_num = 1;
		}
		else
		{
			ind_array[0] = d + 1;
			ind_array[1] = d;
			ind_num = 2;
		}
	// DEBUG;

		for(t = 0; t < ind_num; t++)
		{
			ind = ind_array[t];
			size = pnt_num_array[ind][v].size();

			for(u = 0; u < size; u++)
			{
				glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_ind[ind][v][u]);
				glVertexPointer(3, GL_FLOAT_TYPE_ID, 0, NULL);

				glBindBuffer(GL_ARRAY_BUFFER, texture_buffer_ind[ind][v][u]);
				glTexCoordPointer(2, GL_FLOAT_TYPE_ID, 0, NULL);

				glDrawArrays(GL_TRIANGLE_STRIP, 0, pnt_num_array[ind][v][u]);
			}
		}

	// DEBUG;
	}
	// DEBUG;

	glDisableClientState(GL_VERTEX_ARRAY);
	// DEBUG;
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	// DEBUG;
	glDisable(GL_TEXTURE_2D);
	// DEBUG;
}

