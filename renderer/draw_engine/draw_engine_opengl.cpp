
#include "renderer/draw_engine/draw_engine_opengl.hpp"

CPositionOnTexture::CPositionOnTexture()
{
	// CPoint инициализируется нулями по умолчанию

	height = 0;
	width = 0;
}

CPositionOnTexture & CPositionOnTexture::operator=(const CPositionOnTexture & op)
{
	pos = op.pos;
	height = op.height;
	width = op.width;

	return * this;
}

void CPositionOnTexture::set(CPoint __pos, const double __width, const double __height)
{
	pos = __pos;
	width = __width;
	height = __height;
}

CPoint CPositionOnTexture::from()
{
	return pos;
}

CPoint CPositionOnTexture::to()
{
	return CPoint(pos.x + width, pos.y + height, 0);
}

// ############################################################################ 
 
#ifdef __MINGW32__

	#define CALLBACK_PREFIX __attribute__((__stdcall__)) 

#else

	#define CALLBACK_PREFIX

#endif

vector<CPoint> current_obj;
GLenum current_obj_type;

void CALLBACK_PREFIX begin_callback(GLenum type)
{
	current_obj.clear();
	current_obj_type = type;
}

void CALLBACK_PREFIX end_callback()
{
	const unsigned pnt_num = current_obj.size();
	unsigned v;
	GL_FLOAT_TYPE __vertex[pnt_num * 3];

	for(v = 0; v < pnt_num; v++)
	{
		__vertex[v * 3] = current_obj[v].x;
		__vertex[v * 3 + 1] = current_obj[v].y;
		__vertex[v * 3 + 2] = current_obj[v].z;
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexPointer(3, GL_FLOAT_TYPE_ID, 0, __vertex);
	glDrawArrays(current_obj_type, 0, pnt_num);
}

void CALLBACK_PREFIX vertex_callback(const GLvoid * ptr)
{
	const GL_FLOAT_TYPE * pr = (const GL_FLOAT_TYPE *) ptr;
	
	current_obj.push_back(CPoint(pr[0], pr[1], pr[2]));
}

void CALLBACK_PREFIX error_callback()
{
	; // throw_;
}

void CALLBACK_PREFIX combine_callback(GL_FLOAT_TYPE coords[3], GL_FLOAT_TYPE * vertex_data[4], GL_FLOAT_TYPE weight[4], GL_FLOAT_TYPE ** data_out)
{
	unsigned v;
	GL_FLOAT_TYPE * vertex;
	
	// TODO Зачем и отладить + профилировка по памяти (где delete для vertex?)

	throw_null(vertex = new GL_FLOAT_TYPE[6]);
	
	vertex[0] = coords[0];
	vertex[1] = coords[1];
	vertex[2] = coords[2];
	
	for(v = 3; v < 6; v++)
		vertex[v] = weight[0] * vertex_data[0][v] + weight[1] * vertex_data[1][v]; //  + weight[2] * vertex_data[2][v] + weight[3] * vertex_data[3][v]; // TODO
	
	* data_out = vertex;
}

void new_contour(GLUtesselator * tess, vector<GL_FLOAT_TYPE *> & pnt_buf, vector<CPoint> & pnts)
{
	if(pnts.size())
	{
		gluTessBeginContour(tess);

			unsigned v = 0;
			GL_FLOAT_TYPE * ptr;

			throw_null(ptr = new GL_FLOAT_TYPE[pnts.size() * 3]);
			pnt_buf.push_back(ptr);

			for(auto & pnt : pnts)
			{
				ptr[v] = pnt.x;
				ptr[v + 1] = pnt.y;
				ptr[v + 2] = 0;

				gluTessVertex(tess, ptr, ptr);

				ptr += 3;
			}

			pnts.clear();

		gluTessEndContour(tess);
	}
}

// ############################################################################ 

COpenGLDrawEngine::COpenGLDrawEngine(const CPalette & __palette) :
	CDrawEngine(__palette)
{
	tess = NULL;
	is_renderer_texture_create = false;

	glGenFramebuffers(1, & framebuffer_ind);

	throw_null(tess = gluNewTess());

	gluTessCallback(tess, GLU_TESS_BEGIN, (void (CALLBACK_PREFIX *)()) begin_callback);
	gluTessCallback(tess, GLU_TESS_END, (void (CALLBACK_PREFIX *)()) end_callback);
	gluTessCallback(tess, GLU_TESS_VERTEX, (void (CALLBACK_PREFIX *)()) vertex_callback);
	gluTessCallback(tess, GLU_TESS_COMBINE, (void (CALLBACK_PREFIX *)()) combine_callback);
	gluTessCallback(tess, GLU_TESS_ERROR, (void (CALLBACK_PREFIX *)()) error_callback);

	create_text_rendering();
}

COpenGLDrawEngine::~COpenGLDrawEngine()
{
	if(tess != NULL)
		gluDeleteTess(tess);
}

GLuint COpenGLDrawEngine::create_assistant_texture(const uint8_t * data, const unsigned height, const unsigned width)
{
	GLuint ind;

	glGenTextures(1, & ind);
	glBindTexture(GL_TEXTURE_2D, ind);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	set_texture_params();

	return ind;
}

void COpenGLDrawEngine::set_texture_params()
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void COpenGLDrawEngine::create_text_rendering()
{
	shared_ptr<uint8_t> buf;
	uint8_t * p_buf;
	unsigned buf_size, letter_ind;
	FT_Library lib;
	FT_Face face;
	FT_GlyphSlot slot;
	FT_ULong ch;
	vector<wchar_t> render_letters =
	{
		L'q', L'w', L'e', L'r', L't', L'y', L'u', L'i', L'o', L'p', L'a', L's', L'd', L'f', L'g', L'h', L'j', L'k', L'l', L'z', L'x', L'c', L'v', L'b', L'n', L'm', 
		L'Q', L'W', L'E', L'R', L'T', L'Y', L'U', L'I', L'O', L'P', L'A', L'S', L'D', L'F', L'G', L'H', L'J', L'K', L'L', L'Z', L'X', L'C', L'V', L'B', L'N', L'M', 
		L',', L'.', L' ', L'-', L'(', L')',
		L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7', L'8', L'9', 
		L'а', L'б', L'в', L'г', L'д', L'е', L'ё', L'ж', L'з', L'и', L'й', L'к', L'л', L'м', L'н', L'о', L'п', L'р', L'с', L'т', L'у', L'ф', L'х', L'ш', L'щ', L'ч', L'ц', L'ъ', L'ы', L'ь', L'э', L'ю', L'я',
		L'А', L'Б', L'В', L'Г', L'Д', L'Е', L'Ё', L'Ж', L'З', L'И', L'Й', L'К', L'Л', L'М', L'Н', L'О', L'П', L'Р', L'С', L'Т', L'У', L'Ф', L'Х', L'Ш', L'Щ', L'Ч', L'Ц', L'Ъ', L'Ы', L'Ь', L'Э', L'Ю', L'Я'
	};

	auto begin_get_letter = [ & letter_ind ]()
	{
		letter_ind = 0;
	};

	auto get_letter = [ & face, & slot, & letter_ind, render_letters ](FT_ULong & ch)
	{
		if(! (letter_ind < render_letters.size()))
			return false;

		const FT_UInt glyph_ind = FT_Get_Char_Index(face, render_letters[letter_ind]);

		throw_if(FT_Load_Glyph(face, glyph_ind, FT_LOAD_DEFAULT));
		throw_if(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL));

		ch = render_letters[letter_ind];
		letter_ind ++;

		return true;
	};

	auto get_letter_by_code = [ & face, & slot ](const FT_ULong code)
	{
		const FT_UInt glyph_ind = FT_Get_Char_Index(face, code);

		throw_if(FT_Load_Glyph(face, glyph_ind, FT_LOAD_DEFAULT));
		throw_if(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL));
	};

	// ############################################################################ 
	// Инициализация FreeType

	throw_if(FT_Init_FreeType(& lib));
	throw_if(FT_New_Face(lib, font_fname().c_str(), 0, & face));
	throw_if(FT_Set_Char_Size(face, 0, 64 * 64, 75, 75)); // TODO

	slot = face->glyph;

	// ############################################################################ 
	// Расчет размеров текстуры

	typedef pair<FT_ULong, unsigned> t_lwidth;

	unsigned letter_height, letters_texture_height, letters_texture_width, current_line_width;
	vector<t_lwidth> letters_width;
	vector< vector<FT_ULong> > draw_order;

	letter_height = 0;
	letters_texture_height = 0;

	begin_get_letter();

	while(get_letter(ch))
	{
		letters_width.push_back(t_lwidth(ch, slot->bitmap.width));
		letter_height = max((unsigned) slot->bitmap.rows, letter_height);
	}

	sort(letters_width.begin(), letters_width.end(),
		[](t_lwidth op_1, t_lwidth op_2)
		{
			return (op_1.second < op_2.second);
		});

	letters_texture_width = 0;
	current_line_width = 0;

	draw_order.resize(1);

	for(auto & it : letters_width)
	{
		current_line_width += it.second;

		if(current_line_width >= aux_texture_size)
		{
			letters_texture_width = max(current_line_width - it.second, letters_texture_width);
			draw_order.resize(draw_order.size() + 1);
			current_line_width = it.second;
		}
		
		(draw_order.end() - 1)->push_back(it.first);
	}

	letters_texture_width = max(current_line_width, letters_texture_width);
	letters_texture_height = letter_height * draw_order.size();

	function<unsigned(const unsigned)> right_power_2 = [ this ](const unsigned op)
	{
		unsigned p;

		for(p = 2; p < aux_texture_size; p <<= 1)
			if(op <= p)
				return p;

		return aux_texture_size;
	};

	letters_texture_width = right_power_2(letters_texture_width);
	letters_texture_height = right_power_2(letters_texture_height);
	
	throw_if(letters_texture_width > aux_texture_size);
	throw_if(letters_texture_height > aux_texture_size);

	// ############################################################################ 
	// Отрисовка текстуры во внутренний буфер

	const unsigned number_of_lines = draw_order.size();
	unsigned v, u, tv, tu, x, y;
	uint8_t * p_pixel;
	unsigned char * p_bitmap;
	CPositionOnTexture tex_pos;

	buf_size = letters_texture_height * letters_texture_width * 4;
	buf.reset(new uint8_t[buf_size], std::default_delete<uint8_t[]>());
	throw_null(p_buf = buf.get());
	memset(p_buf, 0, buf_size);

	for(v = 0; v < number_of_lines; v++)
	{
		const unsigned number_of_symbols_in_line = draw_order[v].size();

		for(u = 0, x = 0; u < number_of_symbols_in_line; u++, x += slot->bitmap.width)
		{
			const FT_ULong code = draw_order[v][u];

			get_letter_by_code(code);
			y = v * letter_height + (letter_height - slot->bitmap.rows) / 2.;

			letters[code].set(
				CPoint(
					x / ((double) letters_texture_width),
					(v * letter_height) / ((double) letters_texture_height),
					0
				),
				slot->bitmap.width / ((double) letters_texture_width),
				letter_height / ((double) letters_texture_height)
			);

			for(tv = 0; tv < slot->bitmap.rows; tv++)
				for(tu = 0; tu < slot->bitmap.width; tu++)
				{
					p_pixel = p_buf + (y + tv) * (letters_texture_width * 4) + (x + tu) * 4;
					p_bitmap = slot->bitmap.buffer + (tv * slot->bitmap.pitch) + tu;

					* p_pixel = 255 - * p_bitmap;
					* (p_pixel + 1) = 255 - * p_bitmap;
					* (p_pixel + 2) = 255 - * p_bitmap;
					* (p_pixel + 3) = (* p_bitmap) ? 255 : 0;
				}
		}
	}

	// ############################################################################ 
	// Загрузка текстуры
	
	letters_texture = create_assistant_texture(p_buf, letters_texture_height, letters_texture_width);
}

void COpenGLDrawEngine::__begin_texture(const unsigned texture_size, const CPoint & center, const double max_r)
{
	CPoint min_pnt, max_pnt, a, b;

	min_pnt.x = center.x - max_r;
	min_pnt.y = center.y - max_r;
	min_pnt.z = -1;

	max_pnt.x = center.x + max_r;
	max_pnt.y = center.y + max_r;
	max_pnt.z = 1;
	
	a.x = 1. / (max_pnt.x - min_pnt.x);
	a.y = 1. / (max_pnt.y - min_pnt.y);

	b.x = - a.x * min_pnt.x;
	b.y = - a.y * min_pnt.y;

	// ############################################################################ 
	
#ifdef TEXTURE_COMPRESSION

	if(! is_renderer_texture_create || renderer_texture_size != texture_size)
	{
		if(is_renderer_texture_create)
			glDeleteTextures(1, & renderer_texture_ind);

		renderer_texture_size = texture_size;
		is_renderer_texture_create = true;

		glGenTextures(1, & renderer_texture_ind);
	}

#else

	if(! is_renderer_texture_create)
	{
		renderer_texture_size = texture_size;
		is_renderer_texture_create = true;
	}

	glGenTextures(1, & renderer_texture_ind);

#endif

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_ind);
	glBindTexture(GL_TEXTURE_2D, renderer_texture_ind);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, renderer_texture_size, renderer_texture_size, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	set_texture_params();

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderer_texture_ind, 0);

	// GLenum DrawBuffers[2] = { GL_COLOR_ATTACHMENT0 };
	// glDrawBuffers(1, DrawBuffers);

	throw_if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE);

	glClearColor(palette.substrate.r, palette.substrate.g, palette.substrate.b, palette.substrate.a);
	glClearDepthf(1);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_ALPHA_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// ############################################################################ 
	// Дополнительное сглаживание

	glShadeModel(GL_SMOOTH);
	
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);

	// ############################################################################ 

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, texture_size, texture_size);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gl_ortho(min_pnt.x, max_pnt.x, min_pnt.y, max_pnt.y, min_pnt.z, max_pnt.z);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// ############################################################################ 

	glEnableClientState(GL_VERTEX_ARRAY);
//	glEnableClientState(GL_COLOR_ARRAY);
}

GLuint COpenGLDrawEngine::__end_texture()
{
	GLuint texture_ind;

	glDisableClientState(GL_VERTEX_ARRAY);
//	glDisableClientState(GL_COLOR_ARRAY);

	glFinish();

#ifdef TEXTURE_COMPRESSION

	unsigned v;
	shared_ptr<uint8_t> buf_2(new uint8_t[renderer_texture_size * renderer_texture_size * 2], std::default_delete<uint8_t[]>());
	shared_ptr<uint8_t> buf_4(new uint8_t[renderer_texture_size * renderer_texture_size * 4], std::default_delete<uint8_t[]>());
	uint8_t * p_buf_2 = buf_2.get();
	uint8_t * p_buf_4 = buf_4.get();

	throw_null(p_buf_2);
	throw_null(p_buf_4);
	
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadPixels(0, 0, renderer_texture_size, renderer_texture_size, GL_RGBA, GL_UNSIGNED_BYTE, p_buf_4);

	for(v = 0; v < renderer_texture_size * renderer_texture_size; v++)
	{
		const unsigned ind_2 = v * 2;
		const unsigned ind_4 = v * 4;

		#ifdef EMBEDDED
		
			const uint8_t red = p_buf_4[ind_4 + 3];
			const uint8_t green = p_buf_4[ind_4 + 2];
			const uint8_t blue = p_buf_4[ind_4 + 1];

			p_buf_2[ind_2] = (red & 0xF8) + ((green & 0xE0) >> 5);
			p_buf_2[ind_2 + 1] = ((green & 0x1C) << 3) + ((blue & 0xF8) >> 3);

		#else

			const uint8_t red = p_buf_4[ind_4 + 0];
			const uint8_t green = p_buf_4[ind_4 + 1];
			const uint8_t blue = p_buf_4[ind_4 + 2];

			p_buf_2[ind_2 + 1] = (red & 0xF8) + ((green & 0xE0) >> 5);
			p_buf_2[ind_2] = ((green & 0x1C) << 3) + ((blue & 0xF8) >> 3);

		#endif
	}

	glGenTextures(1, & texture_ind);
	glBindTexture(GL_TEXTURE_2D, texture_ind);
	DEBUG;
	// glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, renderer_texture_size, renderer_texture_size, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, p_buf_2);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, renderer_texture_size, renderer_texture_size, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, p_buf_2);
	DEBUG;
	
	set_texture_params();
	/*
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	*/

#else

	texture_ind = renderer_texture_ind;

#endif

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return texture_ind;
}

void COpenGLDrawEngine::__begin_classifier_load()
{
	marks.clear();
	marks_buf.clear();
}

void COpenGLDrawEngine::__end_classifier_load()
{
	const unsigned number_of_marks = marks_buf.size();
	const unsigned mark_line_width = 32 * 4;
	const unsigned marks_per_row = std::min(number_of_marks, (unsigned) aux_texture_size / 32);
	const unsigned number_of_rows = number_of_marks / marks_per_row + ((number_of_marks % marks_per_row) ? 1 : 0);
	const unsigned texture_width = marks_per_row * 32, texture_height = number_of_rows * 32;
	const unsigned size = texture_width * texture_height * 4;
	shared_ptr<uint8_t> buf;
	unsigned v, u, col, row;

	throw_if(texture_height > aux_texture_size);
	
	buf.reset(new uint8_t[size], std::default_delete<uint8_t[]>());
	throw_if(! buf);

	marks.resize(number_of_marks);
	memset(buf.get(), 0, size);

	for(v = 0, col = 0, row = 0; v < number_of_marks; v++, col++)
	{
		if(col == marks_per_row)
		{
			col = 0;
			row++;
		}

		for(u = 0; u < 32; u++)
		{
			uint8_t * p_buf = buf.get() + (row * 32 + u) * texture_width * 4 + col * mark_line_width;
			const uint8_t * p_mark_buf = marks_buf[v].get() + u * mark_line_width;

			memcpy(p_buf, p_mark_buf, mark_line_width);
		}

		marks[v].set(
			CPoint(
				col / ((double) marks_per_row),
				row / ((double) number_of_rows),
				0),
			32. / texture_width,
			32. / texture_height
		);
	}

	marks_texture = create_assistant_texture(buf.get(), texture_height, texture_width);
	marks_buf.clear();
}

unsigned COpenGLDrawEngine::__create_mark(shared_ptr<uint8_t> data)
{
	marks_buf.push_back(data);

	return marks_buf.size() - 1;
}

void COpenGLDrawEngine::__begin(const e_primitive_type primitive_type)
{
	switch(primitive_type)
	{
		case EPT_POINT:
		{
			glPointSize(1);
			glColor4f(color.r, color.g, color.b, color.a);

			begin_callback(GL_POINTS);

			break;
		}
		case EPT_LINE:
		{
			glLineWidth(1);
			glColor4f(color.r, color.g, color.b, color.a);

			begin_callback(GL_LINE_STRIP);

			break;
		}
		case EPT_DOTTED_LINE:
		{
			glLineWidth(1);
			glColor4f(color.r, color.g, color.b, color.a);

			begin_callback(GL_LINES);

			break;
		}
		case EPT_POLYGON:
		{
			polygon_pnts.clear();
			pnt_buf.clear();

			glColor4f(color.r, color.g, color.b, color.a);
			gluTessProperty(tess, GLU_TESS_WINDING_RULE, GLU_TESS_WINDING_POSITIVE);
			gluTessBeginPolygon(tess, NULL);

			break;
		}
		case EPT_POLYGON_HOLE:
		{
			new_contour(tess, pnt_buf, polygon_pnts);

			break;
		}

#define BEGIN_WITH_TEXTURE(ptype, tex_ind)\
		case ptype:\
		{\
			current_obj.clear();\
			\
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);\
			glEnable(GL_TEXTURE_2D);\
			glBindTexture(GL_TEXTURE_2D, tex_ind);\
			\
			break;\
		}

		BEGIN_WITH_TEXTURE(EPT_POINT_WITH_MARK, marks_texture)
		BEGIN_WITH_TEXTURE(EPT_TEXT, letters_texture)
	}
}

void COpenGLDrawEngine::__vertex(const e_primitive_type primitive_type, const CPoint & pnt)
{
	if(primitive_type == EPT_POLYGON || primitive_type == EPT_POLYGON_HOLE)
		polygon_pnts.push_back(pnt);
	else
		current_obj.push_back(pnt);
}

void COpenGLDrawEngine::__end(const e_primitive_type primitive_type)
{
	switch(primitive_type)
	{
		case EPT_POINT:
		{
			end_callback();

			break;
		}
		case EPT_TEXT:
		case EPT_POINT_WITH_MARK:
		{
			const wstring wlabel = codepage::utf8_to_wstring(text.label);
			const unsigned size = (primitive_type == EPT_TEXT) ? wlabel.size() : 1;
			const double mark_size = (primitive_type == EPT_TEXT) ? 0.003 : 0.01;
			unsigned v;
			GL_FLOAT_TYPE __vertex[size * 4 * 3], __texture[size * 4 * 2];
			vector<CPositionOnTexture> texture_pnts;
			
			// TODO Костылеще - для надписей, отчего-то, две точки передаются
			if(primitive_type == EPT_TEXT)
				current_obj.pop_back();

			auto next = [& __vertex, & __texture](const unsigned ind, const CPoint pos, const CPoint texture_pos)
			{
				const unsigned vertex_ind = ind * 3, texture_ind = ind * 2;

				__vertex[vertex_ind] = pos.x;
				__vertex[vertex_ind + 1] = pos.y;
				__vertex[vertex_ind + 2] = pos.z;

				__texture[texture_ind] = texture_pos.x;
				__texture[texture_ind + 1] = texture_pos.y;
			};

			if(primitive_type == EPT_TEXT)
			{
				// Расчет координат букв на текстуре

				for(v = 0; v < size; v++)
				{
					const FT_ULong code = wlabel[v];

					throw_if(! letters.count(code));
					texture_pnts.push_back(letters[code]);
				}
			}
			else
				texture_pnts.push_back(marks[point.mark_index]);

			for(auto & pnt : current_obj)
			{
				const double from_y = pnt.y;
				const double to_y = from_y + mark_size;

				for(v = 0; v < size; v++)
				{
					const double from_x = pnt.x + v * mark_size;
					const double to_x = from_x + mark_size;
					const CPoint tex_from = texture_pnts[v].from();
					const CPoint tex_to = texture_pnts[v].to();
					const unsigned ind = v * 4;

					next(ind,     CPoint(from_x,   to_y, pnt.z), CPoint(tex_from.x,   tex_to.y, 0));
					next(ind + 1, CPoint(from_x, from_y, pnt.z), CPoint(tex_from.x, tex_from.y, 0));
					next(ind + 2, CPoint(  to_x,   to_y, pnt.z), CPoint(  tex_to.x,   tex_to.y, 0));
					next(ind + 3, CPoint(  to_x, from_y, pnt.z), CPoint(  tex_to.x, tex_from.y, 0));
				}

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glVertexPointer(3, GL_FLOAT_TYPE_ID, 0, __vertex);
				glTexCoordPointer(2, GL_FLOAT_TYPE_ID, 0, __texture);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, size * 4);
			}

			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			glDisable(GL_TEXTURE_2D);

			break;
		}
		case EPT_LINE:
		{
			end_callback();

			break;
		}
		case EPT_DOTTED_LINE:
		{
			const unsigned pnt_num = current_obj.size();
			const double dash_length = 0.001;
			bool is_dash = true;
			unsigned v;
			double k, b, d = 0;
			vector<CPoint> lines;
			CPoint prev, cur, first, second;

			for(v = 1; v < pnt_num; v++)
			{
				prev = current_obj[v - 1];
				cur = current_obj[v];

				d += CPoint::distance(prev, cur);

				first = prev;

				if(prev.x != cur.x)
				{
					CPoint d = prev - cur;

					k = d.y / d.x;
					b = (d.x * cur.y - d.y * cur.x) / d.x;
				}

				while(d > dash_length)
				{
					second.z = prev.z;

					if(prev.x == cur.x)
					{
						second.x = prev.x;
						second.y = prev.y + dash_length;
					}
					else
					{
						const double A = k * k + 1;
						const double B = 2 * k * (b - first.y) - 2 * first.x;
						const double C = pow(first.x, 2) + pow(b - first.y, 2) - dash_length * dash_length;

						const double D = sqrt(B * B - 4 * A * C);

						second.x = (- B + D) / (2 * A);
						second.y = k * second.x + b;

						if(CPoint::distance(cur, second) > d)
						{
							second.x = (- B - D) / (2 * A);
							second.y = k * second.x + b;
						}
					}

					if(is_dash)
					{
						lines.push_back(first);
						lines.push_back(second);
					}

					first = second;
					d -= dash_length;
					is_dash = ! is_dash;
				}
			}

			if(is_dash)
			{
				lines.push_back(first);
				lines.push_back(cur);
			}

			current_obj = lines;

			end_callback();

			break;
		}
		case EPT_POLYGON:
		{
			unsigned v;

			new_contour(tess, pnt_buf, polygon_pnts);

			gluTessEndPolygon(tess);

			for(v = 0; v < pnt_buf.size(); v++)
				delete [] pnt_buf[v];

			break;
		}
		case EPT_POLYGON_HOLE:
		{
			new_contour(tess, pnt_buf, polygon_pnts);

			break;
		}
	}
}

