
#include "renderer/draw_engine/draw_engine_cairo.hpp"

CCairoDrawEngine::CCairoDrawEngine(const CPalette & __palette) :
	CDrawEngine(__palette)
{
	;
}

void CCairoDrawEngine::set_color(const CColor & color)
{
	// Скорее всего, внутреннее представление в Cairo - BGRA, следовательно, get_data() возвращает указатель именно на массив BGRA
	ctx->set_source_rgba(color.b, color.g, color.r, color.a);
}

void CCairoDrawEngine::__begin_texture(const unsigned __texture_size, const CPoint & center, const double max_r)
{
	texture_size = __texture_size;
	img = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, texture_size, texture_size);
	ctx = Cairo::Context::create(img);

	set_color(palette.substrate);
	ctx->paint();

	// ############################################################################ 

	CPoint min_pnt, max_pnt, a, b;

	min_pnt.x = center.x - max_r;
	min_pnt.y = center.y - max_r;
	min_pnt.z = -1;

	max_pnt.x = center.x + max_r;
	max_pnt.y = center.y + max_r;
	max_pnt.z = 1;
	
	a.x = 1 / (max_pnt.x - min_pnt.x);
	a.y = 1 / (max_pnt.y - min_pnt.y);

	b.x = - a.x * min_pnt.x;
	b.y = - a.y * min_pnt.y;

	scene_to_texture = [ this, a, b ](const CPoint & pnt)
	{
		CPoint res_pnt;

		res_pnt.x = (a.x * pnt.x + b.x) * texture_size;
		res_pnt.y = (a.y * pnt.y + b.y) * texture_size;
		res_pnt.z = 0;

		return res_pnt;
	};
}

GLuint CCairoDrawEngine::__end_texture()
{
	GLuint texture_ind;

	img->flush();

	glGenTextures(1, & texture_ind);
	glBindTexture(GL_TEXTURE_2D, texture_ind);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_size, texture_size, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->get_data());
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR); // TODO Ошибка в Windows
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	return texture_ind;
}

void CCairoDrawEngine::__begin_classifier_load()
{
	;
}

void CCairoDrawEngine::__end_classifier_load()
{
	;
}

unsigned CCairoDrawEngine::__create_mark(shared_ptr<uint8_t> data)
{
	unsigned v, u;
	unsigned char * p_mark;
	const uint8_t * p_data;
	Cairo::RefPtr<Cairo::ImageSurface> mark = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, 32, 32);
	const int mark_stride = mark->get_stride();

	marks.push_back(mark);
	mark->flush();

	memset(mark->get_data(), 0, mark_stride * 32);

	for(v = 0, p_data = data.get(); v < 32; v++)
		for(u = 0, p_mark = mark->get_data() + v * mark_stride; u < 32 * 4; u++, p_mark++, p_data++)
			* p_mark = * p_data;

	mark->mark_dirty();

	return marks.size() - 1;
}

void CCairoDrawEngine::__begin(const e_primitive_type primitive_type)
{
	set_color(color);

	switch(primitive_type)
	{
		// default'а нет, чтобы не пропустить реализацию чего-нибудь
		case EPT_POINT:
		case EPT_POINT_WITH_MARK:
		case EPT_LINE:
		case EPT_POLYGON:
		case EPT_TEXT:
		{
			break;
		}
		case EPT_DOTTED_LINE:
		{
			vector<double> dashes =
			{
				line.accent_len,
				line.space_len
			};

			ctx->set_dash(dashes, 0);

			break;
		}
		case EPT_POLYGON_HOLE:
		{
			ctx->set_fill_rule(Cairo::FILL_RULE_EVEN_ODD);
			ctx->begin_new_sub_path();

			break;
		}
	}
}

void CCairoDrawEngine::__vertex(const e_primitive_type primitive_type, const CPoint & pnt)
{
	switch(primitive_type)
	{
		case EPT_POINT:
		{
			ctx->rectangle(pnt.x, pnt.y, 1, 1);
			
			break;
		}
		case EPT_POINT_WITH_MARK:
		{
			ctx->set_source(marks[point.mark_index], pnt.x, pnt.y);
			ctx->paint();

			break;
		}
		case EPT_LINE:
		case EPT_DOTTED_LINE:
		case EPT_POLYGON:
		case EPT_POLYGON_HOLE:
		{
			if(is_first)
				ctx->move_to(pnt.x, pnt.y);
			else
				ctx->line_to(pnt.x, pnt.y);

			break;
		}
		case EPT_TEXT:
		{
			ctx->move_to(pnt.x, pnt.y);
			ctx->show_text(text.label);
			
			break;
		}
	}
}

void CCairoDrawEngine::__end(const e_primitive_type primitive_type)
{
	switch(primitive_type)
	{
		case EPT_POINT:
		case EPT_POINT_WITH_MARK:
		case EPT_TEXT:
		{
			break;
		}
		case EPT_LINE:
		{
			ctx->stroke();

			break;
		}
		case EPT_DOTTED_LINE:
		{
			ctx->stroke();
			ctx->unset_dash();

			break;
		}
		case EPT_POLYGON:
		{
			ctx->close_path();
			ctx->fill();

			break;
		}
		case EPT_POLYGON_HOLE:
		{
			ctx->close_path();

			break;
		}
	}
}

