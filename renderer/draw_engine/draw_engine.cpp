
#include "renderer/draw_engine/draw_engine.hpp"

CDrawEngine * draw_engine = NULL;

CDrawEngine::CDrawEngine(const CPalette & __palette) :
	palette(__palette)
{
	is_first = false;

	color = palette.substrate;
	line.thickness = 1;
	line.accent_len = 10;
	line.space_len = 10;

	scene_to_texture = [](const CPoint & pnt)
	{
		return pnt;
	};

	draw_engine = this;
}

void CDrawEngine::begin_texture(const unsigned texture_size, const CPoint & center, const double max_r)
{
	__begin_texture(texture_size, center, max_r);
}

GLuint CDrawEngine::end_texture()
{
	throw_if(primitive_stack.size());

	return __end_texture();
}

void CDrawEngine::begin_classifier_load()
{
	__begin_classifier_load();
}

void CDrawEngine::end_classifier_load()
{
	__end_classifier_load();
}

unsigned CDrawEngine::create_mark(shared_ptr<uint8_t> data)
{
	return __create_mark(data);
}

void CDrawEngine::begin(const e_primitive_type primitive_type)
{
	is_first = true;

	primitive_stack.push(primitive_type);
	__begin(primitive_type);
}

void CDrawEngine::vertex(const CPoint & pnt)
{
	throw_if(! primitive_stack.size());
	__vertex(primitive_stack.top(), scene_to_texture(pnt));

	if(is_first)
		is_first = false;
}

void CDrawEngine::vertex(const vector<CPoint> & pnts)
{
	for(auto & pnt : pnts)
		vertex(pnt);
}

void CDrawEngine::end()
{
	throw_if(! primitive_stack.size());
	__end(primitive_stack.top());
	primitive_stack.pop();

	if(is_first)
		is_first = false;
}

