
#ifndef DRAW_ENGINE_OPENGL_HPP
#define DRAW_ENGINE_OPENGL_HPP

#include "base/base.hpp"
#include "codepage/codepage.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

class CPositionOnTexture
{
	CPoint pos;
	double height, width;

	public:

		CPositionOnTexture();
		CPositionOnTexture & operator=(const CPositionOnTexture & op);
		void set(CPoint __pos, const double __width, const double __height);
		CPoint from();
		CPoint to();
};

class COpenGLDrawEngine : public CDrawEngine
{
	const unsigned aux_texture_size = 1024;
	bool is_renderer_texture_create;
	unsigned renderer_texture_size;
	GLuint renderer_texture_ind;
	vector<CPositionOnTexture> marks;
	vector< shared_ptr<uint8_t> > marks_buf;
	GLuint framebuffer_ind, letters_texture, marks_texture;
	GLUtesselator * tess;
	vector<CPoint> polygon_pnts;
	vector<GL_FLOAT_TYPE *> pnt_buf;
	map<FT_ULong, CPositionOnTexture> letters;

	unsigned create_assistant_texture(const uint8_t * data, const unsigned height, const unsigned width);
	void set_texture_params();
	void create_text_rendering();

	protected:

		void __begin_texture(const unsigned texture_size, const CPoint & center, const double max_r);
		GLuint __end_texture();
		void __begin_classifier_load();
		void __end_classifier_load();
		unsigned __create_mark(shared_ptr<uint8_t> data);
		void __begin(const e_primitive_type primitive_type);
		void __vertex(const e_primitive_type primitive_type, const CPoint & pnt);
		void __end(const e_primitive_type primitive_type);

	public:

		COpenGLDrawEngine(const CPalette & __palette);
		~COpenGLDrawEngine();
};

#endif

