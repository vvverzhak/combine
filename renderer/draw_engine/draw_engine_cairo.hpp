
#ifndef DRAW_ENGINE_CAIRO_HPP
#define DRAW_ENGINE_CAIRO_HPP

#include "base/base.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

class CCairoDrawEngine : public CDrawEngine
{
	unsigned texture_size;
	vector<Cairo::RefPtr<Cairo::ImageSurface> > marks;
	Cairo::RefPtr<Cairo::ImageSurface> img;
	Cairo::RefPtr<Cairo::Context> ctx;

	void set_color(const CColor & color);

	protected:

		void __begin_texture(const unsigned __texture_size, const CPoint & center, const double max_r);
		GLuint __end_texture();
		void __begin_classifier_load();
		void __end_classifier_load();
		unsigned __create_mark(shared_ptr<uint8_t> data);
		void __begin(const e_primitive_type primitive_type);
		void __vertex(const e_primitive_type primitive_type, const CPoint & pnt);
		void __end(const e_primitive_type primitive_type);

	public:

		CCairoDrawEngine(const CPalette & __palette);
};

#endif

