
#ifndef DRAW_ENGINE_HPP
#define DRAW_ENGINE_HPP

#include "base/base.hpp"
#include "palette/color.hpp"
#include "palette/palette.hpp"

enum e_primitive_type
{
	EPT_POINT, EPT_POINT_WITH_MARK, EPT_LINE, EPT_DOTTED_LINE, EPT_POLYGON, EPT_POLYGON_HOLE, EPT_TEXT
};

class CDrawEngine
{
	stack<e_primitive_type> primitive_stack;

	protected:

		bool is_first;
		const CPalette & palette;
		function<CPoint(const CPoint &)> scene_to_texture;

		virtual void __begin_texture(const unsigned texture_size, const CPoint & center, const double max_r) = 0;
		virtual GLuint __end_texture() = 0;
		virtual void __begin_classifier_load() = 0;
		virtual void __end_classifier_load() = 0;
		virtual unsigned __create_mark(shared_ptr<uint8_t> data) = 0;
		virtual void __begin(const e_primitive_type primitive_type) = 0;
		virtual void __vertex(const e_primitive_type primitive_type, const CPoint & pnt) = 0;
		virtual void __end(const e_primitive_type primitive_type) = 0;

	public:

		CColor color;
		
		struct
		{
			unsigned mark_index;
		} point;

		struct
		{
			double thickness, accent_len, space_len;
		} line;

		struct
		{
			string label;
		} text;

		CDrawEngine(const CPalette & __palette);

		void begin_texture(const unsigned texture_size, const CPoint & center, const double max_r);
		GLuint end_texture();
		void begin_classifier_load();
		void end_classifier_load();
		unsigned create_mark(shared_ptr<uint8_t> data);
		void begin(const e_primitive_type primitive_type);
		void vertex(const CPoint & pnt);
		void vertex(const vector<CPoint> & pnts);
		void end();
};

extern CDrawEngine * draw_engine;

#endif

