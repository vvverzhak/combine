
#include "renderer/classifier/primitive.hpp"

void CPrimitive::init(CFile & fl, const unsigned offset, const SRSCScreenParameter & desc, CRSCPalette * palette)
{
	__desc = desc;
	__palette = palette;

	par_read(fl, offset);
	par_prepare();
}

void CPrimitive::init(const uint8_t * buf, const SRSCScreenParameter & desc, CRSCPalette * palette)
{
	const unsigned par_size = desc.len - sizeof(desc);
	unsigned v;
	uint8_t * p_par;

	__desc = desc;
	__palette = palette;

	par.reset(new uint8_t[par_size], std::default_delete<uint8_t[]>());
	throw_null(p_par = par.get());

	for(v = 0; v < par_size; v++)
		p_par[v] = buf[v];

	par_prepare();
}

void CPrimitive::par_read(CFile & fl, const unsigned offset)
{
	par_size = __desc.len - sizeof(__desc);
	par.reset(new uint8_t[par_size], std::default_delete<uint8_t[]>());
	throw_if(! par);

	fl.seek(offset + sizeof(__desc));
	fl(par.get(), par_size);
}

CPrimitive * CPrimitive::create(CFile & fl, const unsigned offset, CRSCPalette * palette)
{
	CPrimitive * prm;
	SRSCScreenParameter desc;

#define CREATE_PRIMITIVE_FROM_FILE(ind, cls)\
	case ind:\
	{\
		throw_null(prm = new cls());\
		prm->init(fl, offset, desc, palette);\
\
		break;\
	}

	desc = CPrimitive::desc_read(fl, offset);
	CPrimitive::desc_unpack(desc);

	switch(desc.func_num)
	{
		CREATE_PRIMITIVE_FROM_FILE(128, CPrimitive_SimpleLine);
		CREATE_PRIMITIVE_FROM_FILE(129, CPrimitive_DottedLine);
		CREATE_PRIMITIVE_FROM_FILE(135, CPrimitive_Polygon);
		CREATE_PRIMITIVE_FROM_FILE(142, CPrimitive_Text);
		CREATE_PRIMITIVE_FROM_FILE(143, CPrimitive_Point);
		CREATE_PRIMITIVE_FROM_FILE(144, CPrimitive_144);
		CREATE_PRIMITIVE_FROM_FILE(146, CPrimitive_146);
		CREATE_PRIMITIVE_FROM_FILE(147, CPrimitive_MultiObject);
		CREATE_PRIMITIVE_FROM_FILE(149, CPrimitive_149);
		CREATE_PRIMITIVE_FROM_FILE(150, CPrimitive_150);
		CREATE_PRIMITIVE_FROM_FILE(153, CPrimitive_153);

		default:
		{
			throw_;
		}
	}

	return prm;
}

CPrimitive * CPrimitive::create(const unsigned func_num, const unsigned par_size, const uint8_t * buf, CRSCPalette * palette)
{
	CPrimitive * prm;
	SRSCScreenParameter desc;

#define CREATE_PRIMITIVE_FROM_BUFFER(ind, cls)\
	case ind:\
	{\
		throw_null(prm = new cls());\
		prm->init(buf, desc, palette);\
\
		break;\
	}

	desc.len = sizeof(desc) + par_size; // Размер примитива = заголовок + буфер
	desc.private_code = 0;
	desc.func_num = func_num;

	switch(desc.func_num)
	{
		CREATE_PRIMITIVE_FROM_BUFFER(127, CPrimitive_127);
		CREATE_PRIMITIVE_FROM_BUFFER(128, CPrimitive_SimpleLine);
		CREATE_PRIMITIVE_FROM_BUFFER(129, CPrimitive_DottedLine);
		CREATE_PRIMITIVE_FROM_BUFFER(135, CPrimitive_Polygon);
		CREATE_PRIMITIVE_FROM_BUFFER(142, CPrimitive_Text);
		CREATE_PRIMITIVE_FROM_BUFFER(143, CPrimitive_Point);
		CREATE_PRIMITIVE_FROM_BUFFER(144, CPrimitive_144);
		CREATE_PRIMITIVE_FROM_BUFFER(146, CPrimitive_146);
		CREATE_PRIMITIVE_FROM_BUFFER(147, CPrimitive_MultiObject);
		CREATE_PRIMITIVE_FROM_BUFFER(149, CPrimitive_149);
		CREATE_PRIMITIVE_FROM_BUFFER(150, CPrimitive_150);
		CREATE_PRIMITIVE_FROM_BUFFER(153, CPrimitive_153);

		default:
		{
			throw_;
		}
	}

	return prm;
}

SRSCScreenParameter CPrimitive::desc_read(CFile & fl, const unsigned offset)
{
	SRSCScreenParameter desc;

	fl.seek(offset);
	fl(& desc, sizeof(desc));

	return desc;
}

void CPrimitive::desc_unpack(SRSCScreenParameter & desc)
{
	CFile::unpack<uint32_t>(& desc.len);
	CFile::unpack<uint16_t>(& desc.private_code);
	CFile::unpack<uint16_t>(& desc.func_num);
}

void CPrimitive::draw(const CObject & obj)
{
	if(obj.type != EOT_NOT_DRAW)
		base_draw(obj);
}

void CPrimitive::draw_line(const CObject & obj, const CColor color, const bool is_dotted)
{
	draw_engine->color = color;

	draw_engine->begin(is_dotted ? EPT_DOTTED_LINE : EPT_LINE);

	for(auto & pnt : obj.pnt_scene)
		draw_engine->vertex(pnt);

	draw_engine->end();
}

// ############################################################################ 

void CPrimitive_127::par_prepare()
{
	; // TODO
}

void CPrimitive_127::base_draw(const CObject & obj)
{
	; // TODO
}

// ############################################################################ 
// Простая линия

void CPrimitive_SimpleLine::par_prepare()
{
	uint32_t * buf = (uint32_t *) par.get();

	CFile::unpack<uint32_t>(buf, 2);

	color = (* __palette)(buf[0]);	// Цвет линии
	thickness = buf[1];				// Толщина линии
}

void CPrimitive_SimpleLine::base_draw(const CObject & obj)
{
	draw_line(obj, color, false);
}

// ############################################################################ 
// Пунктирная линия

void CPrimitive_DottedLine::par_prepare()
{
	uint32_t * buf = (uint32_t *) par.get();

	CFile::unpack<uint32_t>(buf, 4);

	color = (* __palette)(buf[0]);	// Цвет линии
	thickness = buf[1];				// Толщина линии
	accent_len = buf[2];			// Длина штриха
	space_len = buf[3];				// Длина пробела
}

void CPrimitive_DottedLine::base_draw(const CObject & obj)
{
	draw_engine->line.accent_len = 10;
	draw_engine->line.space_len = 10;

	draw_line(obj, color, true);
}

// ############################################################################ 
// Площадь

void CPrimitive_Polygon::par_prepare()
{
	uint32_t * buf = (uint32_t *) par.get();

	CFile::unpack<uint32_t>(buf);

	color = (* __palette)(buf[0]);	// Цвет площади
}

void CPrimitive_Polygon::base_draw(const CObject & obj)
{
	auto draw_polygon = [](const CObject & obj)
	{
		for(auto & pnt : obj.pnt_scene)
			draw_engine->vertex(pnt);
	};

	draw_engine->color = color;

	draw_engine->begin(EPT_POLYGON);
	draw_polygon(obj);

	for(auto & sub_obj : obj.sub_obj)
	{
		draw_engine->begin(EPT_POLYGON_HOLE);
		draw_polygon(sub_obj);
		draw_engine->end();
	}

	draw_engine->end();
}

// ############################################################################ 

void CPrimitive_Text::par_prepare()
{
	uint8_t * buf = par.get();

	CFile::unpack<uint32_t>((uint32_t *) buf, 5);
	CFile::unpack<uint16_t>((uint16_t *) (buf + 20), 2);
	CFile::unpack<uint8_t>(buf + 24, 8);

	fg_color = (* __palette)(* (uint32_t *) buf);
//	bg_color = (* __palette)(* (uint32_t *) (buf + 4)); // TODO Segfault
//	shade_color = (* __palette)(* (uint32_t *) (buf + 8)); // TODO Segfault
	height = * (uint32_t *) (buf + 12);
	thickness = * (uint32_t *) (buf + 16);
	align = * (uint16_t *) (buf + 20);
	not_used = * (uint16_t *) (buf + 22);
	symbol_width = * (buf + 24);
	is_horizontal_decomposition =	* (buf + 25);
	is_cursive = * (buf + 26);
	is_underlining = * (buf + 27);
	is_overlining = * (buf + 28);
	font_type = * (buf + 29);
	codepage = * (buf + 30);
	is_scale_on_metrics = * (buf + 31);
}

void CPrimitive_Text::base_draw(const CObject & obj)
{
	draw_engine->color = fg_color;
	draw_engine->text.label = obj.label;

	draw_engine->begin(EPT_TEXT);

	for(auto & __pnt : obj.pnt_scene)
		draw_engine->vertex(__pnt);

	draw_engine->end();
}

// ############################################################################ 

void CPrimitive_Point::par_prepare()
{
	shared_ptr<uint8_t> mark;
	uint8_t * p_mark, * __p_mark, * buf_8 = par.get();
	uint32_t * buf = (uint32_t *) buf_8;
	const unsigned mark_size = 32 * 32 * 4;
	unsigned v, u, t, tv, tu, ind;

	CFile::unpack<uint32_t>(buf, 5);

	len = buf[0];
	color_num = buf[1];
	side_size = buf[2];
	y = buf[3];
	x = buf[4];

	CFile::unpack<uint32_t>(buf + 5, color_num);

	mark.reset(new uint8_t[mark_size], std::default_delete<uint8_t[]>());
	throw_null(p_mark = mark.get());

	for(v = 0, ind = 5; v < color_num; v++)
	{
		CColor color = CColor::to_rgb((* __palette)(buf[ind]));
		ind++;

		for(tv = 0; tv < 32; tv++, ind++)
			for(tu = 0, u = 0; u < 4; u++)
			{
				uint8_t byte = buf_8[ind * 4 + u];

				CFile::unpack<uint8_t>(& byte);

				for(t = 0; t < 8; t++, tu++, byte <<= 1)
				{
					__p_mark = p_mark + (tv * 32  + tu) * 4;

					* __p_mark = color.r;
					* (__p_mark + 1) = color.g;
					* (__p_mark + 2) = color.b;
					* (__p_mark + 3) = (byte & 0x80) ? 255 : 0;
				}
			}
	}

	mark_index = draw_engine->create_mark(mark);
}

void CPrimitive_Point::base_draw(const CObject & obj)
{
	draw_engine->point.mark_index = mark_index;
	draw_engine->begin(EPT_POINT_WITH_MARK);

	for(auto pnt : obj.pnt_scene)
		draw_engine->vertex(pnt);

	draw_engine->end();
}

// ############################################################################ 

void CPrimitive_144::par_prepare()
{
	; // TODO
}

void CPrimitive_144::base_draw(const CObject & obj)
{
	; // TODO
}

// ############################################################################ 

void CPrimitive_146::par_prepare()
{
	; // TODO
}

void CPrimitive_146::base_draw(const CObject & obj)
{
	; // TODO
}

// ############################################################################ 

void CPrimitive_MultiObject::par_prepare()
{
	unsigned v;
	uint8_t * buf_8 = par.get();
	uint16_t * buf_16 = (uint16_t *) (buf_8 + 12);
	uint32_t * buf = (uint32_t *) buf_8;
	CPrimitive * prm;

	CFile::unpack<uint32_t>(buf, 3);

	id = buf[0];
	len = buf[1];
	primitives_number = buf[2];

	for(v = 0; v < primitives_number; v++)
	{
		uint16_t par_size = buf_16[0];
		uint16_t func_num = buf_16[1];

		CFile::unpack<uint16_t>(& par_size);
		CFile::unpack<uint16_t>(& func_num);

		if(func_num)
		{
			uint8_t * ptr = (uint8_t *) (buf_16 + 2);
			const unsigned size = par_size - 4; // (par_size - 4) - четверка компенсирует размер полей par_size и func_num

			CFile::unpack<uint8_t>(ptr, size);

			throw_null(prm = CPrimitive::create(func_num, size, ptr, __palette));
			prms.push_back(shared_ptr<CPrimitive>(prm));
		}

		buf_16 += par_size / 2;
	}
}

void CPrimitive_MultiObject::base_draw(const CObject & obj)
{
	for(auto & prm : prms)
		prm->base_draw(obj);
}

// ############################################################################ 

void CPrimitive_149::par_prepare()
{
	; // TODO
}

void CPrimitive_149::base_draw(const CObject & obj)
{
	; // TODO
}

// ############################################################################ 

void CPrimitive_150::par_prepare()
{
	; // TODO
}

void CPrimitive_150::base_draw(const CObject & obj)
{
	; // TODO
}

// ############################################################################ 

void CPrimitive_153::par_prepare()
{
	; // TODO
}

void CPrimitive_153::base_draw(const CObject & obj)
{
	; // TODO
}

