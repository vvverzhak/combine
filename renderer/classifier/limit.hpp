
#ifndef LIMIT_HPP
#define LIMIT_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "renderer/classifier/primitive.hpp"

struct __attribute__((packed)) SRSCLimit
{
	uint32_t len;
	uint32_t classification_code;
	uint8_t localization;
	uint8_t not_used[7];

	struct
	{
		uint32_t code;
		uint16_t limits_number;
		uint16_t default_value;
	} first_sem, second_sem;
};

class CLimit
{
	unsigned sem_codes[2];
	unsigned default_limit_value[2];
	map<uint64_t, map<uint64_t, unsigned> > limits;
	SRSCLimit desc;
	string prefix;

	void unpack_desc();

	public:

		CLimit(CFile & fl, const unsigned offset);

		static string key_for_non_limit_object(const CObject & obj);
		string key(const CObject & obj);

		inline unsigned length() { return desc.len; };
		inline unsigned classification_code() { return desc.classification_code; };
		inline unsigned localization() { return desc.localization; };
};

#endif

