
#include "renderer/classifier/object.hpp"

CRSCObject::CRSCObject(CFile & fl, const unsigned offset, map<unsigned, shared_ptr<CPrimitive> > & prms)
{
	read(fl, offset);
	unpack();

	throw_if(! prms.count(desc.private_code));
	prm = prms[desc.private_code];
}

void CRSCObject::read(CFile & fl, const unsigned offset)
{
	fl.seek(offset);
	fl(& desc, sizeof(desc));
}

void CRSCObject::unpack()
{
	CFile::unpack<uint32_t>(& desc.len);
	CFile::unpack<uint32_t>(& desc.classification_code);
	CFile::unpack<uint32_t>(& desc.private_code);
	CFile::unpack<uint32_t>(& desc.id);
	CFile::unpack<uint8_t>((uint8_t *) desc.short_name, 32);
	CFile::unpack<uint8_t>((uint8_t *) desc.name, 32);
	CFile::unpack<uint8_t>(& desc.localization);
	CFile::unpack<uint8_t>(& desc.layer_ind);
	CFile::unpack<uint8_t>(& desc.not_used_3);
	CFile::unpack<uint8_t>(& desc.not_used_4);
	CFile::unpack<uint8_t>(& desc.not_used_5);
	CFile::unpack<uint8_t>(& desc.not_used_6);
	CFile::unpack<uint8_t>(& desc.not_used_7);
	CFile::unpack<uint8_t>(& desc.not_used_8);
	CFile::unpack<uint16_t>(& desc.extension);
	CFile::unpack<uint8_t>(& desc.not_used_10);
	CFile::unpack<uint8_t>(& desc.not_used_11);
	CFile::unpack<uint8_t>(& desc.not_used_12);
	CFile::unpack<uint8_t>(& desc.not_used_13);
	CFile::unpack<uint8_t>(& desc.not_used_14);
	CFile::unpack<uint8_t>(& desc.not_used_15);
}

void CRSCObject::draw(const CObject & obj)
{
	prm->draw(obj);
}

