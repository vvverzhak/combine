
#include "renderer/classifier/limit.hpp"

template<typename tp>
void unpack_limits(tp * ptr, const unsigned num)
{
	unsigned v;

	for(v = 0; v < num; v++)
		CFile::unpack<tp>(ptr + v);
}

template<typename tp>
tp * read_limits(CFile & fl, const unsigned lnum, shared_ptr<tp> & buf)
{
	tp * p_buf;

	buf.reset(new tp[lnum], std::default_delete<tp []>());
	throw_null(p_buf = buf.get());
	fl(p_buf, lnum * sizeof(tp));

	unpack_limits(p_buf, lnum);

	return p_buf;
}

CLimit::CLimit(CFile & fl, const unsigned offset)
{
	fl.seek(offset);
	fl(& desc, sizeof(desc));

	unpack_desc();

	// ############################################################################ 

	const unsigned first_num = desc.first_sem.limits_number, second_num = desc.second_sem.limits_number;
	unsigned v, u, limits_size;
	shared_ptr<uint64_t> first_limits, second_limits;
	shared_ptr<uint8_t> __limits;
	uint64_t * p_first_limits, * p_second_limits;
	uint8_t * p_limits;

	p_first_limits = read_limits(fl, first_num, first_limits);
	limits_size = first_num;

	if(second_num)
	{
		p_second_limits = read_limits(fl, second_num, second_limits);
		limits_size *= second_num;
	}

	p_limits = read_limits(fl, limits_size, __limits);
	
	if(second_num)
	{
		for(v = 0; v < first_num; v++)
			for(u = 0; u < second_num; u++)
				limits[p_first_limits[v]][p_second_limits[u]] = p_limits[u * first_num + v];
	}
	else
	{
		for(v = 0; v < first_num; v++)
			limits[p_first_limits[v]][0] = p_limits[v];
	}
}

void CLimit::unpack_desc()
{
	CFile::unpack<uint32_t>(& desc.len);
	CFile::unpack<uint32_t>(& desc.classification_code);
	CFile::unpack<uint16_t>((uint16_t *) & desc.localization, 4);
//	CFile::unpack<uint8_t>(desc.not_used, 7);

	CFile::unpack<uint32_t>(& desc.first_sem.code);
	CFile::unpack<uint16_t>(& desc.first_sem.limits_number);
	CFile::unpack<uint16_t>(& desc.first_sem.default_value);

	CFile::unpack<uint32_t>(& desc.second_sem.code);
	CFile::unpack<uint16_t>(& desc.second_sem.limits_number);
	CFile::unpack<uint16_t>(& desc.second_sem.default_value);
}

string CLimit::key_for_non_limit_object(const CObject & obj)
{
	char buf[4096];
	string prefix;

	switch(obj.localization)
	{
#define LCZN(code, prfx)\
		case code:\
		{\
			prefix = prfx;\
		\
			break;\
		}

#define LCZN_TODO(code, prfx)\
		case code:\
		{\
			printf_TODO("Localization %s\n", prfx);\
		\
			break;\
		}
		LCZN(2, "P00")
		LCZN(0, "L00")
		LCZN(1, "S00")
		LCZN(3, "T00")
		LCZN_TODO(4, "Vector")
		LCZN_TODO(5, "Text template")
	}

	sprintf(buf, "%s%u", prefix.c_str(), obj.code);

	return buf;
}

string CLimit::key(const CObject & obj)
{
	const string prefix = CLimit::key_for_non_limit_object(obj);
	const unsigned first_code = desc.first_sem.code, second_code = desc.second_sem.code;
	const uint64_t first_default_value = desc.first_sem.default_value, second_default_value = desc.second_sem.default_value;

	if(obj.code == 45100000) // && obj.id == 16786378)
		printf_TODO("BEGINNNNNNNNNNNN %u\n", obj.id);

	for(auto & first : limits)
	{
		if(obj.cmp_double_semantics(first_code, first.first, first_default_value) <= 0)
		{
			for(auto & second : first.second)
			{
				if(obj.cmp_double_semantics(second_code, second.first, second_default_value) <= 0)
				{
					const unsigned code = second.second - 1; // TODO Вычитание? Почему? Зачем?

					if(code)
					{
						char buf[4096];

						sprintf(buf, "%s%u", prefix.c_str(), code);

	if(obj.code == 45100000 && obj.id == 16786378)
		printf_TODO("IN 1 %u %s\n", obj.localization, buf);

						return buf;
					}
					else
					{
	if(obj.code == 45100000 && obj.id == 16786378)
		printf_TODO("IN 2 %u %s\n", obj.localization, prefix.c_str());
						return prefix;
					}
				}
			}

			// TODO А что делать, если во втором цикле не найдено совпадений?
		}
	}

	if(obj.code == 45100000 && obj.id == 16786378)
		printf_TODO("IN 3 %u %s\n", obj.localization, prefix.c_str());

	return prefix;
}

