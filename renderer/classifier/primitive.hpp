
#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "renderer/classifier/palette.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

#define LAYER_NUM 256

struct __attribute__((packed)) SRSCScreenParameter
{
	uint32_t len;				// Длина записи
	uint16_t private_code;		// Внутренний код объекта
	uint16_t func_num;			// Номер функции отображения
};

class CPrimitive
{
	protected:

		unsigned par_size;
		shared_ptr<uint8_t> par;
		SRSCScreenParameter __desc;
		CRSCPalette * __palette;

		void par_read(CFile & fl, const unsigned offset);
		virtual void par_prepare() = 0;

		void draw_line(const CObject & obj, const CColor color, const bool is_dotted);

	public:

		static CPrimitive * create(CFile & fl, const unsigned offset, CRSCPalette * palette);
		static CPrimitive * create(const unsigned func_num, const unsigned par_size, const uint8_t * buf, CRSCPalette * palette);
		static SRSCScreenParameter desc_read(CFile & fl, const unsigned offset);
		static void desc_unpack(SRSCScreenParameter & desc);

		void init(CFile & fl, const unsigned offset, const SRSCScreenParameter & desc, CRSCPalette * palette);
		void init(const uint8_t * buf, const SRSCScreenParameter & desc, CRSCPalette * palette);
		void draw(const CObject & obj);

		virtual void base_draw(const CObject & obj) = 0;
		virtual string class_name() = 0;

		inline unsigned length() { return __desc.len; };
		inline unsigned private_code() { return __desc.private_code; };
};

#define PRIMITIVE_CLASS(cls, cls_name, fields)\
class cls : public CPrimitive\
{\
	protected:\
\
		fields;\
\
		void par_prepare();\
\
	public:\
\
		void base_draw(const CObject & obj);\
		string class_name() { return cls_name; };\
};

#define COMMA ,
#define PARCOMMA ;

PRIMITIVE_CLASS(CPrimitive_127, "CPrimitive_127", );
PRIMITIVE_CLASS(CPrimitive_SimpleLine, "CPrimitive_SimpleLine", CColor color PARCOMMA uint32_t thickness);
PRIMITIVE_CLASS(CPrimitive_DottedLine, "CPrimitive_DottedLine", CColor color PARCOMMA uint32_t thickness COMMA accent_len COMMA space_len);
PRIMITIVE_CLASS(CPrimitive_Polygon, "CPrimitive_Polygon", CColor color);
PRIMITIVE_CLASS(CPrimitive_Text, "CPrimitive_Text", \
		CColor fg_color COMMA bg_color COMMA shade_color PARCOMMA\
		uint32_t height COMMA thickness PARCOMMA\
		uint16_t align COMMA not_used PARCOMMA\
		uint8_t symbol_width COMMA font_type COMMA codepage PARCOMMA\
		bool is_horizontal_decomposition COMMA is_cursive COMMA is_underlining COMMA is_overlining COMMA is_scale_on_metrics);
PRIMITIVE_CLASS(CPrimitive_Point, "CPrimitive_Point", uint32_t len COMMA color_num COMMA side_size COMMA y COMMA x PARCOMMA CColor mask_color PARCOMMA unsigned mark_index);
PRIMITIVE_CLASS(CPrimitive_144, "CPrimitive_144", );
PRIMITIVE_CLASS(CPrimitive_146, "CPrimitive_146", );
PRIMITIVE_CLASS(CPrimitive_MultiObject, "CPrimitive_MultiObject", uint32_t id COMMA len COMMA primitives_number PARCOMMA vector< shared_ptr<CPrimitive> > prms);
PRIMITIVE_CLASS(CPrimitive_149, "CPrimitive_149", );
PRIMITIVE_CLASS(CPrimitive_150, "CPrimitive_150", );
PRIMITIVE_CLASS(CPrimitive_153, "CPrimitive_153", );

#endif

