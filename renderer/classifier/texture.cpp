
#include "renderer/classifier/texture.hpp"

CClassifierTexture::CClassifierTexture(const unsigned __texture_size, const CPoint __center, const double __max_r) :
	CTexture(__texture_size, __center, __max_r)
{
	;
}

void CClassifierTexture::draw(map<unsigned, vector<t_object_to_draw> > & layers)
{
	int layer_ind;

	draw_engine->begin_texture(texture_size, center, max_r);

	// TODO Разобратся с порядком отрисовки слоев
	// for(layer_ind = 0; layer_ind < LAYER_NUM; layer_ind++)
	for(layer_ind = LAYER_NUM - 1; layer_ind >= 0; layer_ind--)
		if(layers.count(layer_ind))
			for(auto & obj : layers[layer_ind])
				obj.first->draw(obj.second);
	
	ind = draw_engine->end_texture();
}

