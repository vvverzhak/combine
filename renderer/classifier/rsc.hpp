
#ifndef RSC_HPP
#define RSC_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "renderer/classifier/object.hpp"
#include "renderer/classifier/primitive.hpp"
#include "renderer/classifier/limit.hpp"
#include "renderer/classifier/palette.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

struct __attribute__((packed)) SRSCHeader
{
	uint32_t id;						// Идентификатор файла == 0x00435352
	uint32_t len;						// Длина файла
	uint32_t version;					// Версия структуры RSC 
	uint32_t encoding;					// Кодировка
	uint32_t state;						// Номер состояния файла
	uint32_t modification;				// Номер модификации состояния
	uint32_t lang;						// Используемый язык
	uint32_t max_obj_id;				// Максимальный идентификатор  таблицы  объектов
	char creation_date[8];				// Дата создания файла
	char map_type[32];					// Тип карты
	char classificaton_name[32];		// Условное название классификатора
	char classificator_code[8];			// Код классификатора
	uint32_t scale;						// Масштаб карты 
	uint32_t not_used_1;				// Масштабный ряд
	uint32_t obj_table_offset;			// Смещение на таблицу объектов 
	uint32_t obj_table_len;				// Длина таблицы объектов
	uint32_t obj_num;					// Число записей
	uint32_t semantic_table_offset;		// Смещение на таблицу семантики 
	uint32_t semantic_table_len;		// Длина таблицы семантики
	uint32_t semantic_num;				// Число записей
	uint32_t not_used_2;				// Смещение на таблицу классификатор семантики
	uint32_t not_used_3;				// Длина таблицы классификатор семантики
	uint32_t not_used_4;				// Число записей
	uint32_t not_used_5;				// Смещение на таблицу умолчаний
	uint32_t not_used_6;				// Длина таблицы умолчаний
	uint32_t not_used_7;				// Число записей 
	uint32_t not_used_8;				// Смещение на таблицу возможных семантик
	uint32_t not_used_9;				// Длина таблицы возможных семантик
	uint32_t not_used_10;				// Число записей
	uint32_t not_used_11;				// Смещение на таблицу сегментов (слоев)
	uint32_t not_used_12;				// Длина таблицы сегментов (слоев)
	uint32_t not_used_13;				// Число записей
	uint32_t limit_table_offset;		// Смещение на таблицу порогов
	uint32_t limit_table_len;			// Длина таблицы порогов
	uint32_t limit_num;					// Число записей
	uint32_t parameter_table_offset;	// Смещение на таблицу параметров
	uint32_t parameter_table_len;		// Длина таблицы параметров
	uint32_t parameter_num;				// Число записей
	uint32_t not_used_20;				// Смещение на таблицу параметров печати
	uint32_t not_used_21;				// Длина таблицы параметров печати
	uint32_t not_used_22;				// Число записей
	uint32_t palette_table_offset;		// Смещение на таблицу палитр
	uint32_t palette_table_len;			// Длина таблицы палитр
	uint32_t palette_num;				// Число записей
	uint32_t not_used_23;				// Смещение на таблицу шрифтов
	uint32_t not_used_24;				// Длина таблицы шрифтов
	uint32_t not_used_25;				// Число записей
	uint32_t not_used_26;				// Смещение на таблицу библиотек  
	uint32_t not_used_27;				// Длина таблицы библиотек
	uint32_t not_used_28;				// Число записей
	uint32_t not_used_29;				// Смещение на таблицу изображений семантики
	uint32_t not_used_30;				// Длина таблицы  изображений семантики
	uint32_t not_used_31;				// Число записей
	uint32_t not_used_32;				// Смещение на таблицу таблиц
	uint32_t not_used_33;				// Длина таблицы таблиц
	uint32_t not_used_34;				// Число записей
	uint8_t is_flag_as_code;			// Флаг использования ключей как кодов
	uint8_t is_palette_modification;	// Флаг модификации палитры
	uint16_t not_used_35;				// Резерв
	uint32_t not_used_36;				// Резерв
	uint32_t not_used_37;				// Резерв
	uint8_t not_used_38[20];			// Резерв
	uint32_t not_used_39;				// Кодировка шрифтов
	uint32_t color_in_palette;			// Количество цветов в палитрах
};

class CRSC
{
	SRSCHeader header;

	void unpack_header();

	public:

		map<unsigned, shared_ptr<CPrimitive> > prms;
		map<string, shared_ptr<CRSCObject> > objs;
		map<unsigned, map<unsigned, shared_ptr<CLimit> > > lmts;
		CRSCPalette palette;

		CRSC(const string fname);
};

#endif

