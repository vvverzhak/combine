
#ifndef RSC_PALETTE_HPP
#define RSC_PALETTE_HPP

#include "base/base.hpp"
#include "palette/color.hpp"

#define PALETTE_SIZE 256

struct __attribute__((packed)) SRSCPalette
{
	uint32_t color[PALETTE_SIZE];
	char name[32];
};

class CRSCPalette
{
	vector<SRSCPalette> palette;

	void read(CFile & fl, const unsigned offset, const unsigned palette_item_num);
	void unpack();

	public:

		void init(CFile & fl, const unsigned offset, const unsigned palette_item_num);
		CColor operator()(const uint32_t code);
};

#endif

