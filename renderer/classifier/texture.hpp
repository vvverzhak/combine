
#ifndef CLASSIFIER_TEXTURE_HPP
#define CLASSIFIER_TEXTURE_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "renderer/texture.hpp"
#include "renderer/classifier/rsc.hpp"
#include "renderer/classifier/primitive.hpp"

typedef pair<CRSCObject *, const CObject &> t_object_to_draw;

class CClassifierTexture : public CTexture
{
	public:

		CClassifierTexture(const unsigned __texture_size, const CPoint __center, const double __max_r);

		void draw(map<unsigned, vector<t_object_to_draw> > & layers);
};

#endif

