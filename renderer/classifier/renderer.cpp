
#include "renderer/classifier/renderer.hpp"

CClassifierRenderer::CClassifierRenderer(
	const string rsc_fname,
	const unsigned __height, const unsigned __width,
	const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
	const CMap & __map, const CHeightMap & __height_map,
	const CCamera & __camera,
	const CScale & __scale,
	const CPalette & __palette) :

	CRenderer(__height, __width, __texture_size, __texture_per_row, __texture_step, __map, __height_map, __camera, __scale, __palette.background, __palette.horizont), rsc(rsc_fname), palette(__palette)
{
	;
}

void CClassifierRenderer::create_textures()
{
	unsigned v, u, ind;
	double dv, du;
	const double max_r = (scale.scene_to.x - scale.scene_from.x) / (2 * texture_per_row); // TODO Не квадратные сцены
	const double max_r_2 = 2 * max_r; // TODO Не квадратные сцены
	CClassifierTexture * p_texture;

	texture.clear();

	for(auto & obj : map.obj)
		push(obj);

	for(v = 0, ind = 0, dv = scale.scene_from.y + max_r; v < texture_per_row; v++, dv += max_r_2)
		for(u = 0, du = scale.scene_from.x + max_r; u < texture_per_row; u++, ind++, du += max_r_2)
		{
			throw_null(p_texture = new CClassifierTexture(texture_size, CPoint(du, dv, 0), max_r + seam));
			p_texture->draw(layers);
			texture.push_back(shared_ptr<CTexture>(p_texture));
		}
}

void CClassifierRenderer::push(const CObject & obj)
{
	const unsigned localization = obj.localization;
	string ind;
	
	if(rsc.lmts.count(obj.code) && rsc.lmts[obj.code].count(localization))
		ind = rsc.lmts[obj.code][localization]->key(obj);
	else
		ind = CLimit::key_for_non_limit_object(obj);

	if(obj.code == 45100000 && obj.id == 16786378)
		printf_TODO("IN !!! %u %u\n", obj.localization, localization);

	throw_if(! rsc.objs.count(ind));

	CRSCObject * __obj = rsc.objs[ind].get();
	const unsigned layer_ind = __obj->layer_ind();

	layers[layer_ind].push_back(t_object_to_draw(__obj, obj));
}

