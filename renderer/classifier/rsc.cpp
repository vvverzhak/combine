
#include "renderer/classifier/rsc.hpp"

CRSC::CRSC(const string fname)
{
	unsigned v, offset;
	CFile fl(fname.c_str(), EFileModeBinaryRead);

	draw_engine->begin_classifier_load();

	// ############################################################################ 
	// Заголовок

	fl(& header, sizeof(header));
	unpack_header();

	// ############################################################################ 
	// Палитра

	palette.init(fl, header.palette_table_offset, header.palette_num);

	// ############################################################################ 
	// Пороги (ограничения)

	CLimit * lmt;

	for(v = 0, offset = header.limit_table_offset; v < header.limit_num; v++)
	{
		throw_null(lmt = new CLimit(fl, offset));
		lmts[lmt->classification_code()][lmt->localization()] = shared_ptr<CLimit>(lmt);

		offset += lmt->length();
	}

	// ############################################################################ 
	// Примитивы (параметры экрана и печати)
	
	CPrimitive * prm;

	for(v = 0, offset = header.parameter_table_offset; v < header.parameter_num; v++)
	{
		throw_null(prm = CPrimitive::create(fl, offset, & palette));
		prms[prm->private_code()] = shared_ptr<CPrimitive>(prm);

		offset += prm->length();
	}

	// ############################################################################ 
	// Объекты

	CRSCObject * obj;

	for(v = 0, offset = header.obj_table_offset; v < header.obj_num; v++)
	{
		throw_null(obj = new CRSCObject(fl, offset, prms));
		objs[obj->short_name()] = shared_ptr<CRSCObject>(obj);

		offset += obj->length();;
	}

	// ############################################################################ 
	
	draw_engine->end_classifier_load();
}

void CRSC::unpack_header()
{
	CFile::unpack<uint32_t>(& header.id);
	CFile::unpack<uint32_t>(& header.len);
	CFile::unpack<uint32_t>(& header.version);
	CFile::unpack<uint32_t>(& header.encoding);
	CFile::unpack<uint32_t>(& header.state);
	CFile::unpack<uint32_t>(& header.modification);
	CFile::unpack<uint32_t>(& header.lang);
	CFile::unpack<uint32_t>(& header.max_obj_id);
	CFile::unpack<uint8_t>((uint8_t *) header.creation_date, 8);
	CFile::unpack<uint8_t>((uint8_t *) header.map_type, 32);
	CFile::unpack<uint8_t>((uint8_t *) header.classificaton_name, 32);
	CFile::unpack<uint8_t>((uint8_t *) header.classificator_code, 8);
	CFile::unpack<uint32_t>(& header.scale);
	CFile::unpack<uint32_t>(& header.not_used_1);
	CFile::unpack<uint32_t>(& header.obj_table_offset);
	CFile::unpack<uint32_t>(& header.obj_table_len);
	CFile::unpack<uint32_t>(& header.obj_num);
	CFile::unpack<uint32_t>(& header.semantic_table_offset);
	CFile::unpack<uint32_t>(& header.semantic_table_len);
	CFile::unpack<uint32_t>(& header.semantic_num);
	CFile::unpack<uint32_t>(& header.not_used_2);
	CFile::unpack<uint32_t>(& header.not_used_3);
	CFile::unpack<uint32_t>(& header.not_used_4);
	CFile::unpack<uint32_t>(& header.not_used_5);
	CFile::unpack<uint32_t>(& header.not_used_6);
	CFile::unpack<uint32_t>(& header.not_used_7);
	CFile::unpack<uint32_t>(& header.not_used_8);
	CFile::unpack<uint32_t>(& header.not_used_9);
	CFile::unpack<uint32_t>(& header.not_used_10);
	CFile::unpack<uint32_t>(& header.not_used_11);
	CFile::unpack<uint32_t>(& header.not_used_12);
	CFile::unpack<uint32_t>(& header.not_used_13);
	CFile::unpack<uint32_t>(& header.limit_table_offset);
	CFile::unpack<uint32_t>(& header.limit_table_len);
	CFile::unpack<uint32_t>(& header.limit_num);
	CFile::unpack<uint32_t>(& header.parameter_table_offset);
	CFile::unpack<uint32_t>(& header.parameter_table_len);
	CFile::unpack<uint32_t>(& header.parameter_num);
	CFile::unpack<uint32_t>(& header.not_used_20);
	CFile::unpack<uint32_t>(& header.not_used_21);
	CFile::unpack<uint32_t>(& header.not_used_22);
	CFile::unpack<uint32_t>(& header.palette_table_offset);
	CFile::unpack<uint32_t>(& header.palette_table_len);
	CFile::unpack<uint32_t>(& header.palette_num);
	CFile::unpack<uint32_t>(& header.not_used_23);
	CFile::unpack<uint32_t>(& header.not_used_24);
	CFile::unpack<uint32_t>(& header.not_used_25);
	CFile::unpack<uint32_t>(& header.not_used_26);
	CFile::unpack<uint32_t>(& header.not_used_27);
	CFile::unpack<uint32_t>(& header.not_used_28);
	CFile::unpack<uint32_t>(& header.not_used_29);
	CFile::unpack<uint32_t>(& header.not_used_30);
	CFile::unpack<uint32_t>(& header.not_used_31);
	CFile::unpack<uint32_t>(& header.not_used_32);
	CFile::unpack<uint32_t>(& header.not_used_33);
	CFile::unpack<uint32_t>(& header.not_used_34);
	CFile::unpack<uint8_t>(& header.is_flag_as_code);
	CFile::unpack<uint8_t>(& header.is_palette_modification);
	CFile::unpack<uint16_t>(& header.not_used_35);
	CFile::unpack<uint32_t>(& header.not_used_36);
	CFile::unpack<uint32_t>(& header.not_used_37);
	CFile::unpack<uint8_t>(header.not_used_38, 20);
	CFile::unpack<uint32_t>(& header.not_used_39);
	CFile::unpack<uint32_t>(& header.color_in_palette);
}

