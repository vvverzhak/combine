
#ifndef OBJECT_HPP
#define OBJECT_HPP

#include "base/base.hpp"
#include "renderer/classifier/primitive.hpp"

struct __attribute__((packed)) SRSCObject
{
	uint32_t len;						// Длина записи объекта
	uint32_t classification_code;		// Классификационный код
	uint32_t private_code;				// Внутренний код объекта
	uint32_t id;						// Идентификационный код
	char short_name[32];				// Короткое имя объекта
	char name[32];						// Название
	uint8_t localization;				// Характер локализации
	uint8_t layer_ind;					// Номер слоя (сегмента)
	uint8_t not_used_3;					// Признак масштабируемости
	uint8_t not_used_4;					// Нижняя граница видимости
	uint8_t not_used_5;					// Верхняя граница видимости
	uint8_t not_used_6;					// Расширение локализации
	uint8_t not_used_7;					// Направление цифрования
	uint8_t not_used_8;					// Отображение с учетом семантики
	uint16_t extension;					// Номер расширения
	uint8_t not_used_10;				// Количество связанных подписей
	uint8_t not_used_11;				// Признак сжатия объекта
	uint8_t not_used_12;				// Максимальное увеличение
	uint8_t not_used_13;				// Максимальное уменьшение
	uint8_t not_used_14;				// Флаг включения границ
	uint8_t not_used_15;				// Резерв
};

class CRSCObject
{
	SRSCObject desc;
	shared_ptr<CPrimitive> prm;

	void read(CFile & fl, const unsigned offset);
	void unpack();

	public:

		CRSCObject(CFile & fl, const unsigned offset, map<unsigned, shared_ptr<CPrimitive> > & prms);

		void draw(const CObject & obj);

		inline unsigned length() { return desc.len; };
		inline unsigned classification_code() { return desc.classification_code; };
		inline string short_name() { return desc.short_name; };
		inline unsigned private_code() { return desc.private_code; };
		inline unsigned id() { return desc.id; };
		inline unsigned extension() { return desc.extension; };
		inline unsigned layer_ind() { return desc.layer_ind; };
};

#endif

