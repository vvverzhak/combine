
#include "renderer/classifier/palette.hpp"

void CRSCPalette::init(CFile & fl, const unsigned offset, const unsigned palette_item_num)
{
	read(fl, offset, palette_item_num);
	unpack();
}

void CRSCPalette::read(CFile & fl, const unsigned offset, const unsigned palette_item_num)
{
	unsigned v;

	fl.seek(offset);

	for(v = 0; v < palette_item_num; v++)
	{
		SRSCPalette pal;

		fl(& pal, sizeof(pal));
		palette.push_back(pal);
	}
}

void CRSCPalette::unpack()
{
	for(auto & it : palette)
	{
		CFile::unpack<uint32_t>(it.color, PALETTE_SIZE);
		CFile::unpack<uint8_t>((uint8_t *) it.name, 32);
	}
}

CColor CRSCPalette::operator()(const uint32_t code)
{
	unsigned __color;
	CColor color;

	if(code & 0xFF000000)
	{
		unsigned ind = code & 0xFFFFFF;
		unsigned pind = ind / PALETTE_SIZE;

		ind = ind % PALETTE_SIZE;

		__color = palette[pind].color[ind];
	}
	else
		__color = code;

	color = CColor::from_rgb(
			__color & 0xFF,
			(__color & 0xFF00) >> 8,
			(__color & 0xFF0000) >> 16,
			255);

	return color;
}

