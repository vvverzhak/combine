
#ifndef CLASSIFIER_RENDERER_HPP
#define CLASSIFIER_RENDERER_HPP

#include "base/base.hpp"
#include "palette/palette.hpp"
#include "renderer/renderer.hpp"
#include "renderer/classifier/texture.hpp"
#include "renderer/classifier/rsc.hpp"

class CClassifierRenderer : public CRenderer
{
	// std потому, что в CRenderer есть поле с именем map
	std::map<unsigned, vector<t_object_to_draw> > layers;
	CRSC rsc;
	const CPalette & palette;

	void push(const CObject & obj);

	public:
		
		CClassifierRenderer(
			const string rsc_fname,
			const unsigned __height, const unsigned __width,
			const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
			const CMap & __map, const CHeightMap & __height_map,
			const CCamera & __camera,
			const CScale & __scale,
			const CPalette & __palette);

		void create_textures();
};

#endif

