
#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "base/base.hpp"
#include "map/map.hpp"
#include "map/scale.hpp"
#include "palette/color.hpp"
#include "camera/camera.hpp"
#include "renderer/texture.hpp"

struct STriangleRow
{
	unsigned texture;
	vector< pair<CPoint, CPoint> > pnt;
};

class CRenderer
{
	bool is_first;
	vector< vector< vector<GLuint> > > vertex_buffer_ind, texture_buffer_ind;
	vector< vector< vector<unsigned> > > pnt_num_array;

	unsigned find_square(const CPoint pnt);
	vector<STriangleRow> get_triangle(const unsigned step);
	void add_buffer(const vector< STriangleRow > & triangle);
	void create_model();

	protected:

		const double seam = 0.1;
		const unsigned texture_size, texture_per_row;
		const vector<unsigned> texture_step;
		const CMap & map;
		const CHeightMap & height_map;
		const CCamera & camera;
		const CScale & scale;
		const CColor bg_color, horizont_color;
		vector< shared_ptr<CTexture> > texture;

		virtual void create_textures() = 0;

	public:

		const unsigned height, width;

		CRenderer(
			const unsigned __height, const unsigned __width,
			const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
			const CMap & __map, const CHeightMap & __height_map,
			const CCamera & __camera,
			const CScale & __scale,
			const CColor & __bg_color,
			const CColor & __horizont_color);

		void redraw();
};

#endif

