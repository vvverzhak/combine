
#include "renderer/texture.hpp"

CTexture::CTexture(const unsigned __texture_size, const CPoint __center, const double __max_r) :
	texture_size(__texture_size)
{
	CPoint min_pnt, max_pnt;

	center = __center;
	max_r = __max_r;

	min_pnt.x = center.x - max_r;
	min_pnt.y = center.y - max_r;
	min_pnt.z = -1;

	max_pnt.x = center.x + max_r;
	max_pnt.y = center.y + max_r;
	max_pnt.z = 1;
	
	a.x = 1 / (max_pnt.x - min_pnt.x);
	a.y = 1 / (max_pnt.y - min_pnt.y);

	b.x = - a.x * min_pnt.x;
	b.y = - a.y * min_pnt.y;
}

void CTexture::bind()
{
	glBindTexture(GL_TEXTURE_2D, ind);
}

CPoint CTexture::scene_to_texture(const CPoint & pnt)
{
	return CPoint(a.x * pnt.x + b.x, a.y * pnt.y + b.y, 0);
}

bool CTexture::obj_in_texture(const CObject & obj)
{
	for(auto pnt : obj.pnt_scene)
		if(fabs(pnt.x - center.x) <= max_r && fabs(pnt.y - center.y) <= max_r)
			return true;

	return true;
}

