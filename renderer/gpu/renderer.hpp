
#ifndef GPU_RENDERER_HPP
#define GPU_RENDERER_HPP

#include "base/base.hpp"
#include "palette/palette.hpp"
#include "renderer/renderer.hpp"
#include "renderer/gpu/texture.hpp"

class CGPURenderer : public CRenderer
{
	const CPalette & palette;

	public:
		
		CGPURenderer(
			const unsigned __height, const unsigned __width,
			const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
			const CMap & __map, const CHeightMap & __height_map,
			const CCamera & __camera,
			const CScale & __scale,
			const CPalette & __palette);

		void create_textures();
};

#endif

