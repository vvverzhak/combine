
#ifndef GPU_TEXTURE_HPP
#define GPU_TEXTURE_HPP

#include "base/base.hpp"
#include "palette/palette.hpp"
#include "renderer/texture.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

class CGPUTexture : public CTexture
{
	public:

		CGPUTexture(const unsigned __texture_size, const CPoint __center, const double __max_r);

		void draw(const CMap & map, const CPalette & palette);
};

#endif

