
#include "renderer/gpu/texture.hpp"

CGPUTexture::CGPUTexture(const unsigned __texture_size, const CPoint __center, const double __max_r) :
	CTexture(__texture_size, __center, __max_r)
{
	;
}

// ############################################################################ 

void CGPUTexture::draw(const CMap & map, const CPalette & palette)
{
	const bool __is_polygon_as_a_line = is_polygon_as_a_line();

	draw_engine->begin_texture(texture_size, center, max_r);

	for(auto & obj : map.obj)
		if(obj.type != EOT_NOT_DRAW && obj_in_texture(obj))
		{
			draw_engine->color = palette[obj.code];

			if(draw_engine->color.a > 0)
			{
				if(obj.type == EOT_POINT)
				{
					draw_engine->begin(EPT_POINT);
					draw_engine->vertex(obj.pnt_scene);
					draw_engine->end();
				}
				else if(obj.type == EOT_LINE || (obj.type == EOT_POLYGON && __is_polygon_as_a_line))
				{
					draw_engine->begin(EPT_LINE);
					draw_engine->vertex(obj.pnt_scene);
					draw_engine->end();
				}
				else if(obj.type == EOT_POLYGON)
				{
					draw_engine->begin(EPT_POLYGON);
					draw_engine->vertex(obj.pnt_scene);

					for(auto & sub_obj : obj.sub_obj)
					{
						draw_engine->begin(EPT_POLYGON_HOLE);
						draw_engine->vertex(sub_obj.pnt_scene);
						draw_engine->end();
					}

					draw_engine->end();
				}
			}
		}

	ind = draw_engine->end_texture();
}

