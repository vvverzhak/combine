
#include "renderer/gpu/renderer.hpp"

CGPURenderer::CGPURenderer(
	const unsigned __height, const unsigned __width,
	const unsigned __texture_size, const unsigned __texture_per_row, const vector<unsigned> __texture_step,
	const CMap & __map, const CHeightMap & __height_map,
	const CCamera & __camera,
	const CScale & __scale,
	const CPalette & __palette) :

	CRenderer(__height, __width, __texture_size, __texture_per_row, __texture_step, __map, __height_map, __camera, __scale, __palette.background, __palette.horizont), palette(__palette)
{
	;
}

void CGPURenderer::create_textures()
{
	unsigned v, u, ind;
	double dv, du;
	const double max_r = (scale.scene_to.x - scale.scene_from.x) / (2 * texture_per_row); // TODO Не квадратные сцены
	const double max_r_2 = 2 * max_r; // TODO Не квадратные сцены
	CGPUTexture * p_texture;

	texture.clear();

	for(v = 0, ind = 0, dv = scale.scene_from.y + max_r; v < texture_per_row; v++, dv += max_r_2)
		for(u = 0, du = scale.scene_from.x + max_r; u < texture_per_row; u++, ind++, du += max_r_2)
		{
			throw_null(p_texture = new CGPUTexture(texture_size, CPoint(du, dv, 0), max_r + seam));
			p_texture->draw(map, palette);
			texture.push_back(shared_ptr<CTexture>(p_texture));
		}
}

