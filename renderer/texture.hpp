
#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "base/base.hpp"
#include "map/point.hpp"
#include "map/map.hpp"

class CTexture
{
	protected:

		const unsigned texture_size;
		double max_r;
		GLuint ind;
		CPoint center, a, b, min_pnt, max_pnt;

	public:

		CTexture(const unsigned __texture_size, const CPoint __center, const double __max_r);

		void bind();
		CPoint scene_to_texture(const CPoint & pnt);
		bool obj_in_texture(const CObject & obj);
};

#endif

