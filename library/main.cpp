
#include "base/base.hpp"
#include "library/combine.h"
#include "base/protocol.hpp"
#include "driver/sxf.hpp"
#include "driver/mtr.hpp"
#include "camera/camera.hpp"
#include "display/sdl/display.hpp"
#include "palette/my_palette.hpp"
#include "palette/thin_line_palette.hpp"
#include "palette/black_white_palette.hpp"
#include "library/config.hpp"
#include "renderer/gpu/renderer.hpp"
#include "renderer/classifier/renderer.hpp"
#include "renderer/draw_engine/draw_engine.hpp"

#ifdef USING_CAIRO

	#include "renderer/draw_engine/draw_engine_cairo.hpp"

#else

	#include "renderer/draw_engine/draw_engine_opengl.hpp"

#endif

bool is_run = false;
unsigned __height, __width, __texture_size, __texture_per_row;
vector<unsigned> texture_step;
shared_ptr<CPalette> __palette;
shared_ptr<CSXFMap> __map;
shared_ptr<CMTRMap> __height_map;
shared_ptr<CScale> __scale_map, __scale_height_map;
shared_ptr<CCamera> __camera;
shared_ptr<CRenderer> __renderer;
shared_ptr<CDisplay> __disp;
shared_ptr<CDrawEngine> __draw_engine;

int combine_init(
		const unsigned height,
		const unsigned width,
		const unsigned texture_size,
		const unsigned texture_per_row,
		const EMapAccuracy accuracy,
		const EPalette palette,
		const bool is_polygon_as_a_line,
		const bool is_print_coord,
		const char * map_fname,
		const char * height_map_fname,
		const char * classifier_fname,
		const char * font_fname
	)
{
	int ret = 0;

	try
	{
		throw_if(is_run);

		__height = height;
		__width = width;
		__texture_size = texture_size;
		__texture_per_row = texture_per_row;
		__deblurring_port = 1988; // TODO algo в целом и настройка порта в частности

		switch(accuracy)
		{
			case EMA_LOW:
			{
				texture_step.push_back(11);
				texture_step.push_back(13);
				texture_step.push_back(15);

				break;
			}
			case EMA_HIGH:
			{
				texture_step.push_back(1);

				break;
			}
			default:
			{
				throw_;
			}
		}

		__is_polygon_as_a_line = is_polygon_as_a_line;
		__is_print_coord = is_print_coord;

		__map_fname = map_fname;
		__height_map_fname = height_map_fname;
		__classifier_fname = classifier_fname;
		__local_param_fname = "" ; // /tmp/param";
		__font_fname = font_fname;

		// ############################################################################
		// Инициализация

		CPoint gk_min, gk_max, map_min, map_max, height_map_min, height_map_max;

		__map.reset(new CSXFMap(__map_fname));
		throw_null(__map);

		__height_map.reset(new CMTRMap(__height_map_fname, __map->min_gk(), __map->max_gk(), 1500));
		throw_null(__height_map);

		map_min = __map->min_map();
		map_max = __map->max_map();
		map_min.z = 0;
		map_max.z = MAX_H;

		gk_min = __map->min_gk();
		gk_max = __map->max_gk();
		gk_min.z = 0;
		gk_max.z = MAX_H;

		height_map_min = __height_map->min_map();
		height_map_max = __height_map->max_map();
		height_map_min.z = 0;
		height_map_max.z = MAX_H;

		__scale_map.reset(new CScale(gk_min, gk_max, map_min, map_max, CPoint(-1, -1, -1), CPoint(1, 1, 1), true));
		throw_null(__scale_map);

		__scale_height_map.reset(new CScale(height_map_min, height_map_max, height_map_min, height_map_max, CPoint(-1, -1, -1), CPoint(1, 1, 1), true));
		throw_null(__scale_height_map);

		__camera.reset(new CCamera(* __map, * __scale_map, CPoint(0, 0, -1), CEulerAngle(0, 0, 0), 20, 20, ECT_GL));
		throw_null(__camera);

		__map->map_to_scene(* __scale_map);
		__height_map->map_to_scene(* __scale_height_map);

		__disp = create_display(__height, __width);
		throw_null(__disp);

#ifndef EMBEDDED

		throw_if(glewInit() != GLEW_OK);

#endif

		switch(palette)
		{
			case EP_THIN_LINE:
			{
				__palette.reset(new CThinLinePalette);

				break;
			}
			case EP_MY:
			{
				__palette.reset(new CMyPalette);

				break;
			}
			case EP_BLACK_WHITE:
			{
				__palette.reset(new CBlackWhitePalette);

				break;
			}
			default:
			{
				throw_;
			}
		}

		throw_null(__palette);

#ifdef USING_CAIRO
	
		__draw_engine.reset(new CCairoDrawEngine(* __palette));

#else
	
		__draw_engine.reset(new COpenGLDrawEngine(* __palette));

#endif

		throw_null(__draw_engine);

		if(__classifier_fname == "")
			__renderer.reset(new CGPURenderer(
				__height, __width, __texture_size, __texture_per_row, texture_step,
				* __map, * __height_map, * __camera, * __scale_map, * __palette));
		else
			__renderer.reset(new CClassifierRenderer(
				__classifier_fname,
				__height, __width, __texture_size, __texture_per_row, texture_step,
				* __map, * __height_map, * __camera, * __scale_map, * __palette));

		throw_null(__renderer);

		__disp->set_renderer(__renderer.get());

		is_run = true;
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_destroy()
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		__disp.reset();
		__camera.reset();
		__scale_height_map.reset();
		__scale_map.reset();
		__height_map.reset();
		__map.reset();
		is_run = false;
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_set_aspect(const double aspect_x, const double aspect_y)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		__camera->aspect_x = aspect_x;
		__camera->aspect_y = aspect_y;
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_get_aspect(double * aspect_x, double * aspect_y)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		throw_null(aspect_x);
		throw_null(aspect_y);

		* aspect_x = __camera->aspect_x;
		* aspect_y = __camera->aspect_y;
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_set_camera(
		const double x, const double y, const double h,
		const double course, const double roll, const double pitch,
		const ECoordType coord_type
	)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		__camera->euler_angle.course = course;
		__camera->euler_angle.roll = roll;
		__camera->euler_angle.pitch = pitch;

		switch(coord_type)
		{
#define SET(ct, from)\
case ct:\
{\
	__camera->from.x = x;\
	__camera->from.y = y;\
	__camera->from.z = h;\
\
	break;\
}

			SET(ECT_GL, pos)
			SET(ECT_GEO, geo_pos)
			SET(ECT_GAUSS_KRUEGER, gauss_krueger_pos)
		}

		__camera->eval(coord_type);
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_get_camera(
		double * x, double * y, double * h,
		double * course, double * roll, double * pitch,
		const ECoordType coord_type
	)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		throw_null(x);
		throw_null(y);
		throw_null(h);
		throw_null(course);
		throw_null(roll);
		throw_null(pitch);

		* course = __camera->euler_angle.course;
		* roll = __camera->euler_angle.roll;
		* pitch = __camera->euler_angle.pitch;

		switch(coord_type)
		{
#define GET(ct, from)\
case ct:\
{\
	* x = __camera->from.x;\
	* y = __camera->from.y;\
	* h = __camera->from.z;\
\
	break;\
}

			GET(ECT_GL, pos)
			GET(ECT_GEO, geo_pos)
			GET(ECT_GAUSS_KRUEGER, gauss_krueger_pos)
		}
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_get_frame_param(unsigned * height, unsigned * width, unsigned * channel_num, unsigned * bytes_per_channel)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		throw_null(height);
		throw_null(width);
		throw_null(channel_num);
		throw_null(bytes_per_channel);

		* height = __height;
		* width = __width;
		* channel_num = 3;
		* bytes_per_channel = 1;
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_redraw()
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		__disp->redraw();
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_read_frame(void * buffer)
{
	int v, u, ret = 0;
	const unsigned width_3 = __width * 3;
	const unsigned size_3 = __height * width_3;
	shared_ptr<uint8_t> buffer_rev;
	uint8_t * p_buffer_rev;

	try
	{
		throw_if(! is_run);

		buffer_rev.reset(new uint8_t[__height * width_3], std::default_delete<uint8_t[]>());
		throw_null(p_buffer_rev = buffer_rev.get());

		__disp->read_frame(p_buffer_rev, GL_RGB, GL_UNSIGNED_BYTE);

		// RGB -> BGR
		for(v = 0; v < size_3; v += 3)
		{
			uint8_t temp = p_buffer_rev[v];

			p_buffer_rev[v] = p_buffer_rev[v + 2];
			p_buffer_rev[v + 2] = temp;
		}

		// Upside down
		for(v = __height - 1, u = 0; v >= 0; v--, u++)
			memcpy(buffer + u * width_3, p_buffer_rev + v * width_3, width_3);
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

int combine_redraw_read_frame(void * buffer)
{
	int ret = 0;

	try
	{
		throw_if(! is_run);

		throw_if(combine_redraw());
		throw_if(combine_read_frame(buffer));
	}
	catch(...)
	{
		ret = -1;
	}

	return ret;
}

