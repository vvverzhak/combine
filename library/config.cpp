
#include "base/base.hpp"
#include "library/config.hpp"

#define GET(fun, type, var)\
type var;\
\
type fun()\
{\
	return var;\
}

GET(is_print_coord, bool, __is_print_coord);
GET(is_polygon_as_a_line, bool, __is_polygon_as_a_line);

GET(deblurring_port, uint16_t, __deblurring_port);

GET(map_fname, string, __map_fname);
GET(height_map_fname, string, __height_map_fname);
GET(classifier_fname, string, __classifier_fname);
GET(local_param_fname, string, __local_param_fname);
GET(font_fname, string, __font_fname);

