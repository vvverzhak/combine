
#ifndef LIBRARY_CONFIG_HPP
#define LIBRARY_CONFIG_HPP

#include "base/base.hpp"
#include "base/config.hpp"

extern bool __is_print_coord;
extern bool __is_polygon_as_a_line;

extern uint16_t __deblurring_port;

extern string __map_fname;
extern string __height_map_fname;
extern string __classifier_fname;
extern string __local_param_fname;
extern string __font_fname;

#endif

