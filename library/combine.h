
#ifndef COMBINE_H
#define COMBINE_H

enum ECoordType
{
	ECT_GL = 0,
	ECT_GEO = 1,
	ECT_GAUSS_KRUEGER = 2
};

enum EMapAccuracy
{
	EMA_HIGH = 0,
	EMA_LOW = 1
};

enum EPalette
{
	EP_THIN_LINE = 0,
	EP_MY = 1,
	EP_BLACK_WHITE = 2
};

extern "C" int combine_init(
		const unsigned height,
		const unsigned width,
		const unsigned texture_size,
		const unsigned texture_per_row,
		const EMapAccuracy accuracy,
		const EPalette palette,
		const bool is_polygon_as_a_line,
		const bool is_print_coord,
		const char * map_fname,
		const char * height_map_fname,
		const char * classifier_fname,
		const char * font_fname
	);

extern "C" int combine_destroy();

extern "C" int combine_set_aspect(const double aspect_x, const double aspect_y);
extern "C" int combine_get_aspect(double * aspect_x, double * aspect_y);
extern "C" int combine_set_camera(
		const double x, const double y, const double h,
		const double course, const double roll, const double pitch,
		const ECoordType coord_type
	);
extern "C" int combine_get_camera(
		double * x, double * y, double * h,
		double * course, double * roll, double * pitch,
		const ECoordType coord_type
	);

extern "C" int combine_get_frame_param(unsigned * height, unsigned * width, unsigned * channel_num, unsigned * bytes_per_channel);
extern "C" int combine_redraw();
extern "C" int combine_read_frame(void * buffer);
extern "C" int combine_redraw_read_frame(void * buffer);

#endif

