
#ifndef MTR_HPP
#define MTR_HPP

#include "base/base.hpp"
#include "map/map.hpp"

struct __attribute__((packed)) SMTRHeader
{
	uint32_t	id;
	int32_t		x_min;
	int32_t		y_min;
	int32_t		x_max;
	int32_t		y_max;
	float		size_el_mat;
	int32_t		h_min;
	int32_t		h_max;
	uint32_t	width;
	uint32_t	height;
};

/*
struct __attribute__((packed)) SMTRHeader
{
	uint32_t id;
	uint32_t x_min;
	uint32_t y_min;
	uint32_t x_max;
	uint32_t y_max;
	uint16_t not_used_1;
	uint16_t not_used_2;
	uint16_t not_used_3;
	uint16_t not_used_4;
	uint16_t not_used_5;
	uint16_t not_used_6;
	uint16_t height;
	uint16_t width;
	uint16_t elem_size;
	uint32_t elem_size_mm;
	uint16_t discrete_in_elem;
	uint16_t flags;
	uint16_t unit;
	int32_t h_min;
	int32_t h_max;
	uint32_t code;
	uint32_t point_per_meter;
	int32_t scale;
	uint16_t type;
	char not_used_7[6];
	uint16_t version;
	uint16_t not_used_8;
};
*/

// ############################################################################ 

class CMTRMap : public CHeightMap
{
	SMTRHeader header;

	void unpack_header();
	void check();
	void create_point_grid(const vector< vector<CPoint> > & raw_pnt, const CPoint from, const CPoint to, const unsigned size);

	public:

		CMTRMap(const string fname, const CPoint from, const CPoint to, const unsigned size);

		CPoint min_map() const;
		CPoint max_map() const;
		CPoint min_gk() const { throw_; };
		CPoint max_gk() const { throw_; };

		void print();
};

#endif

