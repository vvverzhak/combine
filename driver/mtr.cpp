
#include "driver/mtr.hpp"

CMTRMap::CMTRMap(const string fname, const CPoint from, const CPoint to, const unsigned size)
{
	unsigned v, u;
	
#ifdef EMBEDDED // Ограничение - только для МГК

	header.width = 1000;
	header.height = 1000;

	for(v = 0; v < header.height; v++)
	{
		vector<CPoint> row;

		for(u = 0; u < header.width; u++)
			row.push_back(CPoint(u, v, 100));

		pnt.push_back(row);
	}

#else

	unsigned t, elem_num;
	float * p_buf;
	shared_ptr<float> buf;
	vector< vector<CPoint> > raw_pnt;
	CFile fl(fname, EFileModeBinaryRead);
	
	fl(& header, sizeof(header));
	unpack_header();

	check();

	elem_num = header.height * header.width;
	buf.reset(new float[elem_num], std::default_delete<float[]>());
	p_buf = buf.get();
	throw_null(p_buf);

	fl(p_buf, elem_num * sizeof(float));
	CFile::unpack<float>(p_buf, elem_num);

	for(u = 0, t = 0; u < header.width; u++)
	{
		vector<CPoint> col;

		for(v = 0; v < header.height; v++, t++)
			col.push_back(CPoint(u, v, p_buf[t]));

		raw_pnt.push_back(col);
	}

	create_point_grid(raw_pnt, from, to, size);

#endif
}

void CMTRMap::create_point_grid(const vector< vector<CPoint> > & raw_pnt, const CPoint from, const CPoint to, const unsigned size)
{
	unsigned v, u, raw_v_uint, raw_u_uint;
	double y, x, h, raw_v, raw_u, d_v, d_u;
	const double step_y = (to.y - from.y) / (size - 1), step_x = (to.x - from.x) / (size - 1);
	const double raw_step_y = (header.y_max - header.y_min) / (header.height - 1), raw_step_x = (header.x_max - header.x_min) / (header.width - 1);

	pnt.clear();

	for(v = 0; v < size; v++)
	{
		vector<CPoint> row;

		for(u = 0; u < size; u++)
		{
			y = from.y + v * step_y;
			x = from.x + u * step_x;

			raw_v = (y - header.y_min) / raw_step_y;
			raw_u = (x - header.x_min) / raw_step_x;
			raw_v_uint = raw_v;
			raw_u_uint = raw_u;
			d_v = raw_v - raw_v_uint;
			d_u = raw_u - raw_u_uint;

			// ############################################################################ 

			// TODO Что, если граница матрицы высот совпадает с границей карты

			h = 
				(1 - d_v) * (1 - d_u) * raw_pnt[raw_u_uint][raw_v_uint].z
				+
				d_v * (1 - d_u) * raw_pnt[raw_u_uint + 1][raw_v_uint].z
				+
				(1 - d_v) * d_u * raw_pnt[raw_u_uint][raw_v_uint + 1].z
				+
				d_v * d_u * raw_pnt[raw_u_uint + 1][raw_v_uint + 1].z;

			// ############################################################################ 

			row.push_back(CPoint(u, v, h));
		}

		pnt.push_back(row);
	}
}

void CMTRMap::unpack_header()
{
	CFile::unpack<uint32_t>(& header.id);
	CFile::unpack<uint32_t>((uint32_t *) & header.x_min);
	CFile::unpack<uint32_t>((uint32_t *) & header.y_min);
	CFile::unpack<uint32_t>((uint32_t *) & header.x_max);
	CFile::unpack<uint32_t>((uint32_t *) & header.y_max);
	CFile::unpack<float>(& header.size_el_mat);
	CFile::unpack<uint32_t>((uint32_t *) & header.h_min);
	CFile::unpack<uint32_t>((uint32_t *) & header.h_max);
	CFile::unpack<uint32_t>(& header.width);
	CFile::unpack<uint32_t>(& header.height);
}

void CMTRMap::check()
{
	;
}

void CMTRMap::print()
{
	printf_TODO("%u %u %d %d %d %d %d %d\n", header.height, header.width, header.x_min, header.y_min, header.x_max, header.y_max, header.h_min, header.h_max);
}

CPoint CMTRMap::min_map() const
{
	return CPoint(0, 0, 0);
}

CPoint CMTRMap::max_map() const
{
	return CPoint(pnt[0].size(), pnt.size(), 0);
}

